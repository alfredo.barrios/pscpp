With little steps of geniality we can do everything
--

Credits and Contributors
-
Thank you to all the people who have already contributed to PRO-Codex: PSCPP-Platform !!!
1. Jose Colbes Sanabria
2. Alfredo Barrios Villanueva

The Contribute Motivation doesn't matter(education, business, etc), the fact is this: Your apport is invaluable for the community spirit, and the proyect impact is not a simple metric.
--
###
#Development Cycle
#Commit Message Format  
Each commit message consists of a header, a body and a footer. The header has a special format that includes a type, a scope and a subject:  
```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```
The header is mandatory and the scope of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read on GitLab as well as in various git tools.

The footer should contain a closing reference to an issue if any.

#Samples: (even more samples)

docs(LibraryParserImpl): update the rotamer library parser

Update of the JavaDoc, for support the new type of line in the Standard Rotamer Library  
py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.parserimplementation.LibraryParserImpl

Task Number: 777

build(maven): migrate to Biojava X

Update the BioJava version in the pom.xml, change with no effect in the build time 
because the BioJava support the previous version.

Task Number: 33

fix(main): add validation in the command line method

The option -rd give a null pointer exception when the folder of the  
PDB's is empty  
Now the option has a validation for the issue  

Task Number: 13

##Revert
If the commit reverts a previous commit, it should begin with revert:, followed by the header of the reverted commit. In the body it should say: This reverts commit <hash>., where the hash is the SHA of the commit being reverted.

##Type Must be one of the following:

build: Changes that affect the build system or external dependencies (example scopes: maven )  
docs: Documentation only changes  
feat: A new feature  
fix: A bug fix  
perf: A code change that improves performance  
refactor: A code change that neither fixes a bug nor adds a feature  
style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)  
test: Adding missing tests or correcting existing tests  
##Scope
The scope should be the name of the Class or Module affected (as perceived by the person reading the changelog generated from commit messages.)  
The following is the list of supported scopes:  

main  
pdb  
rotlib  
simple-tests  
utils  
maven  
CLASSNAME
###The subject contains a succinct description of the change:

use the imperative, present tense: "change" not "changed" nor "changes"
don't capitalize the first letter
no dot (.) at the end
###Body
Just as in the subject, use the imperative, present tense: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with previous behavior.  

Footer
--
The footer should contain any information about Breaking Changes and is also the place to reference GitHub issues that this commit Closes.  

Breaking Changes should start with the word BREAKING CHANGE: with a space or two newlines. The rest of the commit message is then used for this.  

Example:  

feat(scope): commit message  

BREAKING CHANGES:  

Describe breaking changes here  

BEFORE:  

Previous code example here  

AFTER:  

New code example here  

#Testing

Testing Policies



# "Freedom, the first-born of science.” ― Thomas Jefferson
#"Let the Experiment Be Made" ―  Benjamin Franklin
