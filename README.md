Pro-Codex: PSCPP Platform
--
Building a high quality and efficient code for Protein Analysis.  
We believe in Free Software as the key to solve complex problems like the Protein Side Packing Problem.     
Please contribute with the platform

About the Problem
---
Protein Side-Chain Packing Problem is one of the most popular open problems in bioinformatics field,  
it has been around since the early homology modeling approach. Along with protein 3D structure prediction,   
solving PSCPP promises a methodology to drugs design and developing accurate enzymes with high impact on modern medicine,   
among other biotechnological applications, importance of PSCPP has been increasing time ago.   
Despite several work on last decades, this problem is known to be NP-hard,   
so a deterministic algorithm that runs on feasible time has not been found.  

Rules
--
Prlić A, Procter JB (2012)  
Ten Simple Rules for the Open Development of Scientific Software.  
PLoS Comput Biol 8(12): e1002802. https://doi.org/10.1371/journal.pcbi.1002802  

1) Don't Reinvent the Wheel.
2) Code Well.
3) Be Your Own User.
4) Be Transparent.
5) Be Simple.
6) Don't Be a Perfectionist.
7) Nurture and Grow the Community.
8) Promote the Project.
9) Find Sponsors.
10) Science Counts.


For Build the Program
--
1) Be sure to have the JDK8 correctly installed executing: java -version
2) Check if your environment variable is correctly configured(A folder with the JDK is required), example in Linux: echo $JAVA_HOME
3) Execute the script: mvnw
    1) Example in Linux:   ./mvnw clean install
    2) In Windows Run the Batch script: mvnw.cmd clean install
