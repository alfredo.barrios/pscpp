package py.com.pol.biocomputacion.pscpp.main;

import org.apache.commons.cli.*;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FilenameUtils;
import org.biojava.nbio.structure.Structure;
import org.biojava.nbio.structure.io.PDBFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.pdb.PDBReader;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.PreprocessRotamerLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.ProcessRotamerLibrary;

import java.io.File;


/**
 * @author Jose Colbes
 * @author Alfredo Barrios
 * @version 1.0
 * @since october 2018
 * Main class for the command line of  the PSCPP-Platform
 */
public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    /**
     * @param args arguments from the user
     **/
    public static void main(String[] args) {
        try {
            ParameterFile.loadAll();
            Options options = new Options();
            addCommandOptions(options);
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);
            PDBFileReader reader = PDBReader.getReader();
            if (commandLine.hasOption("help")) {
                printInfo();
            }
            if (commandLine.hasOption("rd")) {
                String listDirectory = null;
                try {
                    //Read all the pdb files from a Directory
                    listDirectory = commandLine.getOptionValue("rd");
                    if (listDirectory != null) {
                        File folder = new File(listDirectory);
                        for (File file : folder.listFiles()) {
                            if (file != null) {
                                String extension = FilenameUtils.getExtension(file.getAbsolutePath());
                                if ("pdb".equals(extension)) {
                                    Structure structure = reader.getStructure(file);
                                    logger.info("Pdb Code: {}" , structure.getPDBCode());
                                    logger.info("Poly Chains: {}" , structure.getPolyChains());
                                }
                            }
                        }
                    } else {
                        logger.error("Option -rd with empty Parameter");
                    }
                } catch (Exception e) {
                    logger.error("Error reading PDB folder: {} " , listDirectory);
                }
            }
            if (commandLine.hasOption("rf")) {
                //Read a single PDB file, specified in the argument
                String file = commandLine.getOptionValue("rf");
                Structure structure = reader.getStructure(file);
                logger.info("Pdb Code: {}" , structure.getPDBCode());
                logger.info("Poly Chains: {}" , structure.getPolyChains());

            }
            if (commandLine.hasOption("sdl")) {
                String libraryFile = commandLine.getOptionValue("sdl");
                ProcessRotamerLibrary.sortByDependentLibraryByProbability(libraryFile, ".sorted");

            }
            if (commandLine.hasOption("sil")) {
                String libraryFile = commandLine.getOptionValue("sil");
                ProcessRotamerLibrary.sortByIndependentLibraryByProbability(libraryFile);

            }
            if (commandLine.hasOption("fil")) {
                String[] values = commandLine.getOptionValues("fil");
                double probabilityIndependentLibrary = 0D;
                try {
                    probabilityIndependentLibrary = Double.valueOf(values[1]);
                } catch (Exception e) {
                    logger.error("Option -fil invalid parameters,  probability is not a number");
                    System.exit(1);
                }
                ProcessRotamerLibrary.reduceIndependentLibraryByProbability(values[0], probabilityIndependentLibrary);

            }
            if (commandLine.hasOption("fdl")) {
                String[] values = commandLine.getOptionValues("fdl");
                double probabilityDependentLibrary = 0D;
                try {
                    probabilityDependentLibrary = Double.valueOf(values[1]);
                } catch (Exception e) {
                    logger.error("Option -fdl invalid parameter,  probability is not a number");
                    System.exit(1);
                }
                ProcessRotamerLibrary.reduceDependentLibraryByProbability(values[0], probabilityDependentLibrary);

            }
            if (commandLine.hasOption("fpi")) {
                String[] values = commandLine.getOptionValues("fpi");
                double accumulatedProbability = 0D;
                try {
                    accumulatedProbability = Double.valueOf(values[1]);
                } catch (Exception e) {
                    logger.error("Option -fpi invalid parameter,  probability is not a number");
                    System.exit(1);
                }
                ProcessRotamerLibrary.reduceIndependentByAccumulatedProbability(values[0], accumulatedProbability);
            }
            if (commandLine.hasOption("fpd")) {
                String[] values = commandLine.getOptionValues("fpd");
                double accumulatedProbability = 0D;
                try {
                    accumulatedProbability = Double.valueOf(values[1]);
                } catch (Exception e) {
                    logger.error("Option -fpd invalid parameter,  probability is not a number");
                    System.exit(1);
                }
                ProcessRotamerLibrary.reduceDependentByAccumulatedProbability(values[0], accumulatedProbability);
            }
            if (commandLine.hasOption("bdf")) {
                String[] values = commandLine.getOptionValues("bdf");
                Integer interval = null;
                try {
                    interval = Integer.valueOf(values[2]);
                } catch (Exception e) {
                    logger.error("Option -bdf invalid parameter,  interval is not a number");
                    System.exit(1);
                }
                PreprocessRotamerLibrary.generateBinaryFileForDependentLibrary(values[0], values[1], interval);
            }
            if (commandLine.hasOption("bif")) {
                String[] values = commandLine.getOptionValues("bif");
                PreprocessRotamerLibrary.generateBinaryFileForIndependentLibrary(values[0], values[1]);
            }
        } catch (Exception e) {
            logger.error("Main exeption: ", e);
        }
    }

    /**
     * Build the available command line options
     *
     * @param options
     */
    private static void addCommandOptions(Options options) {
        options.addOption("help", false, "display help");
        options.addOption(Option.builder("rd").longOpt("rd").required(false).desc("Read a Folder of Pdb").hasArg().numberOfArgs(1).build());
        options.addOption(Option.builder("rf").longOpt("rf").required(false).desc("Read a Pdb File").hasArg().numberOfArgs(1).build());
        options.addOption(Option.builder("prf").longOpt("prf").required(false).desc("Preprocess Rotamer Library").hasArg().numberOfArgs(1).build());
        options.addOption(Option.builder("sdl").longOpt("sdl").required(false).desc("Sort Rotamer Dependent Library").hasArgs().numberOfArgs(1).build());
        options.addOption(Option.builder("sil").longOpt("sil").required(false).desc("Sort Rotamer Independent Library").hasArg().numberOfArgs(1).build());
        options.addOption(Option.builder("fil").longOpt("fil").required(false).desc("Filter Independent Rotamer Library by Probability").hasArg().numberOfArgs(2).build());
        options.addOption(Option.builder("fdl").longOpt("fdl").required(false).desc("Filter Dependent Rotamer Library by Probability").hasArgs().numberOfArgs(2).build());
        options.addOption(Option.builder("fpi").longOpt("fpi").required(false).desc("Filter Independent Rotamer Library by Accumulated Probability").hasArg().numberOfArgs(2).build());
        options.addOption(Option.builder("bdf").longOpt("bdf").required(false).desc("Generate a Binary File from Dependent Rotamer Library").hasArg().numberOfArgs(3).build());
        options.addOption(Option.builder("bif").longOpt("bif").required(false).desc("Generate a Binary File from Independent Rotamer Library").hasArg().numberOfArgs(2).build());
    }

    /**
     * Print the User Guide
     **/
    private static void printInfo() {
        logger.info("Usage Examples:");
        logger.info("1) PDB files:");
        logger.info("java -jar pscpp.jar -rd /path/to/pdbFolder");
        logger.info("java -jar pscpp.jar -rf /path/to/file/1HBB.pdb");
        logger.info("Where: ");
        logger.info("-rd (read directory) read a complete folder of PDB files");
        logger.info("-rf (read file) read a Single PDB file.");

        //logger.info("\n2) List of PDBs:");
        //logger.info("java -jar pscpp.jar -l listFile -i pdbInFolder -o pdbOutFolder");
        //logger.info("Where: ");
        //logger.info("-l listFile: filename of the list with the PDB identifiers of the proteins.");
        //logger.info("-i pdbInFolder: name of the folder with the input PDB files.");
        //logger.info("-o pdbOutFolder: name of the folder with the output PDB files.");

        logger.info("\n3) Rotamer Library Operations:");
        logger.info("java -jar pscpp.jar -sdl /path/to/rotamerDependentLibrary");
        logger.info("java -jar pscpp.jar -sil /path/to/rotamerIndependentLibrary");
        logger.info("java -jar pscpp.jar -bdf /path/to/rotamerDependentLibrary nameOfTheBinaryFile");
        logger.info("java -jar pscpp.jar -bif /path/to/rotamerInDependentLibrary nameOfTheBinaryFile");
        logger.info("java -jar pscpp.jar -fil /path/to/rotamerIndependentLibrary 0.5");
        logger.info("java -jar pscpp.jar -fdl /path/to/rotamerDependentLibrary 0.5");
        logger.info("java -jar pscpp.jar -fpi /path/to/rotamerIndependentLibrary 0.5");
        logger.info("java -jar pscpp.jar -fpd /path/to/rotamerDependentLibrary 0.5");
        logger.info("Where: ");
        logger.info("-sdl rotamerDependentLibrary: Sort the given Rotamer Dependent Library");
        logger.info("-sil rotamerIndependentLibrary: Sort the given Rotamer Independent Library");
        logger.info("-fil rotamerInDependentLibrary: Filter the library by probabilities greater than MyProbability(X)");
        logger.info("-fdl rotamerDependentLibrary: Filter the library by probabilities greater than MyProbability(X)");
        logger.info("-fpi rotamerInDependentLibrary: Filter the library by accumulate probability greater than MyAccumulatedProbability(X)");
        logger.info("-fpd rotamerDependentLibrary: Filter the library by accumulate probability greater than MyAccumulatedProbability(X)");
        logger.info("-bdf rotamerDependentLibrary: Generate the Dependent Rotamer library in a Binary Format for performance purposes");
        logger.info("-bif rotamerInDependentLibrary: Generate the Independent Rotamer library in a Binary Format for performance purposes");


        logger.info("\nNotes:");
        logger.info("1- The PDB file must have all main-chain atoms (N,CA,C,O,CB). If CB is not present, its position will be estimated according to Eyal et al. (2004).");
        logger.info("2- A parameter file -Parameters.txt- must be in 'data' folder.");
        logger.info("3- The rotamer library must be in 'data/rotlibs' folder.");
        logger.info("4- The file with the list of AAs and their atoms, and the file with the data for atom generation must be in 'data/resData' folder.");
        logger.info("\nFor further information about parameters and other functions, see README.txt");
    }
}
