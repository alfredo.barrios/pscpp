package py.com.pol.biocomputacion.pscpp.energy.implementations;

import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.energy.interfaces.Energy;
import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.AtomS;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EnergyRASP implements Energy {
    //Aromatic aminoacids: HIS, PHE, TRP, TYR
    private final double cos37 = Math.cos(Math.toRadians(37.0));
    private final double cos47 = Math.cos(Math.toRadians(47.0));
    private final double den = ((1.0 - cos37) * (1.0 - cos47));
    private final int cysIndex, serIndex, thrIndex, tyrIndex, aspIndex, gluIndex;
    private final int cysSGIndex, cysCBIndex, serCBIndex, serOGIndex, thrCBIndex, thrOG1Index, tyrCZIndex, tyrOHIndex;
    private final int aspCGIndex, aspOD1Index, aspOD2Index, gluCDIndex, gluOE1Index, gluOE2Index, cAtomIndex, oAtomIndex;
    private static final String[] energyTerms= new String[]{"Etotal", "Eself", "EvdW", "Edb", "Ehb"};

    private final RotamerCollection rlc;
    public EnergyRASP(RotamerCollection rlc) {
        cysIndex = ListUtil.aaNamePosition.get("CYS");
        cysSGIndex = ListUtil.atomNamePosition.get(cysIndex).get("SG");
        cysCBIndex = ListUtil.atomNamePosition.get(cysIndex).get("CB");
        cAtomIndex = ListUtil.atomNamePosition.get(cysIndex).get("C");
        oAtomIndex = ListUtil.atomNamePosition.get(cysIndex).get("O");
        serIndex = ListUtil.aaNamePosition.get("SER");
        serCBIndex = ListUtil.atomNamePosition.get(serIndex).get("CB");
        serOGIndex = ListUtil.atomNamePosition.get(serIndex).get("OG");
        thrIndex = ListUtil.aaNamePosition.get("THR");
        thrCBIndex = ListUtil.atomNamePosition.get(thrIndex).get("CB");
        thrOG1Index = ListUtil.atomNamePosition.get(thrIndex).get("OG1");
        tyrIndex = ListUtil.aaNamePosition.get("TYR");
        tyrCZIndex = ListUtil.atomNamePosition.get(tyrIndex).get("CZ");
        tyrOHIndex = ListUtil.atomNamePosition.get(tyrIndex).get("OH");
        aspIndex = ListUtil.aaNamePosition.get("ASP");
        aspCGIndex = ListUtil.atomNamePosition.get(aspIndex).get("CG");
        aspOD1Index = ListUtil.atomNamePosition.get(aspIndex).get("OD1");
        aspOD2Index = ListUtil.atomNamePosition.get(aspIndex).get("OD2");
        gluIndex = ListUtil.aaNamePosition.get("GLU");
        gluCDIndex = ListUtil.atomNamePosition.get(gluIndex).get("CD");
        gluOE1Index = ListUtil.atomNamePosition.get(gluIndex).get("OE1");
        gluOE2Index = ListUtil.atomNamePosition.get(gluIndex).get("OE2");
        this.rlc = rlc;
    }

    @Override
    public String[] getEnergyTerms() {
        return energyTerms;
    }

    /**
     * van der Waals radii for RASP
     */
    private final double[][] vdwRadiiRASP =
            {
                    {1.75, 2.00, 2.00, 1.65, 1.95}, //ALA
                    {1.75, 2.00, 2.00, 1.65, 2.10, 2.10, 2.10, 1.70, 1.90, 1.70, 1.70}, //ARG
                    {1.75, 2.00, 2.00, 1.65, 2.10, 1.90, 1.60, 1.70}, //ASN
                    {1.75, 2.00, 2.00, 1.65, 2.10, 1.90, 1.55, 1.55}, //ASP
                    {1.75, 2.00, 2.00, 1.65, 1.90, 1.85}, //CYS
                    {1.75, 2.00, 2.00, 1.65, 2.10, 2.10, 1.90, 1.60, 1.70}, //GLN
                    {1.75, 2.00, 2.00, 1.65, 2.10, 2.10, 1.90, 1.55, 1.55}, //GLU
                    {1.75, 2.00, 2.00, 1.65}, //GLY
                    {1.75, 2.00, 2.00, 1.65, 2.10, 1.80, 1.70, 1.80, 1.80, 1.70}, //HIS
                    {1.75, 2.00, 2.00, 1.65, 2.15, 2.10, 1.95, 1.95}, //ILE
                    {1.75, 2.00, 2.00, 1.65, 2.10, 2.15, 1.95, 1.95}, //LEU
                    {1.75, 2.00, 2.00, 1.65, 2.10, 2.10, 2.10, 2.10, 1.70}, //LYS
                    {1.75, 2.00, 2.00, 1.65, 2.10, 2.10, 1.80, 1.95}, //MET
                    {1.75, 2.00, 2.00, 1.65, 2.10, 1.80, 1.80, 1.80, 1.80, 1.80, 1.80}, //PHE
                    {1.60, 2.00, 2.00, 1.65, 2.10, 2.10, 2.10}, //PRO
                    {1.75, 2.00, 2.00, 1.65, 2.10, 1.65}, //SER
                    {1.75, 2.00, 2.00, 1.65, 2.15, 1.65, 1.95}, //THR
                    {1.75, 2.00, 2.00, 1.65, 2.10, 1.80, 1.80, 1.80, 1.70, 1.80, 1.80, 1.80, 1.80, 1.80}, //TRP
                    {1.75, 2.00, 2.00, 1.65, 2.10, 1.80, 1.80, 1.80, 1.80, 1.80, 1.80, 1.65}, //TYR
                    {1.75, 2.00, 2.00, 1.65, 2.15, 1.95, 1.95}, //VAL
            };

    /**
     * welldepths for RASP
     */
    private final double[][] welldepths =
            {
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1811}, //ALA
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.1142, 0.1142, 0.2384, 0.12, 0.2384, 0.2384}, //ARG
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.12, 0.1591, 0.2384}, //ASN
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.12, 0.21, 0.21}, //ASP
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.16}, //CYS
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.1142, 0.12, 0.1591, 0.2384}, //GLN
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.1142, 0.12, 0.21, 0.21}, //GLU
                    {0.2384, 0.0486, 0.14, 0.1591}, //GLY
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.08, 0.2384, 0.08, 0.08, 0.2384}, //HIS
                    {0.2384, 0.0486, 0.14, 0.1591, 0.0486, 0.1142, 0.1811, 0.1811}, //ILE
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.0486, 0.1811, 0.1811}, //LEU
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.1142, 0.1142, 0.1142, 0.2384}, //LYS
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.1142, 0.16, 0.1811}, //MET
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08}, //PHE
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.1142, 0.1142}, //PRO
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.1591}, //SER
                    {0.2384, 0.0486, 0.14, 0.1591, 0.0486, 0.1591, 0.1811}, //THR
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.08, 0.08, 0.08, 0.2384, 0.08, 0.08, 0.08, 0.08, 0.08}, //TRP
                    {0.2384, 0.0486, 0.14, 0.1591, 0.1142, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.1591}, //TYR
                    {0.2384, 0.0486, 0.14, 0.1591, 0.0486, 0.1811, 0.1811}, //VAL
            };

    /**
     * Calculates the 'internal energy' of a residue with a certain rotamer
     *
     * @param aa
     * @param rotInd rotamer position in the list
     * @param rl     list of rotamers
     * @return
     */
    @Override
    public double calculateSelfEnergy(AminoAcidS aa, int rotInd, RotamerLibrary rl) {
        return (calculateRotLibEnergy(aa.aminoIndex, rotInd, rl) + calculateSelfHBEnergy(aa, aa.isEnd()));
    }

    /**
     * Calculates the energy from pair (residue) interactions
     *
     * @param aa1
     * @param aa2 //@param isFinalAA1 indicates if aa1 is at the end of a chain
     *            //@param isFinalAA2 indicates if aa2 is at the end of a chain
     * @return
     */
    @Override
    public double calculatePairEnergy(AminoAcidS aa1, int rotInd1, RotamerLibrary rl1, AminoAcidS aa2, int rotInd2, RotamerLibrary rl2) {
        return (calculateVdWEnergy(aa1, aa2) + calculateDisulfideBondEnergy(aa1, aa2) + calculatePairHBEnergy(aa1, aa2, aa1.isEnd(), aa2.isEnd()));
    }

    /**
     * Method that returns the internal energy of a residue, considering its rotamer probability in the rotamer library
     *
     * @param aminoIndex
     * @param rotInd     rotamer index
     * @param rl         rotamer library for the residue
     * @return
     */
    private double calculateRotLibEnergy(int aminoIndex, int rotInd, RotamerLibrary rl) {
        double p_rot = rl.getRotamers()[rotInd].getProbability();
        if (p_rot < 0.000001) p_rot = 0.000001; //for very low values
        double p_max = rl.getRotamers()[0].getProbability(); //it is assumed an ordered rotamer library (descending order of probability).
        return (-1.0) * waa[aminoIndex] * Math.log(p_rot / p_max);
    }

    //List of coefficients according to the amino acid type (indicated by amino index)
    private final double[] waa =
            {
                    0.0,    // ALA
                    1.40,   // ARG
                    0.90,   // ASN
                    1.30,   // ASP
                    1.00,   // CYS
                    1.10,   // GLN
                    1.20,   // GLU
                    0.0,    // GLY
                    1.90,   // HIS
                    1.00,   // ILE
                    1.10,   // LEU
                    1.00,   // LYS
                    1.10,   // MET
                    2.10,   // PHE
                    0.50,   // PRO
                    0.90,   // SER
                    1.50,   // THR
                    1.00,   // TRP
                    1.00,   // TYR
                    1.00,   // VAL
            };

    /**
     * Calculates the energy between two residues, using the van der Waals energy function defined in Miao(2011) for RASP
     *
     * @param aa1
     * @param aa2
     * @return
     */
    private double calculateVdWEnergy(AminoAcidS aa1, AminoAcidS aa2) {
        double energy = 0.0;
        int aminoIndex1 = aa1.aminoIndex;
        int aminoIndex2 = aa2.aminoIndex;
        AtomS a, b;
        double dist;
        double radii1, radii2, Rij, en, ratio, eij;
        AtomS[] atoms1 = aa1.atoms;
        AtomS[] atoms2 = aa2.atoms;
        double[] vdWRadiis1 = vdwRadiiRASP[aminoIndex1];
        double[] vdWRadiis2 = vdwRadiiRASP[aminoIndex2];
        double[] welldepths1 = welldepths[aminoIndex1];
        double[] welldepths2 = welldepths[aminoIndex2];
        for (int i = 0; i < atoms1.length; i++) {
            a = atoms1[i];
            if (a == null) continue;
            radii1 = vdWRadiis1[i];
            for (int j = 0; j < atoms2.length; j++) {
                b = atoms2[j];
                if (b == null) continue;
                if (i > 4 || j > 4) { //The interactions betweens BB atoms are not considered
                    radii2 = vdWRadiis2[j];
                    eij = Math.sqrt(welldepths1[i] * welldepths2[j]);
                    dist = AtomS.getDistance(a, b);
                    Rij = radii1 + radii2;
                    ratio = dist / Rij;
                    en = 0.0;
                    if (ratio < 0.465) en = 50 * eij;
                    else if (ratio >= 0.465 && ratio < 0.75) en = eij * (80 - (64.5 * ratio));
                    else if (ratio >= 0.75 && ratio < 0.8929)
                        en = 1.63 * eij * (Math.pow((1 / ratio), 12) - (2 * Math.pow((1 / ratio), 6)));
                    else if (ratio >= 0.8929 && ratio < 2.3)
                        en = 0.99 * eij * (Math.pow((1 / ratio), 12) - (2 * Math.pow((1 / ratio), 6)));
                    energy += en;
                }
            }
        }
        return energy;
    }

    /**
     * Calculates the energy of disulfide bonds for cysteine pair, according to Miao(2011) for RASP.
     *
     * @param aa1
     * @param aa2
     * @return
     */
    private double calculateDisulfideBondEnergy(AminoAcidS aa1, AminoAcidS aa2) {
        if (!(aa1.aminoIndex == cysIndex && aa2.aminoIndex == cysIndex)) return 0.0;

        AtomS sg1, sg2, cb1, cb2;
        //{"N", "CA", "C", "O", "CB", "SG"}, CYS
        sg1 = aa1.atoms[cysSGIndex]; //SG
        cb1 = aa1.atoms[cysCBIndex]; //CB
        if (sg1 == null || cb1 == null) return 0.0;
        sg2 = aa2.atoms[cysSGIndex]; //SG
        cb2 = aa2.atoms[cysCBIndex]; //CB
        if (sg2 == null || cb2 == null) return 0.0;
        double dist = AtomS.getDistance(sg1, sg2);
        double A1 = AtomS.getBondAngle(cb1, sg1, sg2);//CB1-SG1-SG2
        double A2 = AtomS.getBondAngle(sg1, sg2, cb2); //SG1-SG2-CB2
        double X3 = AtomS.getTorsionAngle(cb1, sg1, sg2, cb2); //CB1-SG1-SG2-CB2
        double t1 = Math.abs(dist - 2.06);
        double t2 = Math.abs(A1 - 105.0);
        double t3 = Math.abs(A2 - 105.0);
        double t4 = (t2 + t3) / 100.0;
        double t5 = Math.abs(Math.abs(X3) - 90.0) / 140.0;
        double S = (6 * (t1 + t4 + t5)) - 11.4;
        return S;
    }

    /**
     * Returns the energy from hydrogen bonds
     *
     * @param aa1
     * @param aa2
     * @param endAA1 1 if aa1 is the end of a chain
     * @param endAA2 1 if aa2 is the end of a chain
     * @return
     */
    private double calculatePairHBEnergy(AminoAcidS aa1, AminoAcidS aa2, boolean endAA1, boolean endAA2) {
        // SER=15 THR=16 TYR=18 - Hydroxyls
        // ASP=3 GLU=6 - Carboxyls
        double energy = 0.0;
        int aminoIndex1 = aa1.aminoIndex, aminoIndex2 = aa2.aminoIndex;
        if (isHydroxyl(aminoIndex1) && isCarboxyl(aminoIndex2)) energy += calculateHBEnergy(aa1, aa2);
        if (isCarboxyl(aminoIndex1) && isHydroxyl(aminoIndex2)) energy += calculateHBEnergy(aa2, aa1);
        if (isHydroxyl(aminoIndex1) && endAA2) energy += calculateHBEnergyBB(aa1, aa2);
        if (endAA1 && isHydroxyl(aminoIndex2)) energy += calculateHBEnergyBB(aa2, aa1);
        return energy;
    }

    /**
     * Returns the energy from hydrogen bonds between side-chain atoms
     *
     * @param aa1 amino acid with hydroxyl group
     * @param aa2 amino acid with carboxyl group
     */
    private double calculateHBEnergy(AminoAcidS aa1, AminoAcidS aa2) {
        AtomS b = null, od = null, oa1 = null, oa2 = null, c = null; //base donor, oxygen donor, oxygen acceptor, and carbon acceptor
        int aminoIndex1 = aa1.aminoIndex, aminoIndex2 = aa2.aminoIndex;

        //Hydroxyl atoms
        //SER=15 THR=16 TYR=18 - Hydroxyls
        if (aminoIndex1 == serIndex) {
            b = aa1.atoms[serCBIndex];
            od = aa1.atoms[serOGIndex];
        } //{"N", "CA", "C", "O", "CB", "OG"} SER
        if (aminoIndex1 == thrIndex) {
            b = aa1.atoms[thrCBIndex];
            od = aa1.atoms[thrOG1Index];
        } //{"N", "CA", "C", "O", "CB", "OG1", "CG2"}, THR
        if (aminoIndex1 == tyrIndex) {
            b = aa1.atoms[tyrCZIndex];
            od = aa1.atoms[tyrOHIndex];
        } //{"N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"}, TYR

        //Carboxyl atoms
        //ASP=3 GLU=6 - Carboxyls
        if (aminoIndex2 == aspIndex) {
            c = aa2.atoms[aspCGIndex];
            oa1 = aa2.atoms[aspOD1Index];
            oa2 = aa2.atoms[aspOD2Index];
        } //{"N", "CA", "C", "O", "CB", "CG", "OD1", "OD2"}, ASP
        if (aminoIndex2 == gluIndex) {
            c = aa2.atoms[gluCDIndex];
            oa1 = aa2.atoms[gluOE1Index];
            oa2 = aa2.atoms[gluOE2Index];
        } //{"N", "CA", "C", "O", "CB", "CG", "CD", "OE1", "OE2"}, GLU

        double en1 = getHBenergy(b, od, oa1, c);
        double en2 = getHBenergy(b, od, oa2, c);
        if (en1 < en2) return en1;
        else return en2;
    }

    /**
     * Method for calculating the energy from hydrogen bonds in an amino acid.
     *
     * @param aa    amino acid with hydroxyl group
     * @param isEnd indicates if the residue is at the end of a chain
     * @return
     */
    private double calculateSelfHBEnergy(AminoAcidS aa, boolean isEnd) {
        int aminoIndex = aa.aminoIndex;
        if (!(isHydroxyl(aminoIndex) && isEnd)) return 0.0;
        AtomS b = null, od = null, oa, c; //base donor, oxygen donor, oxygen acceptor, and carbon acceptor

        //Hydroxyl atoms
        //SER=15 THR=16 TYR=18 - Hydroxyls
        if (aminoIndex == serIndex) {
            b = aa.atoms[serCBIndex];
            od = aa.atoms[serOGIndex];
        } //{"N", "CA", "C", "O", "CB", "OG"} SER
        if (aminoIndex == thrIndex) {
            b = aa.atoms[thrCBIndex];
            od = aa.atoms[thrOG1Index];
        } //{"N", "CA", "C", "O", "CB", "OG1", "CG2"}, THR
        if (aminoIndex == tyrIndex) {
            b = aa.atoms[tyrCZIndex];
            od = aa.atoms[tyrOHIndex];
        } //{"N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"}, TYR

        //Carboxyl atoms - BB
        c = aa.atoms[cAtomIndex];
        oa = aa.atoms[oAtomIndex];
        return getHBenergy(b, od, oa, c);
    }

    /**
     * Calculates the energy from hydrogen bonds between a side-chain atom and a BB-atom
     *
     * @param aa1 amino acid with hydroxyl group
     * @param aa2 amino acid ending a chain (with carboxyl group in the BB)
     */
    private double calculateHBEnergyBB(AminoAcidS aa1, AminoAcidS aa2) {
        AtomS b = null, od = null, oa, c; //base donor, oxygen donor, oxygen acceptor, and carbon acceptor
        int aminoIndex1 = aa1.aminoIndex;

        //Hydroxyl atoms
        //SER=15 THR=16 TYR=18 - Hydroxyls
        if (aminoIndex1 == serIndex) {
            b = aa1.atoms[serCBIndex];
            od = aa1.atoms[serOGIndex];
        } //{"N", "CA", "C", "O", "CB", "OG"} SER
        if (aminoIndex1 == thrIndex) {
            b = aa1.atoms[thrCBIndex];
            od = aa1.atoms[thrOG1Index];
        } //{"N", "CA", "C", "O", "CB", "OG1", "CG2"}, THR
        if (aminoIndex1 == tyrIndex) {
            b = aa1.atoms[tyrCZIndex];
            od = aa1.atoms[tyrOHIndex];
        } //{"N", "CA", "C", "O", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"}, TYR

        //Carboxyl atoms - BB
        c = aa2.atoms[cAtomIndex];
        oa = aa2.atoms[oAtomIndex];
        return getHBenergy(b, od, oa, c);
    }

    /**
     * Returns the energy from hydrogen bonding, based in Miao(2011) for RASP
     *
     * @param b
     * @param od
     * @param oa
     * @param c
     * @return
     */
    private double getHBenergy(AtomS b, AtomS od, AtomS oa, AtomS c) {
        if (b == null || od == null || oa == null || c == null) return 0.0;
        if (AtomS.getDistance(od, oa) >= 3.2) return 0.0;
        double alpha = AtomS.getBondAngle(b, od, oa);
        double beta = AtomS.getBondAngle(c, oa, od);
        double num = (Math.cos(Math.toRadians(alpha - 111.5)) - cos37) * (Math.cos(Math.toRadians(beta - 120.0)) - cos47);
        double energy = (-1.8) * Math.sqrt(Math.abs(num / den));
        return energy;
    }

    /**
     * Indicates if a given amino acid type contains an hydroxyl group
     *
     * @param aminoIndex
     * @return
     */
    private boolean isHydroxyl(int aminoIndex) {
        // SER=15 THR=16 TYR=18 - Hydroxyls
        return (aminoIndex == serIndex) || (aminoIndex == thrIndex) || (aminoIndex == tyrIndex);
    }

    /**
     * Indicates if a given amino acid type contains a carboxyl group
     *
     * @param aminoIndex
     * @return
     */
    private boolean isCarboxyl(int aminoIndex) {
        // ASP=3 GLU=6 - Carboxyls
        return (aminoIndex == aspIndex) || (aminoIndex == gluIndex);
    }

    @Override
    public double[] calculateEnergies(StructureS s, Protein pRef) {
        double totalSelfEnergy=0.0, totalVDWEnergy=0.0, totalDisulfidBondEnergy=0.0, totalHBEnergy=0.0;
        Protein p = new Protein(s);
        p.rotLibs = pRef.rotLibs;
        p.assignRotamers("Best");
        p.obtainNeighbors();
        List<List<Integer>> neighbors = p.neighborList;
        List<Integer> PDBsToVerify;
        RotamerLibrary[] rotLibs = p.rotLibs;
        RotamerLibrary rl1;
        int[] rotIndices = p.rotamersIndex;
        AminoAcidS[] residues = p.origResidues;
        AminoAcidS aa1,aa2;
        boolean[] isChainEnd = p.chainEnd;
        for(int i=0; i<p.strucSize; i++){
            aa1 = residues[i];
            rl1 = rotLibs[i];
            if(rl1!=null){
                totalSelfEnergy += calculateRotLibEnergy(aa1.aminoIndex, rotIndices[i], rl1);
                totalHBEnergy += calculateSelfHBEnergy(aa1, isChainEnd[i]);
            }
            PDBsToVerify = neighbors.get(i);
            for(int j: PDBsToVerify){
                if(j>i){
                    aa2 = residues[j];
                    totalVDWEnergy += calculateVdWEnergy(aa1, aa2);
                    totalDisulfidBondEnergy += calculateDisulfideBondEnergy(aa1, aa2);
                    totalHBEnergy += calculatePairHBEnergy(aa1, aa2, isChainEnd[i], isChainEnd[j]);
                }
            }
        }

        double totalEnergy = totalSelfEnergy+totalVDWEnergy+totalDisulfidBondEnergy+totalHBEnergy;
        double[] energies = new double[5];
        energies[0] = totalEnergy;
        energies[1] = totalSelfEnergy;
        energies[2] = totalVDWEnergy;
        energies[3] = totalDisulfidBondEnergy;
        energies[4] = totalHBEnergy;
        return energies;
    }
}
