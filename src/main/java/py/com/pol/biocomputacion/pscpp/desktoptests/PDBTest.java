package py.com.pol.biocomputacion.pscpp.desktoptests;

import org.apache.commons.io.FilenameUtils;
import org.biojava.nbio.structure.*;
import org.biojava.nbio.structure.io.FileParsingParameters;
import org.biojava.nbio.structure.io.PDBFileParser;
import org.biojava.nbio.structure.io.PDBFileReader;
import py.com.pol.biocomputacion.pscpp.pdb.PDBReader;

import java.io.File;
import java.util.ArrayList;

public class PDBTest {

    public static void main(String[] args) {

        try {
            PDBFileReader reader=PDBReader.getReader();
            PDBFileParser parser = new PDBFileParser();
            FileParsingParameters params = new FileParsingParameters();
            // parse the C-alpha atoms only, default = false
            params.setParseCAOnly(false);
            // align the SEQRES and ATOM records, default = true
            // slows the parsing speed slightly down, so if speed matters turn it off.`
            params.setAlignSeqRes(false);
            // the parser can read the secondary structure`
            // assignment from the PDB file header and add it to the amino acids`
            params.setParseSecStruc(true);
            parser.setFileParsingParameters(params);
            // First argument: the full path of the pdb folder
            // args[0] = "/home/alfredo/Documents/2018/biocomputacion/NewDatasets/PDB65"
            File folder = new File(args[0]);
            if (folder != null) {
                for (File file : folder.listFiles()) {
                    if (file != null) {
                        String extension = FilenameUtils.getExtension(file.getAbsolutePath());
                        if ("pdb".equals(extension)) {
                            Structure structure = reader.getStructure(file);
                            //System.out.println("Pdb Code: " + structure.getPDBCode());
                            ArrayList<AminoAcid> aminoAcids;
                            ArrayList<ArrayList<AminoAcid>> chains = new ArrayList<>();
                            ArrayList<String> chainIDs = new ArrayList<>();
                            int resCount=0;
                            //Number of chains and lists of amino acids
                            for(Chain c: structure.getChains()){
                                aminoAcids = new ArrayList<>();
                                //List of amino acids in the chain
                                //Alternative Locations

                                for (Group g: c.getAtomGroups()) {
                                        if(g.isAminoAcid()) {
                                            //Alternative Locations
                                            if (g.hasAltLoc()) {
                                                AminoAcid aminoAcid = (AminoAcid) g;
                                                if(aminoAcid.getCA()==null || aminoAcid.getN()==null || aminoAcid.getO()==null || aminoAcid.getC()==null) {
                                                    System.out.println("ERROR: INCOMPLETE backbone in "+aminoAcid.getResidueNumber().toString()+"("+aminoAcid.getPDBName()+")");
                                                    System.exit(0);
                                                }
                                                if(aminoAcid.getCB() == null){
                                                    System.out.println("Adding CB atom to residue "+aminoAcid.getResidueNumber()+"("+aminoAcid.getPDBName()+")");
                                                    System.out.println(aminoAcid.getAtoms());

                                                }
                                                System.out.println("Type: "+aminoAcid.getAminoType()+"    "+aminoAcid.getAltLocs());
                                            }

                                        }
                                }
                                if(aminoAcids.isEmpty()){
                                    chains.add(aminoAcids);
                                    chainIDs.add(c.getChainID());
                                    resCount+=aminoAcids.size();
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
           System.err.println("PDB error : "+e.getMessage());
        }
    }
}

