package py.com.pol.biocomputacion.pscpp.rotlib.interfaces;

import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.DependentLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.IndependentLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.ProbabilityDependentLibrary;

/**
 * The interface provide an Abstraction between parsers.
 * */
public interface ParseLibraryLine {
    DependentLibrary parseDependentLine(String line);
    IndependentLibrary parseIndependentLine(String line);
    Double getProbabilityIndependentLibrary(String line); // Return the probability, given the Independent line
    ///ProbabilityDependentLibrary getProbabilityDependentLibrary(String line);  // Return the probability, given the Dependent line
}
