package py.com.pol.biocomputacion.pscpp.pdb;

import org.biojava.nbio.structure.*;
import org.biojava.nbio.structure.io.FileParsingParameters;
import org.biojava.nbio.structure.io.PDBFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.AtomS;
import py.com.pol.biocomputacion.pscpp.protein.ChainS;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PDBReader {
    private static final Logger logger = LoggerFactory.getLogger(PDBReader.class);
    private boolean isComplete;
    public ArrayList<String> pdbCodes;
    private static boolean printWarnings;
    public static PDBFileReader pdbFileReader;


    private static final int ALAINDEX, NATOMINDEX, CAATOMINDEX, CATOMINDEX, OATOMINDEX, CBATOMINDEX;

    static {
        ALAINDEX = ListUtil.aaNamePosition.get("ALA");
        NATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("N");
        CAATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("CA");
        CATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("C");
        OATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("O");
        CBATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("CB");
    }
    /**
     * Read a list of PDB codes as String
     * @param filename List filename
     * @return a List of PDB codes
     */
    public static List<String> readPDBList(String filename) throws Exception{
        BufferedReader bf=null;
        List<String> pdbList=new ArrayList<>();

        try {
            bf = new BufferedReader(new FileReader(filename));
            String line = bf.readLine();
            while(line!=null){
                pdbList.add(line.toUpperCase());
                line = bf.readLine();
            }
        } catch (IOException ex) {
            logger.error("Problem reading the protein list {} ", filename, ex);
            System.exit(0);
        } finally {
            if(bf!=null)
                bf.close();
            return pdbList;
        }
    }

    public PDBReader() {
        pdbFileReader = getReader();
    }

    /**
     * @return a Reader for PDB Files
     * */
    public static PDBFileReader getReader(){
        printWarnings=(Boolean) ParameterFile.getParameters().get(ParameterFile.BOOL_PRINT_WARNINGS_PDB);
        PDBFileReader pdbreader = new PDBFileReader();
        // configure the parameters of file parsing
        FileParsingParameters params = new FileParsingParameters();
        // parse the C-alpha atoms only, default = false
        params.setParseCAOnly(false);
        // align the SEQRES and ATOM records, default = true
        // slows the parsing speed slightly down, so if speed matters turn it off.`
        params.setAlignSeqRes(false);
        // the parser can read the secondary structure`
        // assignment from the PDB file header and add it to the amino acids`
        params.setParseSecStruc(true);
        pdbreader.setFileParsingParameters(params);
        return pdbreader;
    }

    /**
     * Parse the data from the PDB file of a protein.
     * @param pdbFilename Protein PDB filename
     * @return
     */
    private StructureS readStructureWithBioJava(String pdbFilename, String pdbID) {
        try{
            return cleanStructure(pdbFileReader.getStructure(pdbFilename), pdbID);
        } catch (Exception ex){
            logger.error("Error loading {}", pdbFilename);
            logger.error("Error: {}", ex.getMessage());
            System.exit(0);
            return null;
        }
    }
    /**
     * 'Filter' a structure parsed with BioJava, translating the objects from BioJava classes (Structure, Chain, AminoAcid, Atom) to ours.
     * Returns null if the structure has missing backbone atoms
     * @param sIn Input BioJava structure
     * @return
     */
    private StructureS cleanStructure(Structure sIn, String pdbID){
        ArrayList<ArrayList<AminoAcid>> chains = new ArrayList<>();
        ArrayList<String> chainIDs = new ArrayList<>();
        ArrayList<AminoAcid> aminoAcids;
        int resCount=0;

        //Number of chains and lists of amino acids
        for(Chain c: sIn.getChains()){
            aminoAcids = new ArrayList<>();
            //List of amino acids in the chain
            for (Group g: c.getAtomGroups()) {
                if(g instanceof AminoAcid && ListUtil.isAminoAcidinList(g.getPDBName())){
                    aminoAcids.add((AminoAcid) g);
                }
            }
            if(aminoAcids.size()>0){
                chains.add(aminoAcids);
                chainIDs.add(c.getChainID());
                resCount+=aminoAcids.size();
            }
        }

        //Process of "translation"
        pdbCodes = new ArrayList<>(resCount);
        StructureS sOut = new StructureS(chains.size());
        sOut.pdbID = pdbID;
        isComplete = true;

        AminoAcid aaIn;
        AminoAcidS aaOut;
        resCount=1;
        int countChain;
        for(int chainNum=0; chainNum<chains.size(); chainNum++){
            aminoAcids = chains.get(chainNum);
            sOut.chains[chainNum] = new ChainS(aminoAcids.size());
            sOut.chains[chainNum].chainID = chainIDs.get(chainNum);
            countChain=0;
            for(int i=0; i<aminoAcids.size(); i++){
                aaIn = aminoAcids.get(i);
                pdbCodes.add(aaIn.getResidueNumber().toString());
                aaOut = parseBioJavaAminoAcid(aaIn, resCount++);
                //Check missing BB atoms
                if(aaOut.atoms[NATOMINDEX]==null || aaOut.atoms[CAATOMINDEX]==null || aaOut.atoms[CATOMINDEX]==null || aaOut.atoms[OATOMINDEX]==null){
                    logger.error("ERROR: INCOMPLETE backbone in {} , pdb: {} ",aaIn.getResidueNumber(),aaIn.getPDBName());
                    return null;
                }
                //Add missing CB atom
                if(aaOut.atoms.length>4){
                    if(aaOut.atoms[CBATOMINDEX]==null){
                        if(printWarnings)logger.info("Adding CB atom to residue {}  pdb: {} ",aaIn.getResidueNumber(), aaIn.getPDBName());
                        aaOut.atoms[CBATOMINDEX] = createCB(aaOut.atoms[NATOMINDEX], aaOut.atoms[CAATOMINDEX], aaOut.atoms[CATOMINDEX], aaOut.aminoIndex);
                    }
                }
                //Check missing SC atoms
                checkMissingSCAtoms(aaOut, resCount-2);
                //Add created AminoAcidS object
                aaOut.setIsStart(i==0);
                aaOut.setIsEnd((i == aminoAcids.size()-1));
                sOut.chains[chainNum].residues[countChain++] = aaOut;
            }
        }
        sOut.isComplete=isComplete;
        return sOut;
    }

    /**
     * Read a structure given the folder of its PDBFile and its ID
     * @param folder
     * @param pdbID
     * @return
     */
    public StructureS readStructure(String folder, String pdbID) {
        String pdb;
        logger.info("ID: {}", pdbID);
        if(folder==null) pdb = pdbID.toUpperCase();
        else pdb = folder+File.separator+pdbID;
        String pdbFilenameShort = pdb+".pdb.short";
        String pdbNotHETATM = pdb+".nh";
        File f = new File(pdbFilenameShort);
        if(f.exists()){
            logger.info("Reading {}.pdb.short...   ",pdb);
            return readShortStructure(pdbFilenameShort, pdbID);
        }
        else{
            if(printWarnings) logger.info("Short version of {} not present.", pdbID);
            logger.info("Reading {}.pdb... ", pdb);
            filterHETATMRecords(pdb);
            StructureS struct = readStructureWithBioJava(pdbNotHETATM, pdbID);
            PDBWriter.writeShortPDB(struct, pdbFilenameShort, pdbCodes);
            System.gc(); //BioJava probably keeps the stream open...
            File f2 = new File(pdbNotHETATM);
            f2.delete();
            return struct;
        }
    }
    /**
     * Discard HETATM records from a PDB File and saves them in a new file.
     * @param pdbName
     */
    private static void filterHETATMRecords(String pdbName){
        try {
            BufferedReader bf = new BufferedReader(new FileReader(pdbName+".pdb"));
            File fileOut = new File(pdbName+".nh");
            PrintWriter outFile = new PrintWriter(new BufferedWriter(new FileWriter(fileOut)));
            String line = bf.readLine();
            while(line!=null){
                if(line.length()>6){
                    if(!line.substring(0,6).equalsIgnoreCase("HETATM")) outFile.println(line);
                }
                else outFile.println(line);
                line = bf.readLine();
            }
            bf.close();
            outFile.close();
        } catch (IOException ex) {
            logger.error("Error filtering HETATM records from  {}.pdb",pdbName);
            System.exit(0);
        }
    }

    /**
     * Parse the data from the PDB file (in short format) of a protein.
     * @param pdbFilename
     * @param pdbID
     * @return
     */
    private StructureS readShortStructure(String pdbFilename, String pdbID){
        try {
            BufferedReader bf = new BufferedReader(new FileReader(pdbFilename));
            String delims = " ";
            String line = bf.readLine();
            String[] fields = line.split(delims);
            StructureS struct = new StructureS(Integer.parseInt(fields[0]));
            pdbCodes = new ArrayList<>(Integer.parseInt(fields[1]));
            struct.pdbID = pdbID;
            ChainS chain;
            AminoAcidS residue;
            int resCount=1,numAtoms,position;
            Map<String,Integer> atomNamePosition;
            double x,y,z;
            isComplete = true;
            for(int i=0; i<struct.chains.length; i++){
                line = bf.readLine();
                fields = line.split(delims);
                struct.chains[i] = new ChainS(Integer.parseInt(fields[1]));
                chain = struct.chains[i];
                chain.chainID = fields[0];
                for(int j=0; j<chain.residues.length; j++){
                    line = bf.readLine();
                    fields = line.split(delims);
                    chain.residues[j] = new AminoAcidS(ListUtil.aaNamePosition.get(fields[0]), resCount++);
                    residue = chain.residues[j];
                    pdbCodes.add(fields[1]);
                    atomNamePosition = ListUtil.atomNamePosition.get(residue.aminoIndex);
                    numAtoms = Integer.parseInt(fields[2]);
                    for(int k=0; k<numAtoms; k++){
                        line = bf.readLine();
                        fields = line.split(delims);
                        position = atomNamePosition.get(fields[0]);
                        x = Double.parseDouble(fields[1]);
                        y = Double.parseDouble(fields[2]);
                        z = Double.parseDouble(fields[3]);
                        residue.atoms[position] = new AtomS(x, y, z);
                    }
                    //Check missing SC atoms
                    checkMissingSCAtoms(residue, resCount-2);
                    chain.residues[j].setIsStart(j==0);
                    chain.residues[j].setIsEnd(j==(chain.residues.length-1));
                }
            }
            bf.close();
            struct.isComplete=isComplete;
            return struct;
        } catch (IOException ex) {
            logger.error("Error loading {}", pdbFilename);
            System.exit(0);
            return null;
        }
    }
    /**
     * Method for checking SC atoms (since all BB atom must be present)
     * @param aaOut
     * @param position
     */
    private void checkMissingSCAtoms(AminoAcidS aaOut, int position){
        String[] atomList = ListUtil.atomGenOrder[aaOut.aminoIndex];
        for(int i=0; i<atomList.length; i++){
            if(aaOut.atoms[i]==null){
                if(printWarnings) logger.info("Warning: Missing {} atom in residue {}  amino: {}",atomList[i],pdbCodes.get(position),ListUtil.aaThreeLetterCode[aaOut.aminoIndex]);
                isComplete = false;
            }
        }
    }

    /**
     * Method for passing the data from BioJava.AminoAcid format to AminoAcidS (selecting atoms with highest ocurrence)
     * @param aaIn
     * @param aaID
     * @return
     */
    private AminoAcidS parseBioJavaAminoAcid(AminoAcid aaIn, int aaID){
        int aminoAcidIndex = ListUtil.aaNamePosition.get(aaIn.getPDBName());
        AminoAcidS aaOut = new AminoAcidS(aminoAcidIndex, aaID);
        Integer position;
        Map<String,Integer> atomNamePosition = ListUtil.atomNamePosition.get(aminoAcidIndex);
        if(!aaIn.hasAltLoc()){
            for(Atom at: aaIn.getAtoms()){
                position = atomNamePosition.get(at.getName());
                if(position!=null) aaOut.atoms[position] = new AtomS(at.getX(), at.getY(), at.getZ());
            }
        }
        else{
            int numAtoms = ListUtil.atomGenOrder[aminoAcidIndex].length;
            ArrayList<ArrayList<Atom>> allAtoms = new ArrayList<>(numAtoms);
            for(int i=0; i<numAtoms; i++) allAtoms.add(new ArrayList<>());
            for(Atom at: aaIn.getAtoms()){
                position = atomNamePosition.get(at.getName());
                if(position!=null) allAtoms.get(position).add(at);
            }
            //Alternative Locations
            for(Group g: aaIn.getAltLocs()){
                for(Atom at: g.getAtoms()){
                    position = atomNamePosition.get(at.getName());
                    if(position!=null) allAtoms.get(position).add(at);
                }
            }
            //Atoms with highest occupancy are selected
            ArrayList<Atom> atoms;
            double occ, maxOcc;
            for(int i=0; i<numAtoms; i++){
                atoms = allAtoms.get(i);
                if(!atoms.isEmpty()){
                    Atom highestOccAtom = atoms.get(0);
                    maxOcc = atoms.get(0).getOccupancy();
                    for(int j=1; j<atoms.size(); j++){
                        occ = atoms.get(j).getOccupancy();
                        if(maxOcc<occ){
                            maxOcc = occ;
                            highestOccAtom = atoms.get(j);
                        }
                    }
                    aaOut.atoms[i] = new AtomS(highestOccAtom.getX(), highestOccAtom.getY(), highestOccAtom.getZ());
                }
            }
        }
        return aaOut;
    }

    /**
     * This method adds missing CB atoms
     * @param aN
     * @param aCA
     * @param aC
     * @param aminoIndex
     * @return AtomS
     */
    private AtomS createCB(AtomS aN, AtomS aCA, AtomS aC, int aminoIndex){
        double bondData[] = ListUtil.atomGenData[aminoIndex][CBATOMINDEX].bondValues;
        AtomS aCB = AtomS.getAtom(aN, aC, aCA, bondData[2], bondData[1], bondData[0]);
        return aCB;
    }

    /**
     * Returns the list with relative SASA values for each residue, given the protein PDB fileName and the number of residues
     * @param numRes
     * @param pdbFileName
     * @return
     */
    public double[] getSolventAccess(int numRes, String pdbFileName){
        try {
            List<String> lines = new ArrayList<>(numRes);
            BufferedReader bf = new BufferedReader(new FileReader(pdbFileName));
            String line = bf.readLine();
            while(line!=null){
                lines.add(line);
                line = bf.readLine();
            }
            bf.close();
            if(numRes!=lines.size()){
                logger.error("PROBLEM: different number of SASAs and residues: \t {} \t  lines: {} ", numRes,lines.size());
                System.exit(0);
            }
            double[] perc = new double[numRes];
            String delims = "\t";
            String[] tokens;
            for(int i=0;i<numRes;i++){
                tokens = lines.get(i).split(delims);
                perc[i] = Double.parseDouble(tokens[3]); //position of SASA value
            }
            return perc;
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("Problem reading SASAs");
            System.exit(0);
            return null;
        }
    }

}
