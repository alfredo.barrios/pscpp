package py.com.pol.biocomputacion.pscpp.desktoptests;

import com.esotericsoftware.kryo.io.Input;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;

import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;

public class DeserializeRotamerFile {
    static final HashMap<String, RotamerLibrary> libraryList = new HashMap<>();

    public static void main(String[] args)throws Exception {
        long t1=System.currentTimeMillis();
        //Paht of the Dunbrak raw library
        // args[0] = "dependent_rotamers.dat"
        Input in=new Input(new FileInputStream(new RandomAccessFile(args[0],"r").getFD()));
        int size=in.readInt(true);
        while (!in.end()){
            RotamerLibrary rotamerLibrary=new RotamerLibrary();
            String key=in.readString();
            rotamerLibrary.setKey(key);
            int rotamersize=in.readInt();
            Rotamer[] rotamerarray=new Rotamer[rotamersize];
            for(int j=0;j<rotamersize;j++){
                Rotamer rotamer=new Rotamer();
                double probability=in.readDouble();
                rotamer.setProbability(probability);
                double[] chi=new double[4];
                for(int k=0;k<4;k++)
                    chi[k]=in.readDouble();
                rotamer.setChi(chi);
                rotamerarray[j]=rotamer;
            }
            rotamerLibrary.setRotamers(rotamerarray);
            libraryList.put(rotamerLibrary.getKey(),rotamerLibrary);
        }
        long t2=System.currentTimeMillis();
        long t3=System.nanoTime();
        RotamerLibrary element=libraryList.get("PRO;30.0;20.0");
        long t4=System.nanoTime();
        System.out.println("Time To Load All the Libary: "+(t2-t1)+"ms");
        System.out.println("Library Size: "+libraryList.size());
        System.out.println("Time to Find One Element: "+(t4-t3)+"ns");
        System.out.println("Element: "+element);
        in.close();
    }
}
