package py.com.pol.biocomputacion.pscpp.protein;

import java.util.ArrayList;
import java.util.List;

/**
 * This class deals with the data and operations for an atom (or atoms).
 *
 * @author jcolbes
 */
public class AtomS {
    public final double x, y, z;

    /**
     * Constructor - Only coordinates as input
     *
     * @param x
     * @param y
     * @param z
     */
    public AtomS(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Method for cloning an object from AtomS
     *
     * @return
     */
    public AtomS cloneAtomS() {
        AtomS b = new AtomS(this.x, this.y, this.z);
        return b;
    }

    public void print() {
        System.out.println(this.x + "\t" + this.y + "\t" + this.z);
    }

    /**
     * Method to obtain the magnitude of the position vector of an atom.
     *
     * @param a
     * @return
     */
    private static double getMagnitude(AtomS a) {
        return Math.sqrt((a.x) * (a.x) + (a.y) * (a.y) + (a.z) * (a.z));
    }

    /**
     * This method gets a unit vector from the position vector of an atom
     *
     * @param a
     * @return
     */
    private static AtomS unitVector(AtomS a) {
        double mod = getMagnitude(a);
        AtomS b = new AtomS((a.x / mod), (a.y / mod), (a.z / mod));
        return b;
    }

    /**
     * This method gets the difference vector of the position vectors of two atoms
     *
     * @param a
     * @param b
     * @return
     */
    private static AtomS subtract(AtomS a, AtomS b) { //A-B ó BA
        double x = (a.x - b.x);
        double y = (a.y - b.y);
        double z = (a.z - b.z);
        AtomS dif = new AtomS(x, y, z);
        return dif;
    }

    /**
     * This method gets the distance between two atoms
     *
     * @param a
     * @param b
     * @return
     */
    public static double getDistance(AtomS a, AtomS b) {
        double dist;
        dist = getMagnitude(subtract(a, b));
        return dist;
    }

    /**
     * This method calculates the scalar product of the position vectors of two atoms
     *
     * @param n1
     * @param n2
     * @return
     */
    private static double scalarProduct(AtomS n1, AtomS n2) {
        return ((n1.x) * (n2.x) + (n1.y) * (n2.y) + (n1.z) * (n2.z));
    }

    /**
     * This method gets the angle between the position vectors of two atoms
     *
     * @param n1
     * @param n2
     * @return
     */
    private static double getAngle(AtomS n1, AtomS n2) {
        AtomS a = AtomS.unitVector(n1);
        AtomS b = AtomS.unitVector(n2);
        double prodEsc = scalarProduct(a, b);
        if (prodEsc > 1.0) prodEsc = 1.0;
        else if (prodEsc < (-1.0)) prodEsc = (-1.0);
        double ang = Math.acos(prodEsc) * 180 / Math.PI;
        return ang;
    }

    /**
     * This method returns the vector product of the position vectors of two atoms
     *
     * @param a
     * @param b
     * @return
     */
    private static AtomS vectorProduct(AtomS a, AtomS b) {
        double x = (a.y) * (b.z) - (a.z) * (b.y);
        double y = (a.z) * (b.x) - (a.x) * (b.z);
        double z = (a.x) * (b.y) - (a.y) * (b.x);
        AtomS n = new AtomS(x, y, z);
        return n;
    }

    /**
     * This method calculates the angle between two bonds (two difference vectors)
     *
     * @param a
     * @param b Vertex
     * @param c
     * @return
     */
    public static double getBondAngle(AtomS a, AtomS b, AtomS c) {
        AtomS ba = AtomS.subtract(a, b);
        AtomS bc = AtomS.subtract(c, b);
        return getAngle(ba, bc);
    }

    /**
     * This method returns the torsion angle in bc
     *
     * @param a
     * @param b
     * @param c
     * @param d
     * @return
     */
    public static double getTorsionAngle(AtomS a, AtomS b, AtomS c, AtomS d) {
        AtomS ab = AtomS.subtract(a, b); //BA
        AtomS cb = AtomS.subtract(c, b); //BC
        AtomS bc = AtomS.subtract(b, c); //CB
        AtomS dc = AtomS.subtract(d, c); //CD

        AtomS abc = AtomS.vectorProduct(ab, cb);
        AtomS bcd = AtomS.vectorProduct(bc, dc);

        double angl = AtomS.getAngle(abc, bcd); //angl is the dihedral angle between atoms a,b,c, and d

        AtomS vecprod = AtomS.vectorProduct(abc, bcd);
        double val = AtomS.scalarProduct(cb, vecprod);
        if (val < 0.0) angl = -angl;

        return angl;
    }

    /**
     * This method returns a position vector of an atom d, based on the torsion angle of bc, the angle bcd and the lenght of cd (Parsons, 2005)
     *
     * @param a          reference atoms
     * @param b
     * @param c
     * @param torsion_bc torsion angle in bc
     * @param angle_bcd  angle between cb and cd
     * @param bond_cd    lenght of bond cd
     * @return vector position of d
     */
    static public AtomS getAtom(AtomS a, AtomS b, AtomS c, double torsion_bc, double angle_bcd, double bond_cd) {
        angle_bcd = (180 - angle_bcd) * Math.PI / 180;
        torsion_bc = torsion_bc * Math.PI / 180;

        AtomS AB = AtomS.subtract(b, a);
        AtomS BC = AtomS.subtract(c, b);
        AtomS bc = AtomS.unitVector(BC);
        AtomS n = AtomS.unitVector(AtomS.vectorProduct(AB, bc));

        AtomS Mx = bc;
        AtomS My = AtomS.unitVector(AtomS.vectorProduct(n, bc));
        AtomS Mz = n;

        double x2, y2, z2;
        x2 = bond_cd * Math.cos(angle_bcd);
        y2 = bond_cd * Math.cos(torsion_bc) * Math.sin(angle_bcd);
        z2 = bond_cd * Math.sin(torsion_bc) * Math.sin(angle_bcd);
        AtomS D2 = new AtomS(x2, y2, z2);

        double x, y, z;
        x = Mx.x * D2.x + My.x * D2.y + Mz.x * D2.z + c.x;
        y = Mx.y * D2.x + My.y * D2.y + Mz.y * D2.z + c.y;
        z = Mx.z * D2.x + My.z * D2.y + Mz.z * D2.z + c.z;
        AtomS D = new AtomS(x, y, z);
        return D;
    }

    /**
     * This method measures the RMSD between two sets of atoms
     *
     * @param alAtom1
     * @param alAtom2
     * @return
     */
    public static double getRMSD(List<AtomS> alAtom1, List<AtomS> alAtom2) {
        if (alAtom1.size() != alAtom2.size()) {
            System.out.println("Different size of atom sets for RMSD");
            return -1.0;
        }
        int numAtoms = alAtom1.size();
        double sum = 0.0, dist;
        for (int i = 0; i < numAtoms; i++) {
            dist = AtomS.getDistance(alAtom1.get(i), alAtom2.get(i));
            sum += (dist * dist);
        }
        return Math.sqrt(sum / numAtoms);
    }

    @Override
    public String toString() {
        return "AtomS{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
