package py.com.pol.biocomputacion.pscpp.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ListUtil {
    private static final Logger logger = LoggerFactory.getLogger(ListUtil.class);


    public static String[][] atomList; //atom lists for each residue (for writing PDB files)
    public static String[][] atomGenOrder; //order of creation of atoms for each residue
    public static String[] aaThreeLetterCode; //three-letter code identifier for each residue
    public static double[][] vdWRadii; //van der Waals radii for each residue (according to atomGenOrder)
    public static double[] maxVdWRadii; //maximum van der Waals radii for each residue
    public static double[] maxCADistances; //maximum distance from an atom to CA for every residue
    public static int[] numSidechainTorsion; //number of torsion angles for each residue
    public static int resNum; //number of considered residues
    public static AtomData[][] atomGenData; //data necessary for the creation of atoms in each residue (according to atomGenOrder)
    public static Map<String,Integer> aaNamePosition;
    public static List<Map<String,Integer>> atomNamePosition;

    /**
     * Method for reading all the information necessary for the process
     */
    public static void initializeLists(){
        readAtomList((String) ParameterFile.getParameters().get(ParameterFile.STR_LIST_ATOMS));
        readAtomGenData((String) ParameterFile.getParameters().get(ParameterFile.STR_ATOM_GENERATION_DATA));
        readMaxDistances((String)ParameterFile.getParameters().get(ParameterFile.STR_MAX_CA_DISTANCE));
    }
    /**
     * Parse the file with the atom list for each type of residue
     * @param atomListFilename
     */
    private static void readAtomList(String atomListFilename){
        BufferedReader bf = null;
        try {
            bf= new BufferedReader(new FileReader(atomListFilename));
            String line = bf.readLine();
            resNum = Integer.parseInt(line); line = bf.readLine();
            int atomNum;
            aaThreeLetterCode = new String[resNum];
            atomList = new String[resNum][];
            String delims = " ";
            String[] fields;
            int i=0;
            aaNamePosition = new ConcurrentHashMap<>();
            while(line!=null){
                fields = line.split(delims);
                aaThreeLetterCode[i] = fields[1];
                atomNum = fields.length-2;
                atomList[i] = new String[atomNum];
                for(int j=0; j<atomNum; j++) atomList[i][j] = fields[j+2];
                line=bf.readLine();
                i++;
            }
            for(i=0; i<resNum; i++) aaNamePosition.put(aaThreeLetterCode[i], i);
        } catch (Exception ex) {
           logger.error("Problem loading the file: {}",atomListFilename);
            System.exit(0);
        }finally {
            if(bf!=null) {
                try {
                    bf.close();
                } catch (IOException e) {
                   logger.error("Error Closing AtomListFilename : {}", e);
                }
            }

        }
    }
    /**
     * Parse the file with the data for amino acid 'construction' and number of torsion angles for each type of residue
     * @param atomDataFilename
     */
    private static void readAtomGenData(String atomDataFilename){
        try {
            BufferedReader bf = new BufferedReader(new FileReader(atomDataFilename));
            int count,  numAtoms, aminoIndex, size;
            String delims = " ", atomName, atomRefName;
            String[] fields;
            int[] refAtomPos;
            double[] bondVal;
            atomGenData = new AtomData[resNum][];
            atomGenOrder = new String[resNum][];
            vdWRadii = new double[resNum][];
            maxVdWRadii = new double[resNum];
            numSidechainTorsion = new int[resNum];
            double maxvdWRad;
            String line = bf.readLine();
            while(line!=null){
                if(line.length()==0){ line = bf.readLine(); continue;}
                if(line.substring(0,3).equalsIgnoreCase("RES")){
                    fields = line.split(delims);
                    if(!isAminoAcidinList(fields[1])){
                        logger.error("The three-letter code of this amino acid is not in the list.");
                        System.exit(1);
                    }
                    aminoIndex = aaNamePosition.get(fields[1]);
                    numAtoms = Integer.parseInt(fields[3]);
                    if(numAtoms!=ListUtil.atomList[aminoIndex].length){
                        logger.error("The number of atoms in the list of atoms for each amino acid and the one in atom data generation does not match for residue: {} ", ListUtil.aaThreeLetterCode[aminoIndex]);
                        System.exit(1);
                    }
                    numSidechainTorsion[aminoIndex] = Integer.parseInt(fields[5]);
                    vdWRadii[aminoIndex] = new double[numAtoms];
                    atomGenData[aminoIndex] = new AtomData[numAtoms];
                    atomGenOrder[aminoIndex] = new String[numAtoms];
                    maxvdWRad=(-1.0);
                    for(int i=0; i<numAtoms; i++){
                        line = bf.readLine();
                        fields = line.split(delims);
                        vdWRadii[aminoIndex][i] = Double.parseDouble(fields[1]);
                        if(vdWRadii[aminoIndex][i]>maxvdWRad) maxvdWRad = vdWRadii[aminoIndex][i];
                        atomName = fields[0];
                        atomGenOrder[aminoIndex][i] = atomName;
                        if(i>3){ //From CB to the last atom
                            count=2;
                            if(fields.length==8) size=3;
                            else size=4;
                            refAtomPos = new int[size];
                            bondVal = new double[3];
                            for(int k=0; k<size; k++){
                                atomRefName = fields[k+2];
                                for(int j=0; j<i; j++){
                                    if(atomRefName.equalsIgnoreCase(atomGenOrder[aminoIndex][j])){
                                        refAtomPos[k]=j;
                                        break;
                                    }
                                }
                            }
                            count+=size;
                            bondVal[0] = Double.parseDouble(fields[count]); count++;
                            bondVal[1] = Double.parseDouble(fields[count]); count++;
                            if(i==4 || i>(4+numSidechainTorsion[aminoIndex])) bondVal[2] = Double.parseDouble(fields[count]);
                            atomGenData[aminoIndex][i] = new AtomData(refAtomPos, bondVal);
                        }
                        else atomGenData[aminoIndex][i] = new AtomData(null, null);

                    }
                    maxVdWRadii[aminoIndex] = maxvdWRad;
                }
                line = bf.readLine();
            }
            bf.close();
            atomNamePosition = new ArrayList(resNum);
            int atomNum;
            for(int i=0; i<resNum; i++){
                atomNum = atomGenOrder[i].length;
                atomNamePosition.add(new ConcurrentHashMap<>());
                for(int j=0; j<atomNum; j++) atomNamePosition.get(i).put(atomGenOrder[i][j], j);
            }
        } catch (Exception ex) {
            logger.error("Problem loading the file: {}", atomDataFilename);
            System.exit(1);
        }
    }

    /**
     * Indicates if a three-letter-code string corresponds to a considered amino acid
     * @param aminoAcid
     * @return true to a considered amino acid (i.e. is present in ATOM_LIST)
     */
    public static boolean isAminoAcidinList(String aminoAcid) {
        Integer i = aaNamePosition.get(aminoAcid);
        if(i==null) {
           logger.error("{} is not in the list of considered amino acids",aminoAcid);
           return false;
        }
        else return true;
    }

    /**
     * Parse the file with max distances to CA for every type of residue
     */
    private static void readMaxDistances(String maxCADistanceFilename){
        BufferedReader bf = null;
        try {
            ListUtil.maxCADistances = new double[resNum];
            bf = new BufferedReader(new FileReader(maxCADistanceFilename));
            String delims = " ";
            String[] fields;
            int aminoIndex;
            String line = bf.readLine();
            while(line!=null){
                fields = line.split(delims);
                if(!isAminoAcidinList(fields[0])){
                    logger.error("The three-letter code of this amino acid is not in the list.");
                    System.exit(0);
                }
                aminoIndex = aaNamePosition.get(fields[0]);
                ListUtil.maxCADistances[aminoIndex] = (Double.parseDouble(fields[1]));
                line = bf.readLine();
            }
            bf.close();
        } catch (Exception ex) {
            logger.error("Problem loading the file: {} ",maxCADistanceFilename);
            System.exit(1);
        }finally {
            if(bf != null) {
               try {
                   bf.close();
               }catch (Exception e ) {
                    logger.error("Error Closing the file {} ", maxCADistanceFilename);
                    System.exit(1);
               }
            }
        }
    }
}
