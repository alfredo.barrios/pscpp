package py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess;

import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;

/**
 * Represents a Dependent Library with a key and a single rotamer.
 */
public class DependentLibrary {

    DependentKey key;
    Rotamer rotamer;

    public DependentKey getKey() {
        return key;
    }

    public void setKey(DependentKey key) {
        this.key = key;
    }

    public Rotamer getRotamer() {
        return rotamer;
    }

    public void setRotamer(Rotamer rotamers) {
        this.rotamer = rotamers;
    }


    @Override
    public String toString() {
        return "DependentLibrary{" +
                "key=" + key +
                ", rotamer=" + rotamer +
                '}';
    }
}
