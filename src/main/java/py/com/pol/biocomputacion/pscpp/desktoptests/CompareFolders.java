package py.com.pol.biocomputacion.pscpp.desktoptests;

import java.io.File;

/**
 * Utility main program
 * */
public class CompareFolders {
    public static void main(String[] args) {
        try {
            File folderA = new File(args[0]);
            File folderB = new File(args[1]);
            System.out.println();
            for (File file : folderA.listFiles()) {
                System.out.println("Comparing: "+file.getCanonicalPath()+ " with: "+ folderB.getAbsolutePath()+"/"+file.getName());
                boolean result = CompareTwoFiles.compare(file.getCanonicalPath(),folderB.getAbsolutePath()+"/"+file.getName());
                System.out.println("Result: "+ result);
                if(!result){
                    System.out.println("Error, the last files are not equal");
                    System.exit(-1);
                }
            }
            System.out.println("All the files arer equal !!!");
        }catch (Exception e){
           System.out.println("Error comparing the folders :"+ e.getMessage());
        }
    }
}
