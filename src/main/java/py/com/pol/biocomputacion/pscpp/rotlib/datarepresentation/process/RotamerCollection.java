package py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process;

import py.com.pol.biocomputacion.pscpp.utils.Utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The class Represents a Collection of Rotamers.
 * Improve the Performance with a "tuned" ConcurrentHashMap.
 * @author Alfredo Barrios
 * */
public class RotamerCollection {
    private boolean isDependentLibrary;
    private final Map<String, RotamerLibrary> libraryList;
    private int interval;


    public RotamerCollection(int size) {
        //Create a Full hashmap, for avoid resize during the BulkLoad of elements,
        //   because we know the total final elements and we only make reads after the load
        int initialCapacity = size + 1;
        int loadFactor = 1;
        //Improve the performance because we use the
        // full size without resize and rehash
        this.libraryList=new ConcurrentHashMap<>(initialCapacity, loadFactor,
                Runtime.getRuntime().availableProcessors());
        // ConcurrencyLevel: Use the available Processors,  the estimated number of concurrently updating threads
    }

    public boolean isDependentLibrary() {
        return isDependentLibrary;
    }

    public void setDependentLibrary(boolean dependentLibrary) {
        isDependentLibrary = dependentLibrary;
    }

    public Map<String, RotamerLibrary> getLibraryList() {
        return libraryList;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
