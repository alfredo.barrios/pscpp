package py.com.pol.biocomputacion.pscpp.desktoptests;

import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.energy.EnergyFactory;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.pdb.PDBReader;
import py.com.pol.biocomputacion.pscpp.pdb.PDBWriter;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.PreprocessRotamerLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.ProcessRotamerLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;
import py.com.pol.biocomputacion.pscpp.search.SearchFactory;
import py.com.pol.biocomputacion.pscpp.search.interfaces.Search;


public class EnergyFunction {
    public static void main(String[] args) {

        try {
            ParameterFile.loadAll();
            ListUtil.initializeLists();
            PDBReader reader = new PDBReader();
            // The first argument must be the full path of the PDB folder
            // args[0] = /home/alfredo/Documents/2019/BioComputacion/jcolbes-pscppframeworkv2.0-36eafdabc66c/PSCPP/pdb65
            // The second argument: the protein name
            // args[1] = "1A7S"
            StructureS structureS = reader.readStructure(args[0], args[1]);
            // The third argument: the full path of the output folder for the Generated Rotamer Library in binary format
            // args[2] = "/home/alfredo/Documents/2019/BioComputacion/biocomputacion/BibliotecasDeRotameros/Library"
            PreprocessRotamerLibrary.generateBinaryFileForDependentLibrary((String) ParameterFile.getParameters().get(ParameterFile.STR_ROTAMER_TEXT_FILE),args[2],10);
            long t1=System.currentTimeMillis();
            RotamerCollection collection=ProcessRotamerLibrary.readBinaryFile((String) ParameterFile.getParameters().get(ParameterFile.STR_ROTAMER_LIBRARY_FILE));
            long t2=System.currentTimeMillis();
            System.out.println("Time To read Binary Rotamer Library: "+(t2-t1)+"ms");
            EnergyFactory factory=new EnergyFactory(collection);
            Protein protein = new Protein(structureS,collection,false,factory);
            protein.assignRotamers((String) ParameterFile.getParameters().get(ParameterFile.STR_INITIAL_STRUCTURE));
            long t3=System.currentTimeMillis();
            protein.calculateInteractions(false);
            long t4=System.currentTimeMillis();
            System.out.println("Time to calculate interactions: "+(t4-t3) + "ms");
            SearchFactory searchFactory = new SearchFactory();
            Search search = searchFactory.getSearch();
            search.setProtein(protein);
            search.search();
            StructureS sOut = protein.createStructure();
            PDBWriter pdbWriter = new PDBWriter();
            // args[3] = "/home/alfredo/newPdb"
            pdbWriter.writePDB(sOut, args[3], reader.pdbCodes);
            long t5 = System.currentTimeMillis();
            System.out.println("Time to Search and Write Pdb File: "+ (t5-t4) + "ms");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
