package py.com.pol.biocomputacion.pscpp.protein;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.data.AtomData;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.energy.CalcEnergyParallel;
import py.com.pol.biocomputacion.pscpp.energy.EnergyFactory;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;
import py.com.pol.biocomputacion.pscpp.utils.Utils;

import java.io.*;
import java.util.*;
import java.util.concurrent.ForkJoinPool;

/**
 * This class deals with the data and operations for a protein.
 * Basically, this is the central class representing the input data of a protein.
 * An object of this class contains a two-dimensional and a four-dimensional arrays with all the possible interactions
 * between residues (that are pre-calculated before the search process), which is used by an object of the Search class
 * to find an approximate solution that is stored as an integer array (representing the indices of rotamers in the library
 * for each residue of the protein) inside the object. The output structure is then created from the selected rotamers in
 * the solution array.
 *
 * @author jcolbes
 * @author aBarrios
 */
public class Protein {
    private static final Logger logger = LoggerFactory.getLogger(Protein.class);
    public final StructureS struc; //Protein Structure
    public final int strucSize; //Number of residues
    public final AminoAcidS[] residuesBB; //Array of residues (with BB atoms only).
    public final AminoAcidS[] origResidues; //Array of original residues
    public AminoAcidS[][] possibleResidues; //All possible residues (considering the rotamers in the library)
    public final boolean[] chainStart; //indicates if a chain starts at a residue
    public final boolean[] chainEnd; //indicates if a chain end at a residue
    //public RotamerLibraryCollection rlc; //Rotamer Library
    public int[] rotamersIndex; //Rotamer index in the library
    public Rotamer[] nativeRotamers; //Rotamers of original structure
    public RotamerLibrary[] rotLibs; //Rotamer library for each residue
    public int[] phi, psi; //BB torsion angles for each residue
    public List<List<Integer>> neighborList; //Neighbors of each residue
    public double[][][][] enerPairInteractions; //energy from residue pair interactions
    public double[][] enerSelfInteractions; //energy from residue self interactions
    public int[][][][] pairCollisions; //collisions between atoms od different residues
    //private Energy enerFunc; //Energy Function
    private final double krot = 0.35;
    private int cysIndex, hisIndex, pheIndex, tyrIndex, trpIndex, glyIndex, alaIndex;
    private int cysSGIndex, cysCAIndex, cysCBIndex;
    private EnergyFactory energyFactory;


    private static final int ALAINDEX, NATOMINDEX, CAATOMINDEX, CATOMINDEX;
    private static final double LIMITACC;

    static {
        ALAINDEX = ListUtil.aaNamePosition.get("ALA");
        NATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("N");
        CAATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("CA");
        CATOMINDEX = ListUtil.atomNamePosition.get(ALAINDEX).get("C");
        LIMITACC = (Double) ParameterFile.getParameters().get(ParameterFile.DOUBLE_LIMIT_ACCURACY);
    }


    /**
     * Constructor with only the protein structure as input
     *
     * @param s
     */
    public Protein(StructureS s) {
        this.struc = s;
        this.strucSize = s.getNumberofResidues();
        this.residuesBB = new AminoAcidS[strucSize];
        this.origResidues = new AminoAcidS[strucSize];
        this.rotamersIndex = new int[strucSize];
        this.chainStart = new boolean[strucSize]; //start of chains
        this.chainEnd = new boolean[strucSize]; //end of chains
        boolean ini;

        int count = 0;
        for (ChainS c : s.chains) {
            ini = true;
            for (AminoAcidS aa : c.residues) {
                if (ini) {
                    ini = false;
                    chainStart[count] = true;
                } else chainStart[count] = false;
                origResidues[count] = aa.cloneAminoAcidS();
                residuesBB[count] = aa.cloneBBAtomsAminoAcidS(); //only BB atoms
                rotamersIndex[count] = (-1);
                count++;
            }
        }
        inicializeIndex();
        //Ends of chains
        for (int i = 0; i < (strucSize - 1); i++) chainEnd[i] = chainStart[i + 1];
        chainEnd[strucSize - 1] = true;
    }

    public Protein(StructureS s, RotamerCollection rlc, boolean addNatRots, EnergyFactory providedEnergyFactory) {
        this(s);
        calculatePhiAndPsiAngles(rlc.getInterval());
        assignRotamerLibraries(rlc);
        //inicializeIndex();
        energyFactory = providedEnergyFactory;
        if (addNatRots) {
            if (s.hasMissingSCAtoms())
                logger.error("Warning: The structure {} has missing side-chain main atoms", s.pdbID);
            //Calculate Native Rotamers
            calculateNatRotamers();
            //Add rotamers to the list of available rotamers (for each residue)
            addNativeRotamers();
        }
    }

    private void inicializeIndex() {
        cysIndex = ListUtil.aaNamePosition.get("CYS");
        hisIndex = ListUtil.aaNamePosition.get("HIS");
        pheIndex = ListUtil.aaNamePosition.get("PHE");
        tyrIndex = ListUtil.aaNamePosition.get("TYR");
        trpIndex = ListUtil.aaNamePosition.get("TRP");
        glyIndex = ListUtil.aaNamePosition.get("GLY");
        alaIndex = ListUtil.aaNamePosition.get("ALA");
        cysSGIndex = ListUtil.atomNamePosition.get(cysIndex).get("SG");
        cysCAIndex = ListUtil.atomNamePosition.get(cysIndex).get("CA");
        cysCBIndex = ListUtil.atomNamePosition.get(cysIndex).get("CB");
    }

    /**
     * Method for calculating the rotamers of the original structure
     */
    private void calculateNatRotamers() {
        nativeRotamers = new Rotamer[strucSize];
        double[] chi;
        int aaIndex, numTorAng;
        AminoAcidS aa;
        AtomS a1, a2, a3, a4;
        AtomS[] atoms;
        AtomData[] atomsData;
        int[] refAtoms;
        for (int i = 0; i < strucSize; i++) {
            aa = origResidues[i];
            if (aa.hasMissingSCAtoms()) continue;
            aaIndex = aa.aminoIndex;
            numTorAng = ListUtil.numSidechainTorsion[aaIndex];
            chi = new double[numTorAng];
            atomsData = ListUtil.atomGenData[aaIndex];
            atoms = aa.atoms;
            for (int j = 0; j < numTorAng; j++) {
                refAtoms = atomsData[(j + 5)].refAtomsPos;
                a1 = atoms[refAtoms[0]];
                a2 = atoms[refAtoms[1]];
                a3 = atoms[refAtoms[2]];
                a4 = atoms[j + 5];
                chi[j] = AtomS.getTorsionAngle(a1, a2, a3, a4);
            }
            Rotamer nativeRotamer = new Rotamer();
            nativeRotamer.setChi(chi);
            nativeRotamer.setProbability(0.0);
            nativeRotamers[i] = nativeRotamer;
        }
    }

    /**
     * This method adds the native rotamer of a residue to its lists of candidate rotamers
     */
    private void addNativeRotamers() {
        RotamerLibrary[] rotLibsAux = new RotamerLibrary[strucSize];
        Rotamer[] rotamersAux, rotamers;
        Rotamer natRot, correctAndMostProbRot;
        int x;
        for (int i = 0; i < strucSize; i++) {
            if (rotLibs[i] == null) rotLibsAux[i] = null;
            else {
                rotamers = rotLibs[i].getRotamers();
                if (nativeRotamers[i] == null) {
                    natRot = rotamers[0]; //Most Probable Rotamer
                } else {
                    x = assignCorrectAndMostProbRotAA(i);
                    correctAndMostProbRot = rotamers[x];
                    natRot = new Rotamer();
                    natRot.setChi(nativeRotamers[i].getChi());
                    natRot.setProbability(correctAndMostProbRot.getProbability());
                }
                rotamersAux = new Rotamer[rotamers.length + 1];
                for (int j = 0; j < rotamers.length; j++) rotamersAux[j] = rotamers[j];
                rotamersAux[rotamers.length] = natRot;
                RotamerLibrary newRotLib = new RotamerLibrary();
                newRotLib.setRotamers(rotamersAux);
                newRotLib.setKey(ListUtil.aaThreeLetterCode[origResidues[i].aminoIndex]);
                rotLibsAux[i] = newRotLib;
            }
        }
        rotLibs = rotLibsAux;
    }

    /**
     * Method for calculating the BB torsion angles
     */
    public void calculatePhiAndPsiAngles(int interval) {
        phi = new int[strucSize];
        psi = new int[strucSize];
        double ang;
        double lim_inf;
        double midInterval = interval / 2.0;
        for (int i = 0; i < strucSize; i++) {
            if (chainStart[i]) {
                phi[i] = (-60); //from SCWRL4 and OPUS-Rota
                if (i > 0) {
                    psi[i - 1] = 60; //end of the past chain
                }
            } else {
                ang = AtomS.getTorsionAngle(residuesBB[i - 1].atoms[CATOMINDEX]/*C*/, residuesBB[i].atoms[NATOMINDEX]/*N*/, residuesBB[i].atoms[CAATOMINDEX]/*CA*/, residuesBB[i].atoms[CATOMINDEX]/*C*/); //C-N-CA-C
                ang += 180.0; //to work non-negative numbers
                lim_inf = ((int) (ang / interval)) * 1.0 * interval - 180.0;
                ang -= 180.0;
                if ((ang - lim_inf) >= midInterval) phi[i] = (((int) lim_inf) + interval);
                else phi[i] = ((int) lim_inf);
            }
            if (i == (strucSize - 1)) psi[i] = 60; //from SCWRL4 and OPUS-Rota
            else {
                ang = AtomS.getTorsionAngle(residuesBB[i].atoms[NATOMINDEX]/*N*/, residuesBB[i].atoms[CAATOMINDEX]/*CA*/, residuesBB[i].atoms[CATOMINDEX]/*C*/, residuesBB[i + 1].atoms[NATOMINDEX]/*N*/); //N-CA-C-N
                ang += 180.0; //to work non-negative numbers
                lim_inf = ((int) (ang / interval)) * 1.0 * interval - 180.0;
                ang -= 180.0;
                if ((ang - lim_inf) >= midInterval) psi[i] = (((int) lim_inf) + interval);
                else psi[i] = ((int) lim_inf);
            }
        }
    }

    /**
     * Method that assigns a rotamer library to each residue
     */
    private void assignRotamerLibraries(RotamerCollection rotamerCollection) {
        rotLibs = new RotamerLibrary[strucSize];
        if (rotamerCollection.isDependentLibrary()) {
            for (int i = 0; i < strucSize; i++) {
                rotLibs[i] = rotamerCollection.getLibraryList().get(ListUtil.aaThreeLetterCode[residuesBB[i].aminoIndex] + ";" + phi[i] + ".0" + ";" + psi[i] + ".0");
                if (rotLibs[i] != null)
                    Arrays.sort(rotLibs[i].getRotamers(), Collections.reverseOrder());
            }
        } else {
            for (int i = 0; i < strucSize; i++) {
                rotLibs[i] = rotamerCollection.getLibraryList().get(ListUtil.aaThreeLetterCode[residuesBB[i].aminoIndex]);
                if (rotLibs[i] != null)
                    Arrays.sort(rotLibs[i].getRotamers(), Collections.reverseOrder());
            }
        }
    }

    /**
     * Method for assigning a rotamer to each residue
     *
     * @param option Type of desired structure (Best, MostProb, BestRMSD, Random)
     */
    public void assignRotamers(String option) {
        for (int i = 0; i < strucSize; i++) {
            rotamersIndex[i] = assignRotamerAA(i, option);
        }
    }

    /**
     * Method that assigns a rotamer to a residue
     *
     * @param index  position of residue
     * @param option type of structure
     * @return
     */
    private int assignRotamerAA(int index, String option) {
        if (rotLibs[index] == null) return -1; // ALA and GLY
        if ("MostProb".equalsIgnoreCase(option)) return assignMostProbRotAA(index);
        if ("Best".equalsIgnoreCase(option)) return assignBestRotAA(index);
        if ("BestRMSD".equalsIgnoreCase(option)) return assignBestRMSDRotAA(index);
        if ("Random".equalsIgnoreCase(option)) return assignRandomRotAA(index);
        if ("Native".equalsIgnoreCase(option)) return assignNativeRotAA(index);
        else {
            System.err.println("Invalid option for rotamer assignment");
            System.exit(0);
            return -1;
        }
    }

    /**
     * Assigns the native rotamer (which is positioned at the end of the list)
     *
     * @param index
     * @return
     */
    private int assignNativeRotAA(int index) {
        RotamerLibrary rl = rotLibs[index];
        return (rl.getRotamers().length - 1);
    }

    /**
     * Method that assigns the rotamer with highest probability in the list. It is assumed that the rotamer library is in descending order of probability.
     *
     * @param index position of residue
     * @return
     */
    private int assignMostProbRotAA(int index) {
        return 0;
    }

    /**
     * Assigns the closest (regarding accuracy) rotamer to the original one.
     *
     * @param index position of residue
     * @return
     */
    private int assignBestRotAA(int index) {

        int bestInd = -1;
        int iAminoAcidIndex = residuesBB[index].aminoIndex;
        int iBestRotamerAccuracy = 0;
        double angleDev, sumErr, sumErrRef = Double.MAX_VALUE;
        Rotamer[] rotamers = rotLibs[index].getRotamers();
        AminoAcidS aaPDB = origResidues[index];
        AtomS a1, a2, a3, a4;
        int x1, x2, x3, x4;
        int[] refAtoms;
        double chiAngles[];
        AtomData[] atomsData = ListUtil.atomGenData[iAminoAcidIndex];
        int iTmpRotamerAccuracy;
        double  menorErr = Double.MAX_VALUE;
        boolean choose;
        for (int i = 0; i < rotamers.length; i++) {
            iTmpRotamerAccuracy = 0;
            sumErr = 0.0;
            chiAngles = rotamers[i].getChi();
            choose = false;
            for (int j = 0; j < chiAngles.length; j++) {

                refAtoms = atomsData[(j + 5)].refAtomsPos;
                x1 = refAtoms[0];
                x2 = refAtoms[1];
                x3 = refAtoms[2];
                x4 = (j + 5);
                a1 = aaPDB.atoms[x1];
                a2 = aaPDB.atoms[x2];
                a3 = aaPDB.atoms[x3];
                a4 = aaPDB.atoms[x4];

                if (a1 != null && a2 != null && a3 != null && a4 != null) {

                    double aaTorAngle = AtomS.getTorsionAngle(a1, a2, a3, a4);

                    double rotTorAngle = chiAngles[j];

                    angleDev = Utils.angleDev(iAminoAcidIndex, aaTorAngle, rotTorAngle, (j + 1));

                    sumErr += angleDev;

                    if (angleDev < LIMITACC)
                        iTmpRotamerAccuracy++;
                    else
                        break;
                }

            }

            if(menorErr > sumErr){
                menorErr = sumErr;
                choose = true;
            }

            if (bestInd == (-1) || choose || ((iBestRotamerAccuracy == iTmpRotamerAccuracy) && (sumErrRef > sumErr))) {
                bestInd = i;
                iBestRotamerAccuracy = iTmpRotamerAccuracy;
                sumErrRef = sumErr;
            }

        }


        return bestInd;
    }

    /**
     * Assigns the closest (regarding accuracy) rotamer to the original one.
     *
     * @param index position of residue
     * @return
     */
    private int assignCorrectAndMostProbRotAA(int index) {
        int bestInd = -1;
        int iAminoAcidIndex = residuesBB[index].aminoIndex;
        int iBestRotamerAccuracy = 0;
        double angleDev;
        Rotamer[] rotamers = rotLibs[index].getRotamers();
        AminoAcidS aaPDB = origResidues[index];
        AtomS a1, a2, a3, a4;
        int x1, x2, x3, x4;
        int[] refAtoms;
        double chiAngles[];
        AtomData[] atomsData = ListUtil.atomGenData[iAminoAcidIndex];
        double maxProb = (-1.0);
        double prob;

        for (int i = 0; i < rotamers.length; i++) {
            int iTmpRotamerAccuracy = 0;
            chiAngles = rotamers[i].getChi();
            for (int j = 0; j < chiAngles.length; j++) {
                refAtoms = atomsData[(j + 5)].refAtomsPos;
                x1 = refAtoms[0];
                x2 = refAtoms[1];
                x3 = refAtoms[2];
                x4 = (j + 5);
                a1 = aaPDB.atoms[x1];
                a2 = aaPDB.atoms[x2];
                a3 = aaPDB.atoms[x3];
                a4 = aaPDB.atoms[x4];
                if (a1 != null && a2 != null && a3 != null && a4 != null) {
                    double aaTorAngle = AtomS.getTorsionAngle(a1, a2, a3, a4);
                    double rotTorAngle = chiAngles[j];
                    angleDev = Utils.angleDev(iAminoAcidIndex, aaTorAngle, rotTorAngle, (j + 1));
                    if (angleDev < LIMITACC) iTmpRotamerAccuracy++;
                    else break;
                }
            }
            prob = rotamers[i].getProbability();
            if (bestInd == (-1) || iBestRotamerAccuracy < iTmpRotamerAccuracy || ((iBestRotamerAccuracy == iTmpRotamerAccuracy) && (prob > maxProb))) {
                bestInd = i;
                iBestRotamerAccuracy = iTmpRotamerAccuracy;
                maxProb = prob;
            }
        }
        return bestInd;
    }

    /**
     * Assigns the closest (regarding RMSD) rotamer to the original one.
     *
     * @param index position of residue
     * @return
     */
    private int assignBestRMSDRotAA(int index) {
        int bestInd = -1;
        Rotamer[] rotamers = rotLibs[index].getRotamers();

        double actualRMSD, bestRMSD = Double.MAX_VALUE;
        AminoAcidS aaAct, aaPDB = origResidues[index];

        for (int i = 0; i < rotamers.length; i++) {
            aaAct = AminoAcidS.createAminoacid(residuesBB[index], rotamers[i]);
            actualRMSD = AminoAcidS.calculateSCRMSDAAs(aaPDB, aaAct);
            if (actualRMSD < bestRMSD) {
                bestRMSD = actualRMSD;
                bestInd = i;
            }
        }

        return bestInd;
    }

    /**
     * Method that calculates all the interactions (energy and collisions) between atoms of all residues, considering all the conformations given by the rotamer library.
     *
     * @param calculateCollisions indicates if the collisions must be calculated
     */
    public void calculateInteractions(boolean calculateCollisions) {
        obtainNeighbors();
        calculatePossibleResidues();
        calculateAllInteractions(calculateCollisions);
    }

    /**
     * Assigns a rotamer picked at random
     *
     * @param index
     * @return
     */
    private int assignRandomRotAA(int index) {
        Random ran = new Random(System.currentTimeMillis() + index);
        return ran.nextInt(rotLibs[index].getRotamers().length);
    }

    /**
     * Method that creates a structure based on the data (list of rotamer index) of the protein at the moment.
     * It is assumed that the rotamers used are from a library (i.e., there won't be missing atoms in any residue)
     *
     * @return
     */
    public StructureS createStructure() {
        StructureS sOut = new StructureS(this.struc.chains.length);
        sOut.pdbID = this.struc.pdbID;
        AminoAcidS aaBB, aaOut;
        int countRes = 0;
        ChainS c;
        for (int chain = 0; chain < struc.chains.length; chain++) {
            c = new ChainS(struc.chains[chain].residues.length);
            c.chainID = struc.chains[chain].chainID;
            for (int k = 0; k < c.residues.length; k++) {
                aaBB = residuesBB[countRes];
                if (rotLibs[countRes] == null) aaOut = aaBB.cloneAminoAcidS(); //For ALA and GLY
                else aaOut = AminoAcidS.createAminoacid(aaBB, rotLibs[countRes].getRotamers()[rotamersIndex[countRes]]);
                c.residues[k] = aaOut;
                countRes++;
            }
            sOut.chains[chain] = c;
        }
        return sOut;
    }

    /**
     * Method that filters all the BB atoms from the original structure (CB included)
     *
     * @return
     */
    public StructureS createBBStructure() {
        StructureS sOut = new StructureS(this.struc.chains.length);
        sOut.pdbID = this.struc.pdbID;
        int countRes = 0;
        ChainS c;
        for (int chain = 0; chain < struc.chains.length; chain++) {
            c = new ChainS(struc.chains[chain].residues.length);
            c.chainID = struc.chains[chain].chainID;
            for (int k = 0; k < c.residues.length; k++) {
                c.residues[k] = residuesBB[countRes].cloneAminoAcidS();
                countRes++;
            }
            sOut.chains[chain] = c;
        }
        return sOut;
    }

    /**
     * Method that filters all the BB atoms from the original structure (CB excluded)
     *
     * @return
     */
    public StructureS createBBStructureWithoutCB() {
        StructureS sOut = new StructureS(this.struc.chains.length);
        sOut.pdbID = this.struc.pdbID;
        AminoAcidS aaBB, aaOut;
        int countRes = 0;
        ChainS c;
        for (int chain = 0; chain < struc.chains.length; chain++) {
            c = new ChainS(struc.chains[chain].residues.length);
            c.chainID = struc.chains[chain].chainID;
            for (int k = 0; k < c.residues.length; k++) {
                aaBB = residuesBB[countRes];
                aaOut = new AminoAcidS(aaBB.aminoIndex, aaBB.iD);
                for (int i = 0; i < 4; i++) aaOut.atoms[i] = aaBB.atoms[i].cloneAtomS();
                c.residues[k] = aaOut;
                countRes++;
            }
            sOut.chains[chain] = c;
        }
        return sOut;
    }

    /**
     * Construct a structure from the rotamers of the original structure (using the selected bond length and angle parameters).
     *
     * @return
     */
    public StructureS createNativeStructure() {
        StructureS sOut = new StructureS(this.struc.chains.length);
        sOut.pdbID = this.struc.pdbID;
        AminoAcidS aaBB, aaOut;
        int countRes = 0;
        ChainS c;
        for (int chain = 0; chain < struc.chains.length; chain++) {
            c = new ChainS(struc.chains[chain].residues.length);
            c.chainID = struc.chains[chain].chainID;
            for (int k = 0; k < c.residues.length; k++) {
                aaBB = residuesBB[countRes];
                if (rotLibs[countRes] == null) aaOut = aaBB.cloneAminoAcidS(); //For ALA and GLY
                else aaOut = AminoAcidS.createAminoacid(aaBB, nativeRotamers[countRes]);
                c.residues[k] = aaOut;
                countRes++;
            }
            sOut.chains[chain] = c;
        }
        return sOut;
    }


    /**
     * Calculates all possible residues, considering all rotamers in the library.
     */
    private void calculatePossibleResidues() {
        AminoAcidS aaBB;
        RotamerLibrary rl;
        possibleResidues = new AminoAcidS[strucSize][];
        int numRots;
        Rotamer[] rots;
        for (int i = 0; i < strucSize; i++) {
            aaBB = residuesBB[i];
            rl = rotLibs[i];
            if (rl == null) {
                possibleResidues[i] = new AminoAcidS[1];
                possibleResidues[i][0] = aaBB.cloneAminoAcidS();
            } else {
                rots = rl.getRotamers();
                numRots = rots.length;
                possibleResidues[i] = new AminoAcidS[numRots];
                for (int j = 0; j < numRots; j++) possibleResidues[i][j] = AminoAcidS.createAminoacid(aaBB, rots[j]);
            }
        }

    }

    /**
     * Method that obtains the neighbors of each residue
     */
    public void obtainNeighbors() {
        neighborList = new ArrayList<>(strucSize);
        for (int i = 0; i < strucSize; i++) neighborList.add(new ArrayList<>(strucSize));
        int aa1Index, aa2Index;
        double dist;
        AminoAcidS aa1, aa2;
        double[] maxVdWRadii = ListUtil.maxVdWRadii;
        double[] maxCADistance = ListUtil.maxCADistances;
        for (int i = 0; i < strucSize; i++) {
            aa1 = residuesBB[i];
            aa1Index = aa1.aminoIndex;
            for (int j = (i + 1); j < strucSize; j++) {
                aa2 = residuesBB[j];
                aa2Index = aa2.aminoIndex;
                //The distance between CA atoms must be lower to the sum of max distances and two times the maximum van der Waals radii
                dist = AtomS.getDistance(aa1.atoms[CAATOMINDEX], aa2.atoms[CAATOMINDEX]); //CAs
                if (!(dist > (maxCADistance[aa1Index] + 2.0 * maxVdWRadii[aa1Index] + maxCADistance[aa2Index] + 2.0 * maxVdWRadii[aa2Index]))) {
                    neighborList.get(i).add(j);
                    neighborList.get(j).add(i); //Reciprocity
                }
            }
        }

    }

    /**
     * Calculates all the possible (self and pair) interactions
     *
     * @param calculateCollisions
     */
    private void calculateAllInteractions(boolean calculateCollisions) {
        try {
            Integer threads = (Integer) ParameterFile.getParameters().get((ParameterFile.INT_PARALLEL_CALCULATION_INTERACTIONS));
            if (threads == 1)
                calculateInteractionsSerial(this);
            else
                calculateInteractionsParallel(threads);
            if (calculateCollisions) calculateAllCollisions();
        } catch (Exception e) {
           logger.error("Error Calculating All Interactions :", e);
        }
    }

    private void calculateInteractionsParallel(int threads) throws Exception {
        int strucSize = this.strucSize;
        this.enerPairInteractions = new double[strucSize][][][]; //energy between residues
        this.enerSelfInteractions = new double[strucSize][]; //internal residue energy
        int cores = Runtime.getRuntime().availableProcessors();
        ForkJoinPool pool;
        if (threads > cores) {
            pool = new ForkJoinPool();
            logger.info("Number of Threads: {}" , cores);
        }else {
            logger.info("Number of Threads: {}" , threads);
            pool = new ForkJoinPool(threads);
        }
        pool.invoke(new CalcEnergyParallel(this, -1, energyFactory.getEnergy()));
    }

    /**
     * Calculates all possible collisions, considering all rotamers in the library
     */
    private void calculateAllCollisions() {
        obtainNeighbors();
        List<Integer> pDBsToVerify;
        AminoAcidS aa1, aa2;
        int tam1, tam2;
        pairCollisions = new int[strucSize][][][];
        AminoAcidS[] posResI, posResJ;

        for (int i = 0; i < strucSize; i++) {
            posResI = possibleResidues[i];
            tam1 = posResI.length;
            pairCollisions[i] = new int[tam1][][];
            pDBsToVerify = neighborList.get(i);
            for (int rotI = 0; rotI < tam1; rotI++) {
                aa1 = posResI[rotI];
                pairCollisions[i][rotI] = new int[strucSize][];
                for (int j = 0; j < pDBsToVerify.size(); j++) {
                    if (i < j) {
                        posResJ = possibleResidues[j];
                        tam2 = posResJ.length;
                        pairCollisions[i][rotI][j] = new int[tam2];
                        for (int rotJ = 0; rotJ < tam2; rotJ++) {
                            aa2 = posResJ[rotJ];
                            pairCollisions[i][rotI][j][rotJ] = AminoAcidS.calcCollisions(aa1, aa2);
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the total energy of a structure
     *
     * @return
     */
    public double getTotalEnergy() {
        return (getTotalSelfEnergy() + getTotalPairEnergy());
    }

    /**
     * Returns the total energy of a structure
     *
     * @param rotIndex
     * @return
     */
    public double getTotalEnergy(int[] rotIndex) {
        return (getTotalSelfEnergy(rotIndex) + getTotalPairEnergy(rotIndex));
    }

    /**
     * Returns the sum of the internal residue energies
     *
     * @return
     */
    public double getTotalPairEnergy() {
        double totPairEn = 0.0;
        int rotI, rotJ;
        List<Integer> pDBsToVerify;
        for (int i = 0; i < strucSize; i++) {
            if (rotamersIndex[i] == (-1)) rotI = 0; //GLY and ALA
            else rotI = rotamersIndex[i];
            pDBsToVerify = neighborList.get(i);
            for (int j : pDBsToVerify) {
                if (j > i) {
                    if (rotamersIndex[j] == (-1)) rotJ = 0; //GLY and ALA
                    else rotJ = rotamersIndex[j];
                    totPairEn += enerPairInteractions[i][rotI][j][rotJ];
                }
            }
        }
        return totPairEn;
    }

    /**
     * Returns the sum of the internal residue energies
     *
     * @param rotIndex
     * @return
     */
    private double getTotalPairEnergy(int[] rotIndex) {
        double totPairEn = 0.0;
        int rotI, rotJ;
        List<Integer> PDBsToVerify;
        for (int i = 0; i < strucSize; i++) {
            if (rotIndex[i] == (-1)) rotI = 0; //GLY and ALA
            else rotI = rotIndex[i];
            PDBsToVerify = neighborList.get(i);
            for (int j : PDBsToVerify) {
                if (j > i) {
                    if (rotIndex[j] == (-1)) rotJ = 0; //GLY and ALA
                    else rotJ = rotIndex[j];
                    totPairEn += enerPairInteractions[i][rotI][j][rotJ];
                }
            }
        }
        return totPairEn;
    }

    /**
     * Returns the sum of the internal residue energies
     *
     * @return
     */
    public double getTotalSelfEnergy() {
        double totSelfEn = 0.0;
        int rotI;
        for (int i = 0; i < strucSize; i++) {
            if (rotamersIndex[i] == (-1)) rotI = 0; //GLY and ALA
            else rotI = rotamersIndex[i];
            totSelfEn += enerSelfInteractions[i][rotI];
        }
        return totSelfEn;
    }

    /**
     * Returns the sum of the internal residue energies
     *
     * @param rotIndex
     * @return
     */
    private double getTotalSelfEnergy(int[] rotIndex) {
        double totSelfEn = 0.0;
        int rotI;
        for (int i = 0; i < strucSize; i++) {
            if (rotIndex[i] == (-1)) rotI = 0; //GLY and ALA
            else rotI = rotIndex[i];
            totSelfEn += enerSelfInteractions[i][rotI];
        }
        return totSelfEn;
    }

    /**
     * Returns the total number of collisions
     *
     * @return
     */
    public int getTotalCollisions() {
        int totCol = 0;
        int rotI, rotJ;
        List<Integer> pDBsToVerify;
        for (int i = 0; i < strucSize; i++) {
            if (rotamersIndex[i] == (-1)) rotI = 0; //GLY and ALA
            else rotI = rotamersIndex[i];
            pDBsToVerify = neighborList.get(i);
            for (int j : pDBsToVerify) {
                if (j > i) {
                    if (rotamersIndex[j] == (-1)) rotJ = 0; //GLY and ALA
                    else rotJ = rotamersIndex[j];
                    totCol += pairCollisions[i][rotI][j][rotJ];
                }
            }
        }
        return totCol;
    }

    /**
     * Method for sequential calculation of interactions
     *
     * @param p
     */
    public void calculateInteractionsSerial(Protein p) throws Exception {
        double[][][][] enerPairInteractions; //energy between residues
        double[][] enerSelfInteractions; //internal residue energy
        int strucSize = p.strucSize;
        RotamerLibrary[] rotLibs = p.rotLibs;
        List<List<Integer>> neighborList = p.neighborList;
        AminoAcidS[][] possibleResidues = p.possibleResidues;
        AminoAcidS[] posResI, posResJ;
        List<Integer> pDBsToVerify;
        AminoAcidS aa1, aa2;
        int tam1, tam2;
        RotamerLibrary rl1, rl2;
        enerSelfInteractions = new double[strucSize][];
        enerPairInteractions = new double[strucSize][][][];
        boolean[] isChainEnd = p.chainEnd;

        for (int i = 0; i < strucSize; i++) {
            posResI = possibleResidues[i];
            tam1 = posResI.length;
            rl1 = rotLibs[i];
            enerSelfInteractions[i] = new double[tam1];
            enerPairInteractions[i] = new double[tam1][][];
            pDBsToVerify = neighborList.get(i);
            for (int rotI = 0; rotI < tam1; rotI++) {
                aa1 = posResI[rotI];
                aa1.setIsEnd(isChainEnd[i]);
                if (rl1 == null) enerSelfInteractions[i][rotI] = 0.0;
                else enerSelfInteractions[i][rotI] = energyFactory.getEnergy().calculateSelfEnergy(aa1, rotI, rl1);
                enerPairInteractions[i][rotI] = new double[strucSize][];
                for (int j : pDBsToVerify) {
                    if (i < j) {
                        posResJ = possibleResidues[j];
                        tam2 = posResJ.length;
                        rl2 = rotLibs[j];
                        enerPairInteractions[i][rotI][j] = new double[tam2];
                        for (int rotJ = 0; rotJ < tam2; rotJ++) {
                            aa2 = posResJ[rotJ];
                            aa2.setIsEnd(isChainEnd[j]);
                            enerPairInteractions[i][rotI][j][rotJ] = energyFactory.getEnergy().calculatePairEnergy(aa1, rotI, rl1, aa2, rotJ, rl2);
                        }
                    }
                }
            }
        }
        p.enerSelfInteractions = enerSelfInteractions;
        p.enerPairInteractions = enerPairInteractions;
    }


}
