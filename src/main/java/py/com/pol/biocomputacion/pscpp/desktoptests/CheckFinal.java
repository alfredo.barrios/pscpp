package py.com.pol.biocomputacion.pscpp.desktoptests;

import org.apache.commons.io.FilenameUtils;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.energy.EnergyFactory;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.pdb.PDBReader;
import py.com.pol.biocomputacion.pscpp.pdb.PDBWriter;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.ProcessRotamerLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;
import py.com.pol.biocomputacion.pscpp.search.SearchFactory;
import py.com.pol.biocomputacion.pscpp.search.interfaces.Search;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class CheckFinal {
    public static void main(String[] args) {
        try {
            long t1 = System.currentTimeMillis();
            ParameterFile.loadAll();
            ListUtil.initializeLists();
            PDBReader reader = new PDBReader();
            //Folder of PDB files
            File[] files = new File(args[0]).listFiles();
            if (files != null) {
                PDBWriter writer = new PDBWriter();
                RotamerCollection collection = ProcessRotamerLibrary.readBinaryFile((String) ParameterFile.getParameters().get(ParameterFile.STR_ROTAMER_LIBRARY_FILE));
                EnergyFactory factory = new EnergyFactory(collection);
                SearchFactory searchFactory = new SearchFactory();
                Search search = null;
                for (File file : files) {
                    try {
                        if (file.getName().endsWith(".pdb")) {
                            StructureS structureS = reader.readStructure(file.getParent(), FilenameUtils.getBaseName(file.getName()));
                            Protein protein = new Protein(structureS, collection, false, factory);
                            protein.assignRotamers((String) ParameterFile.getParameters().get(ParameterFile.STR_INITIAL_STRUCTURE));
                            protein.calculateInteractions(false);
                            search = searchFactory.getSearch();
                            search.setProtein(protein);
                            search.search();
                            StructureS sOut = protein.createStructure();
                            //Write in the output folder provides as main parameter
                            writer.writePDB(sOut, args[1] + file.getName(), reader.pdbCodes);
                        }
                    } catch (Exception e) {
                       System.out.println("Error processing the file :"+file.getName());
                    }
                }
            }
            long t2 = System.currentTimeMillis();
            System.out.println("Time to Search and Write Pdb Dataset: " + TimeUnit.MILLISECONDS.toSeconds(t2 - t1) + " sec");

        } catch (Exception e) {
            System.out.println("Error: " + e.getLocalizedMessage());
        }
    }
}
