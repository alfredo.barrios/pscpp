package py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing;


import com.esotericsoftware.kryo.io.Input;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.parserimplementation.ParserRotamerLibraryFactory;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.DependentKey;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.DependentLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.IndependentLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class for Processing the Rotamer Library
 *
 * @author Alfredo Barrios
 * @version 1.0
 * @since 1.0
 */
public class ProcessRotamerLibrary {

    private static final Logger logger = LoggerFactory.getLogger(ProcessRotamerLibrary.class);

    public static long dependentTime = 0;
    public static long independentTime = 0;
    public static ParserRotamerLibraryFactory parserRotamerLibraryFactory = new ParserRotamerLibraryFactory();


    /**
     * @param rotamerLibraryFilename
     * @return ConcurrentMap a HASH with the filtered and collected Data by key(The Amino and the Primary Angles).
     */
    public static ConcurrentMap<DependentKey, List<DependentLibrary>> rotamerLibraryToObject(String rotamerLibraryFilename) {
        try {
            System.out.println(rotamerLibraryFilename);
            long t1 = System.currentTimeMillis();
            ConcurrentMap<DependentKey, List<DependentLibrary>> lines = Files.lines(Paths.get(rotamerLibraryFilename)).parallel().
                    map(ProcessRotamerLibrary::parseLine).filter(o -> o != null).
                    collect(Collectors.groupingByConcurrent(x -> x.getKey()));
            long t2 = System.currentTimeMillis();
           logger.info("JAVA-MAP-COLLECT-TIME-FROM-TEXT-RAW-DEPENDENT-LIBRARY: {} ms", t2 - t1);
            dependentTime = t2 - t1;
            return lines;
        } catch (Exception e) {
            logger.error("rotamerLibraryToObject:  ", e);
        }
        return null;
    }

    /**
     * @param rotamerLibrary the string path of the file of an Independent Library.
     * @return ConcurrentMap a HASH with the filtered and collected Data by key(The Amino).
     */
    public static ConcurrentMap<String, List<IndependentLibrary>> rotamerIndependentLibrarytoObject(String rotamerLibrary) {
        try {
            long t1 = System.currentTimeMillis();
            ConcurrentMap<String, List<IndependentLibrary>> lines = Files.lines(Paths.get(rotamerLibrary)).parallel().
                    map(ProcessRotamerLibrary::parseIndependentLine).filter(o -> o != null).collect(Collectors.groupingByConcurrent(x -> x.getAmino()));
            long t2 = System.currentTimeMillis();
            logger.info("JAVA-MAP-COLLECT-TIME-FROM-TEXT-RAW-INDEPENDENT-LIBRARY: {} ms" , t2 - t1);
            independentTime = t2 - t1;
            return lines;
        } catch (Exception e) {
            logger.error("rotamerIndependentLibrarytoObject: ", e);
        }
        return null;
    }
    /**
     * Sort a given Independent Rotamer Libary
     * @param rotamerLibrary filepath of the library
     * */
    public static void sortByIndependentLibraryByProbability(String rotamerLibrary) {
        try {
            long t1 = System.currentTimeMillis();
            Comparator<String> probabilityComparator = Comparator.comparing(parserRotamerLibraryFactory.rotamerLine::getProbabilityIndependentLibrary);
            Stream<String> fileSorted = Files.lines(Paths.get(rotamerLibrary)).sorted(probabilityComparator);
            long t2 = System.currentTimeMillis();
            Files.write(Paths.get(rotamerLibrary + ".sorted"), (Iterable<String>) fileSorted::iterator);
            long t3 = System.currentTimeMillis();
            logger.info("Sorting Independent Library in : {} ms",t2 - t1);
            logger.info("Writing Independent Library in : {} ms", t3 - t2);
        } catch (Exception e) {
            logger.error("Error Sorting the Independent Library: " , e);
        }
    }
    /**
     * Sort a given Dependent Rotamer Libary
     * @param rotamerLibrary The filepath of the library
     * */
    public static void sortByDependentLibraryByProbability(String rotamerLibrary, String suffix) {
            try {
                List<String> lines = Files.readAllLines(Paths.get(rotamerLibrary),StandardCharsets.UTF_8);
                File fileOut1 = new File(rotamerLibrary+suffix);
                PrintWriter outFile = new PrintWriter(new BufferedWriter(new FileWriter(fileOut1)));
                String delims = " ",line;
                String[] fields;
                int tam,i=0;
                double p;
                ArrayList<String> buff;
                while(i<lines.size()){
                    line = lines.get(i);
                    fields = line.split(delims);
                    if(fields.length==1 && IsInteger(fields[0])){
                        tam = Integer.parseInt(line);
                        buff = new ArrayList<>(tam);
                        i++;
                        line = lines.get(i);
                        buff.add(line);
                        i++;
                        for(int j=0;j<tam;j++){ buff.add(lines.get(i)); i++;}
                        sortList(buff);
                        outFile.println((buff.size()-1));
                        for(String s: buff) outFile.println(s);
                    }
                }
                outFile.close();
            } catch (IOException ex) {
                logger.error("Problem sorting rotamers according to probability values", ex);
                System.exit(0);
            }
    }
    private static boolean IsInteger(String str){
        try{
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException nfe){
            return false;
        }
    }
    /**
     * Sorts a list of rotamers
     * @param buffer
     */
    private static void sortList(ArrayList<String> buffer){
        String aux_s;
        boolean band=true;
        double aux;
        double[] probs = new double[buffer.size()];
        probs[0] = -1.0;
        String delims = " ";
        String[] fields;
        for(int i=1; i<buffer.size(); i++){
            fields = buffer.get(i).split(delims);
            probs[i] = Double.parseDouble(fields[0]);
        }
        while(band){
            band=false;
            for(int i=1; i<buffer.size()-1; i++){
                if(probs[i]<probs[i+1]){
                    aux = probs[i];
                    probs[i] = probs[i+1];
                    probs[i+1] = aux;
                    aux_s = buffer.get(i);
                    buffer.set(i, buffer.get(i+1));
                    buffer.set(i+1, aux_s);
                    band=true;
                }
            }
        }
    }
    /**
     * Filter an Independent Rotamer Library, keep the data if the probability of each line is greater than the given probability.
     * @param rotamerLibrary the filepath of the library
     * @param probability the probability from user
     * */
    public static void reduceIndependentLibraryByProbability(String rotamerLibrary, Double probability) {
        try {
            long t1 = System.currentTimeMillis();
            List<String> filteredFile = Files.lines(Paths.get(rotamerLibrary)).filter(x -> isProbabilityFromIndependentLibraryGreaterThan(x, probability)).collect(Collectors.toList());
            FileUtils.writeLines(new File(rotamerLibrary + ".reduced"), filteredFile);
            long t2 = System.currentTimeMillis();
            logger.info("Reducing Independent Library in : {} ms total size: {}" , t2 - t1, filteredFile.size());
        } catch (Exception e) {
            logger.error("Error in Reducing Independent Library ", e);
        }
    }
    /**
     * Filter an Dependent Rotamer Library, keep the data if the probability of each line is greater than the given probability.
     * @param rotamerLibrary the filepath of the library
     * @param probability the probability from user
     * */
    public static void reduceDependentLibraryByProbability(String rotamerLibrary, double probability) {
        try {
            long t1 = System.currentTimeMillis();
           // List<String> filteredFile = Files.lines(Paths.get(rotamerLibrary)).filter(x -> isProbabilityFromDependentLibraryGreaterThan(x, probability)).collect(Collectors.toList());
            //FileUtils.writeLines(new File(rotamerLibrary + ".reduced"), filteredFile);
            long t2 = System.currentTimeMillis();
           // System.out.println("Reducing Independent Library in : " + (t2 - t1) + "ms" + ", total size: " + filteredFile.size());
        } catch (Exception e) {
            logger.error("Error reducing dependent library" , e);
        }
    }

    /**
     * Filter the Independent Library, given the accumulated Probability, keeping only the values in the range of this given probability
     * @param rotamerLibrary the filepath of the library
     * @param accumulatedProbability the probability from user
     * */
    public static void reduceIndependentByAccumulatedProbability(String rotamerLibrary, double accumulatedProbability) {
        try {
            Double currentProbability= new Double("0");
            List<String> filteredFile=Files.lines(Paths.get(rotamerLibrary)).filter(x->checkRangeProbabilityIndependent(x,accumulatedProbability,currentProbability)).collect(Collectors.toList());
            FileUtils.writeLines(new File(rotamerLibrary + ".filteredByAccumulatedProbability"), filteredFile);
        }catch (Exception e){
           logger.error("Error filtering independent Rotamer library by Accumulated Probability: " , e);
        }

    }
    /**
     * Filter the Dependent Library, given the accumulated Probability, keeping only the values in the range of this given probability
     * @param rotamerLibrary  the filepath of the library
     * @param accumulatedProbability the probability from user
     * */
    public static void reduceDependentByAccumulatedProbability(String rotamerLibrary, double accumulatedProbability) {
        try {
            Double currentProbability= new Double("0");
            List<String> filteredFile=Files.lines(Paths.get(rotamerLibrary)).filter(x->checkRangeProbabilityDependent(x,accumulatedProbability,currentProbability)).collect(Collectors.toList());
            FileUtils.writeLines(new File(rotamerLibrary + ".filteredByAccumulatedProbability"), filteredFile);
        }catch (Exception e){
            logger.error("Error filtering Dependent Rotamer library by Accumulated Probability: " , e);
        }

    }
    /**
     * @param rotamerBinaryFile the filepath of the binary libary
     * @return Return a Processed Collection of Rotamers from the given binary file
     * */
    public static RotamerCollection readBinaryFile(String rotamerBinaryFile){
        RotamerCollection collection;
        try {
            Input in=new Input(new FileInputStream(new RandomAccessFile(rotamerBinaryFile,"r").getFD()));
            int size=in.readInt(true);
            collection =new RotamerCollection(size);
            Map<String, RotamerLibrary> libraryList=collection.getLibraryList();
            boolean isDependent=in.readBoolean();
            collection.setDependentLibrary(isDependent);
            if(isDependent)
                collection.setInterval(in.readInt());
            while (!in.end()){
                RotamerLibrary rotamerLibrary=new RotamerLibrary();
                String key=in.readString();
                rotamerLibrary.setKey(key);
                int rotamersize=in.readInt();
                Rotamer[] rotamerarray=new Rotamer[rotamersize];
                for(int j=0;j<rotamersize;j++){
                    Rotamer rotamer=new Rotamer();
                    double probability=in.readDouble();
                    rotamer.setProbability(probability);
                    int rotSize=in.readInt(true);
                    if(rotSize!=0) {
                        double[] chi = new double[rotSize];
                        for (int k = 0; k < rotSize; k++)
                            chi[k] = in.readDouble();
                        rotamer.setChi(chi);
                    }
                    rotamerarray[j]=rotamer;
                }
                rotamerLibrary.setRotamers(rotamerarray);
                libraryList.put(rotamerLibrary.getKey(),rotamerLibrary);
            }
        }catch (Exception e){
            logger.error("Error Reading the Rotamer Binary File");
            return null;
        }
        return collection;
    }
    /**
     * Check if the probability of the Independent Rotamer library line is in the correct range of the accumulate probability
     * @param line
     * @param accumulatedProbability
     * @param maxProbability
     * @return true/false if the probability of the line is in the range of the accumulate probability
     * */
    private static boolean checkRangeProbabilityIndependent(String line, double maxProbability, Double accumulatedProbability){
        double currentProbability=parserRotamerLibraryFactory.rotamerLine.getProbabilityIndependentLibrary(line);
        accumulatedProbability=accumulatedProbability+currentProbability;
        return (Double.compare(accumulatedProbability,maxProbability)<0?true:false);
    }
    /**
     * Check if the probability of the line is in the correct range of the accumulate probability
     * @param accumulatedProbability
     * @param maxProbability
     * @param line
     * @return true/false if the probability of the dependent Rotamer library line is in the range of the accumulate probability
     *
     * */
    private static boolean checkRangeProbabilityDependent(String line, double maxProbability, Double accumulatedProbability){
       // ProbabilityDependentLibrary currentProbability=parserRotamerLibraryFactory.rotamerLine.getProbabilityDependentLibrary(line);
        //accumulatedProbability=accumulatedProbability+currentProbability.getProbability();
        return (Double.compare(accumulatedProbability,maxProbability)<0?true:false);
    }
    /**
     * @param line
     * @param givenProbability
     * @return true/false if the probability of the line is greater than the given probability
     * */
    private static boolean isProbabilityFromIndependentLibraryGreaterThan(String line, double givenProbability) {
        return parserRotamerLibraryFactory.rotamerLine.getProbabilityIndependentLibrary(line).compareTo(givenProbability) > 0;
    }
    /**
     * @param line
     * @param givenProbability
     * @return true/false if the probability of the line is greater than the given probability
     * */
    //private static boolean isProbabilityFromDependentLibraryGreaterThan(String line, ProbabilityDependentLibrary givenProbability) {
      //  return parserRotamerLibraryFactory.rotamerLine.getProbabilityDependentLibrary(line).compareTo(givenProbability)>0;
   //}

    //private static boolean isProbabilityFromDependentLibraryGreaterThan(String line, double givenProbability) {
      //  return parserRotamerLibraryFactory.rotamerLine.getProbabilityDependentLibrary(line).getProbability()>givenProbability;
    //}


    /**
     * Parse the Data from line
     *
     * @param line The line to be processed
     * @return DependentLibrary: the equivalent object of the line
     **/
    private static DependentLibrary parseLine(String line) {
        try {
            return parserRotamerLibraryFactory.rotamerLine.parseDependentLine(line);
        } catch (Exception e) {
            logger.error("Error Parsing an DependentLibrary Line: " ,e);
        }
        return null;
    }

    /**
     * Parse the Data from line
     *
     * @param line The line to be processed
     * @return IndependentLibrary: the equivalent object of the line
     */
    private static IndependentLibrary parseIndependentLine(String line) {
        try {
            return parserRotamerLibraryFactory.rotamerLine.parseIndependentLine(line);
        } catch (Exception e) {
            logger.error("Error Parsing an IndependentLibrary Line: " , e);
        }
        return null;
    }
}
