package py.com.pol.biocomputacion.pscpp.search.implementations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;
import py.com.pol.biocomputacion.pscpp.search.interfaces.Search;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * This class performs a simulated annealing search, very similar to the used in OPUS-Rota.
 * @author jcolbes
 */
public class SimulatedAnnealingSearch implements Search {

    private static final Logger logger = LoggerFactory.getLogger(SimulatedAnnealingSearch.class);

    private  Random ran;
    private int iter,changes;
    private double totEnergyEnd,totEnergyStart;
    private Protein p;
    private int[] rotIndices;
    private int[] numRots;
    private boolean[] hasRotamers;
    private DecimalFormat df;
    private double T;
    private double[] actualSolutionEnergy;
    private int protSize;

    public void  setProtein(Protein p) {
        df = new DecimalFormat("#0.000");
        this.p = p;
        numRots = new int[p.strucSize];
        hasRotamers = new boolean[p.strucSize];
        RotamerLibrary rl;
        RotamerLibrary[] rotLibs = p.rotLibs;
        for(int i=0; i<p.strucSize; i++){
            rl = rotLibs[i];
            if(rl==null){ numRots[i]=0; hasRotamers[i]=false; }
            else{ numRots[i]=rl.getRotamers().length; hasRotamers[i]=true; }
        }
        rotIndices = p.rotamersIndex; //Solution
        ran = new Random();
        protSize = p.strucSize;
    }

    /**
     * Performs a simulated annealing search
     */
    @Override
    public void search() {
        double T_ini = 2.5, T_fin = 0.05;
        int numIterSA = 97, numIterFix = 3;
        int numIter = numIterFix+numIterSA;
        double interval = (T_ini-T_fin)/(numIterSA-1);
        int[] order;
        actualSolutionEnergy = new double[numIter+1];
        actualSolutionEnergy[iter] = p.getTotalEnergy();
        iter++;
        for(int i=0; i<numIterSA; i++){
            order = generatePermutation();
            T = T_ini - (i*interval);
            for(int j: order){
                if(hasRotamers[j]) performMoveHBMC(j);
            }
            actualSolutionEnergy[iter] = p.getTotalEnergy();
            iter++;
        }

        for(int i=0; i<numIterFix; i++){
            order = generatePermutation();
            for(int j: order){
                if(hasRotamers[j]) performMoveLS(j);
            }
            actualSolutionEnergy[iter] = p.getTotalEnergy();
            iter++;
        }

        logger.info("Energy[\"{}\"]:\t {}", iter-1, df.format(p.getTotalEnergy()));
    }

    /**
     * Performs a local search move
     * @param res
     */
    private void performMoveLS(int res){
        int max = numRots[res];
        int currRotInd = rotIndices[res];
        double[] delta = new double[max];

        for(int rot=0; rot<max; rot++){
            if(rot==currRotInd) delta[rot] = 0.0;
            else delta[rot] = calculateSingleEnergy(res, rot) - calculateSingleEnergy(res,currRotInd);
        }

        double minDelta=delta[0];
        int newRot=0;
        for(int rot=1; rot<max; rot++){
            if(minDelta>delta[rot]){
                minDelta=delta[rot];
                newRot = rot;
            }
        }

        if(newRot!=currRotInd){
            rotIndices[res] = newRot;
            changes++;
        }
    }

    /**
     * Performs a Monte-Carlo move
     * @param res
     */
    private void performMoveHBMC(int res){
        int max = numRots[res];
        int currRotInd = rotIndices[res];
        double boltz, sum_boltz=0.0;
        double[] prob = new double[max];
        double delta;

        for(int rot=0; rot<max; rot++){
            if(rot==currRotInd) delta = 0.0;
            else delta = calculateSingleEnergy(res, rot) - calculateSingleEnergy(res,currRotInd);
            boltz = delta/T;
            if(boltz>7.0) prob[rot] = 0.0;
            else prob[rot] = Math.exp(-boltz);
            sum_boltz += prob[rot];
        }

        double accumBoltz = 0.0;
        double val = ran.nextDouble();
        int newRot = (-2);
        for(int rot=0; rot<max; rot++){
            accumBoltz += (prob[rot]/sum_boltz);
            if(val<accumBoltz){newRot = rot; break;}
        }

        if(newRot!=currRotInd){
            rotIndices[res] = newRot;
            changes++;
        }
    }

    /**
     * Performs a permutation used to determine the order of residues in an iteration of the SA search
     * @return
     */
    private int[] generatePermutation(){
        int[] perm = new int[protSize];
        for(int i=0; i<protSize; i++) perm[i]=i;

        int pos,x;
        for(int j=perm.length; j>0; j--){
            pos=ran.nextInt(j);
            //Swap
            x=perm[pos];
            perm[pos]=perm[j-1];
            perm[j-1]=x;
        }

        return perm;
    }

    /**
     * Calculates the contribution of a rotamer in the selected residue position
     * @param res
     * @param rot
     * @return
     */
    private double calculateSingleEnergy(int res, int rot){
        double e = p.enerSelfInteractions[res][rot];
        int rotJ;
        for(int j: p.neighborList.get(res)){
            rotJ = rotIndices[j];
            if(rotJ==(-1)) rotJ = 0;
            if(j>res) e += p.enerPairInteractions[res][rot][j][rotJ];
            else e += p.enerPairInteractions[j][rotJ][res][rot];
        }
        return e;
    }

    @Override
    public double[] getPosibleSolution() {
        return actualSolutionEnergy;
    }

    @Override
    public int getChanges() {
        return changes;
    }

    @Override
    public int getIterations(){
        return iter;
    }
}
