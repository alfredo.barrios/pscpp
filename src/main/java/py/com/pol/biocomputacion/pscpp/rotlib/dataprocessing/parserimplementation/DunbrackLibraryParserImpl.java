package py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.parserimplementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.DependentKey;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.DependentLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.IndependentLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.interfaces.ParseLibraryLine;
import py.com.pol.biocomputacion.pscpp.utils.ListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Implementation Class for the File Parsing of a RotamerLibrary.
 *
 * @author alfredo Barrios
 * @since 1.0
 **/
public class DunbrackLibraryParserImpl implements ParseLibraryLine {

    private static final Logger logger = LoggerFactory.getLogger(DunbrackLibraryParserImpl.class);

    static final Pattern pattern = Pattern.compile("[ ]+");

    /**
     * @param line from the Raw Dependent Library File
     * @return an DependetLibrary Object
     */
    @Override
    public DependentLibrary parseDependentLine(String line) {
        try {
            String [] str = pattern.split(line);
            DependentKey key = new DependentKey();
            Rotamer rotamer = new Rotamer();
            rotamer.setProbability(Double.valueOf(str[8]));
            if(rotamer.getProbability()<0 || rotamer.getProbability()>1){
                logger.error("Error, the probability is not between [0,1], is: {}", rotamer.getProbability());
                System.exit(0);
            }
            List<Double> chisList = new ArrayList();
            for (int i = 0; i < 4; i++) {
                double chiFromFile = Double.parseDouble(str[9 + i]);
                if(chiFromFile!=0D){
                    chisList.add(chiFromFile);
                    if(chiFromFile<-180 || chiFromFile>180)
                    {
                        logger.error("Error, in the Chi Range [-180, 180] : {} ", chiFromFile);
                        System.exit(0);
                    }
                }
            }
            rotamer.setChi(chisList.stream().mapToDouble(d -> d).toArray());
            key.setPhi(Double.valueOf(str[1]));
            key.setPsi(Double.valueOf(str[2]));
            key.setAmino(str[0]);
            DependentLibrary d = new DependentLibrary();
            d.setKey(key);
            d.setRotamer(rotamer);
            return d;
        } catch (Exception e) {
            logger.error("Error Parsing Dependent Line : ", e);
        }
        return null;

    }

    /**
     * @param line from the Raw Independent Library File
     * @return an IndependetLibrary Object
     */
    @Override
    public IndependentLibrary parseIndependentLine(String line) {
        try {
            String str[] = pattern.split(line);
            if (str.length > 11) {
                if (ListUtils.aminoacidsOrder.get(str[0]) != null) {
                    Rotamer rotamer = new Rotamer();
                    rotamer.setProbability(Double.valueOf(str[8]));
                    List<Double> chisList = new ArrayList();
                    int c = 11;
                    for (int i = 0; i < 4; i++) {
                        if (Integer.parseInt(str[i + 1]) > 0) // Check if Exits
                        {
                            double chifromfile= Double.valueOf(str[c++]);
                            if(chifromfile!=0) {
                                if (chifromfile < -180 || chifromfile > 180) {
                                    logger.error("Error, in the Chi Range [-180, 180] : {} " , chifromfile);
                                    System.exit(0);
                                    chisList.add(chifromfile);
                                }
                            }
                            c++;
                        }
                    }
                    rotamer.setChi(chisList.stream().mapToDouble(d -> d).toArray());
                    IndependentLibrary d = new IndependentLibrary();
                    d.setRotamer(rotamer);
                    d.setAmino(str[0]);
                    if(rotamer.getProbability()<0 || rotamer.getProbability()>1){
                       logger.error("Error, the probability is not between [0,1], is: {}", rotamer.getProbability());
                        System.exit(0);
                    }
                    return d;
                } else {
                    logger.error("Error in RotamerLibrary Line, AMINO NOT FOUND...");
                    return null;
                }
            }
        } catch (Exception e) {
            logger.error("Error in RotamerLibrary Line: {}", e);
        }
        return null;
    }
    /**
     * @param line from the Raw Independent Library File
     * @return the probability of the line
     */
    @Override
    public Double getProbabilityIndependentLibrary(String line) {
        try {
            String [] str = pattern.split(line);
            return Double.valueOf(str[8]);
        } catch (Exception e) {
            logger.error("Error in RotamerLibrary Line: {}", e);
        }
        return 0D;
    }
}
