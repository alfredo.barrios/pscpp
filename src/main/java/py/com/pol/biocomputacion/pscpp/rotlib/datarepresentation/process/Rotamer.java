package py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process;

import java.util.Arrays;

/**
 * This class manages the data representing a rotamer.
 */
public class Rotamer implements Comparable<Rotamer>{

    private double probability;
    private double[] chi;


    public double[] getChi() {
        return chi;
    }

    public void setChi(double[] chi) {
        this.chi = chi;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    @Override
    public int compareTo(Rotamer rotamer) {
       return Double.compare(getProbability(),rotamer.getProbability());
    }

    @Override
    public String toString() {
        return "Rotamer{" +
                "chi=" + Arrays.toString(chi) +
                ", probability=" + probability +
                '}';
    }
}
