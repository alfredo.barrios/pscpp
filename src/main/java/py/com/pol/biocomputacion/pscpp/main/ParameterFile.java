package py.com.pol.biocomputacion.pscpp.main;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.utils.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;

/**
 * @author Alfredo Barrios
 * @version 1.0
 * <p>
 * Load the parameters from aplication.properties file to HashMap of parameters
 */
public class ParameterFile {
    private static final Logger logger = LoggerFactory.getLogger(ParameterFile.class);
    public static PropertiesConfiguration config = null;
    public static final int size=18;
    private static final Map<String, Object> parameters=new ConcurrentHashMap<>(size,1f, Runtime.getRuntime().availableProcessors());
    public static final String STR_ROTAMER_LIBRARY_PARSER_IMPLEMENTATION = "proCodex.pscpp.rotlib.parserImplementation.class";
    public static final String STR_ENERGY_FUNCTION_IMPLEMENTATION = "proCodex.pscpp.energy.implementation.class";
    public static final String STR_SEARCH_METHOD_IMPLEMENTATION = "proCodex.pscpp.search.implementation.class";
    public static final String STR_LIST_ATOMS = "ListOfAtoms";
    public static final String STR_ATOM_GENERATION_DATA = "AtomGenerationData";
    public static final String STR_MAX_CA_DISTANCE = "MaxCADistanceFilename";
    public static final String DOUBLE_ALPHA_COLISION_FACTOR = "alpha(collisionFactor)";
    public static final String DOUBLE_LIMIT_ACCURACY = "limitAccuracy";
    public static final String DOUBLE_LIMIT_SOL_ACC_DOWN = "limitSolAccDown";
    public static final String DOUBLE_LIMIT_SOL_ACC_UP = "limitSolAccUp";
    public static final String STR_ROTAMER_LIBRARY_FILE = "rotLibBinaryFile";
    public static final String STR_ROTAMER_TEXT_FILE ="rotLibTextFile";
    public static final String STR_SEARCH_METHOD = "searchMethod";
    public static final String STR_INITIAL_STRUCTURE = "initialStructure";
    public static final String STR_ENERGY_FUNCTION = "energyFunction";
    public static final String BOOL_CALCULATE_COLITIONS = "calculateCollisions?";
    public static final String BOOL_ALL_SC_ATOMS = "allSCAtoms?";
    public static final String BOOL_PRINT_WARNINGS_PDB = "printWarnings(whileReadingPDBs)?";
    public static final String INT_PARALLEL_CALCULATION_INTERACTIONS = "numberOfThreadsForEnergyInteractions";

    /**
     * Static method for load All the Parameters from application.properties
     */
    public static void loadAll() {
        try {
            config = new PropertiesConfiguration("application.properties");//Load the Configuration file
            //Get the value from the application.properties, if the parameter is not present, default value is assigned
            parameters.put(STR_ROTAMER_LIBRARY_PARSER_IMPLEMENTATION, config.getString(STR_ROTAMER_LIBRARY_PARSER_IMPLEMENTATION,
                    "py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.parserimplementation.LibraryParserImpl"));
            parameters.put(STR_ENERGY_FUNCTION_IMPLEMENTATION,config.getString(STR_ENERGY_FUNCTION_IMPLEMENTATION));
            parameters.put(STR_LIST_ATOMS, config.getString(STR_LIST_ATOMS, "resAtom.dat"));
            parameters.put(STR_ATOM_GENERATION_DATA, config.getString(STR_ATOM_GENERATION_DATA, "atomDataEngh.dat"));
            parameters.put(STR_MAX_CA_DISTANCE, config.getString(STR_MAX_CA_DISTANCE, "maxDistancesToCA.dat"));
            parameters.put(DOUBLE_ALPHA_COLISION_FACTOR, config.getDouble(DOUBLE_ALPHA_COLISION_FACTOR, 0.6D));
            parameters.put(DOUBLE_LIMIT_ACCURACY, config.getDouble(DOUBLE_LIMIT_ACCURACY, 40.0D));
            parameters.put(DOUBLE_LIMIT_SOL_ACC_DOWN, config.getDouble(DOUBLE_LIMIT_SOL_ACC_DOWN, 20.0D));
            parameters.put(DOUBLE_LIMIT_SOL_ACC_UP, config.getDouble(DOUBLE_LIMIT_SOL_ACC_UP, 20.0D));
            parameters.put(STR_ROTAMER_LIBRARY_FILE, config.getString(STR_ROTAMER_LIBRARY_FILE, "bbdep02.May.libshortsortRASP.bin"));
            parameters.put(STR_SEARCH_METHOD, config.getString(STR_SEARCH_METHOD, "LS"));
            parameters.put(STR_INITIAL_STRUCTURE, config.getString(STR_INITIAL_STRUCTURE, "MostProb"));
            parameters.put(STR_ENERGY_FUNCTION, config.getString(STR_ENERGY_FUNCTION, "CISRR"));
            parameters.put(BOOL_CALCULATE_COLITIONS, config.getBoolean(BOOL_CALCULATE_COLITIONS, Boolean.FALSE));
            parameters.put(BOOL_ALL_SC_ATOMS, config.getBoolean(BOOL_ALL_SC_ATOMS, Boolean.TRUE));
            parameters.put(BOOL_PRINT_WARNINGS_PDB, config.getBoolean(BOOL_PRINT_WARNINGS_PDB, Boolean.TRUE));
            parameters.put(INT_PARALLEL_CALCULATION_INTERACTIONS, config.getInt(INT_PARALLEL_CALCULATION_INTERACTIONS, 1));
            parameters.put(STR_ROTAMER_TEXT_FILE, config.getString(STR_ROTAMER_TEXT_FILE,"bbdep02.May.lib"));
            parameters.put(STR_SEARCH_METHOD_IMPLEMENTATION, config.getString(STR_SEARCH_METHOD_IMPLEMENTATION, "LocalSearch"));
        } catch (Exception e) {
            logger.error("Error loading parameters ",  e);
        }
    }

    public static void print() {
        BiConsumer<String, Object> formatPrint=(key, value) ->
                logger.info("{} = {}", key, value);
        parameters.forEach(formatPrint);
    }

    public static Map<String, Object> getParameters(){
        return parameters;
    }
}
