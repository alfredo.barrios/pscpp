package py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.parserimplementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.rotlib.interfaces.ParseLibraryLine;
/**
 * Provides a Factory for Build a Parser Class.
 * */
public class ParserRotamerLibraryFactory {
    private static final Logger logger = LoggerFactory.getLogger(ParserRotamerLibraryFactory.class);

    public static ParseLibraryLine rotamerLine;
    private static final String PACKAGE_BASE="py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.parserimplementation.";
    /**
     * When the Factory is created, we choose the parser.
     * The complete path of the Implementation Class is a Simple String provided.
     * */
    public ParserRotamerLibraryFactory(){
        try {

            Class rotamerParserImpl= Class.forName(PACKAGE_BASE+(ParameterFile.getParameters().get(ParameterFile.STR_ROTAMER_LIBRARY_PARSER_IMPLEMENTATION))
                    ,true,this.getClass().getClassLoader());
            if(ParseLibraryLine.class.isAssignableFrom(rotamerParserImpl)){
                rotamerLine= (ParseLibraryLine)rotamerParserImpl.getConstructor().newInstance();
            }else {
                logger.error("The Given Class do not implement ParseLibraryLine Interface");
                System.exit(0);
            }
        } catch (Exception e) {
           logger.error("Error generating the Parser Rotamer Library Factory : {}", e);
            System.exit(0);
        }
    }
    public ParseLibraryLine getRotamerLine(){
        return rotamerLine;
    }
}
