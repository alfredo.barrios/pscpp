package py.com.pol.biocomputacion.pscpp.main;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.energy.EnergyFactory;
import py.com.pol.biocomputacion.pscpp.pdb.PDBReader;
import py.com.pol.biocomputacion.pscpp.pdb.PDBWriter;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.ProcessRotamerLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;
import py.com.pol.biocomputacion.pscpp.search.SearchFactory;
import py.com.pol.biocomputacion.pscpp.search.interfaces.Search;
import py.com.pol.biocomputacion.pscpp.utils.reporting.OutputData;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;


/**
 * Class for performing an analysis test with a list of proteins.
 */
public class AnalysisPSCPP {

    private static final Logger logger = LoggerFactory.getLogger(AnalysisPSCPP.class);
    /**
     * Returns a solution (stored in folderOut) for every protein in a list, given the folder that contains the input PDBFiles.
     * Additionally, some data about the process is saved in Excel files; and the initial structure can be selected
     * @param pdbListFilename
     * @param folderIn
     * @param folderOut
     * @param initialStruct 
     * @param addNatRots 
     */
    public static void analysisPSCPP(String pdbListFilename, String folderIn, String folderOut, String initialStruct, boolean addNatRots)throws Exception {
        long tini = System.nanoTime(), tProt;
        PDBReader pdbReader = new PDBReader();
        List<String> listPDBs = pdbReader.readPDBList(pdbListFilename);
        File[] files = new File(folderIn).listFiles();
        int numMaxPDBs = listPDBs.size();
        if (files != null) {
            String testName = "_" + "_" + initialStruct + "_";
            String testFolder = folderOut + File.separator + "Results" + testName;
            String pdbsOutFolder = testFolder + File.separator + "PDBs";
            String searchFolder = testFolder + File.separator + "Search";
            boolean okPdbFolder = new File(pdbsOutFolder).mkdirs(); //output PDB files
            if(!okPdbFolder) logger.warn("Pdb Out Folder Could not be created");
            boolean okSearchFolder = new File(searchFolder).mkdirs(); //output search
            if(!okSearchFolder) logger.warn("Search Folder Could not be created");
            DecimalFormat df = new DecimalFormat("#0.000");
            RotamerCollection rotLib = ProcessRotamerLibrary.readBinaryFile((String) ParameterFile.getParameters().get(ParameterFile.STR_ROTAMER_LIBRARY_FILE));
            logger.info("Rotamer Library(\" {} \"): {} " ,(rotLib.isDependentLibrary() ? "DEPENDENT" : "INDEPENDENT"), ParameterFile.STR_ROTAMER_LIBRARY_FILE);
            logger.info("Energy Function: {}" , ParameterFile.STR_ENERGY_FUNCTION_IMPLEMENTATION);
            logger.info("Initial Structure: {}" , initialStruct);
            EnergyFactory energyFactory = new EnergyFactory(rotLib);
            String[] energyTermsNames = energyFactory.getEnergy().getEnergyTerms();

            StructureS sNat, sIn, sOut;
            Protein p;
            int numRes, numChain;
            double[] ene;
            long tInt, tSea;
            int numberOfTorsionAngles = 2;
            OutputData outData = new OutputData(numMaxPDBs, numberOfTorsionAngles);
            double verEnerLimit = 0.01;
            PDBWriter pdbWriter = new PDBWriter();
            Search search;
            SearchFactory searchFactory = new SearchFactory();
            for(String pdb: listPDBs){
                    //Read
                    tProt = System.nanoTime();
                    sNat = pdbReader.readStructure(folderIn, pdb);
                    if (sNat == null) {
                        logger.warn("Structure Incomplete!");
                        continue;
                    }
                    outData.pdbNames.add(pdb);
                    numChain = sNat.chains.length;
                    outData.chainNums.add(numChain);
                    numRes = sNat.getNumberofResidues();
                    outData.resNums.add(numRes);
                    logger.info("Chains: {} \tResidues:  {} \tAtoms: {} ", numChain,  numRes, sNat.getNumberofAtoms());
                    outData.perc_access = pdbReader.getSolventAccess(numRes, folderIn + File.separator + pdb + "acc.txt");
                    outData.isComplete.add(sNat.isComplete);
                    p = new Protein(sNat, rotLib, addNatRots, energyFactory);
                    ene = energyFactory.getEnergy().calculateEnergies(sNat, p);
                    outData.eNat.add(ene);
                    outData.collisionsNat.add(StructureS.calculateTotalCollisions(sNat));

                    //Init
                    p.assignRotamers(initialStruct);
                    sIn = p.createStructure();
                    outData.calculateAccuraciesAndRSMDIn(sNat, sIn);
                    outData.collisionsIn.add(StructureS.calculateTotalCollisions(sIn));
                    ene = energyFactory.getEnergy().calculateEnergies(sIn, p);
                    outData.eStart.add(ene);
                    //Interactions
                    tInt = System.currentTimeMillis();
                    p.calculateInteractions((boolean) ParameterFile.getParameters().get(ParameterFile.BOOL_CALCULATE_COLITIONS));
                    outData.timesI.add(System.currentTimeMillis() - tInt);
                    if (Math.abs(p.getTotalEnergy() - ene[0]) > verEnerLimit)
                        logger.warn("Energies discrepancies on Init! {} \t {}", ene[0] , p.getTotalEnergy());

                //Search"
                // /home/alfredo/pdb"
                    search = searchFactory.getSearch();
                    tSea = System.currentTimeMillis();
                    search.setProtein(p);
                    search.search();
                    outData.timesS.add(System.currentTimeMillis() - tSea);
                    outData.createExcelSearch(pdb, searchFolder, search.getPosibleSolution(), null, p.strucSize);
                    outData.iterations.add(search.getIterations());
                    outData.changes.add(search.getChanges());

                    // Indices Originales p.rotamersIndex
                    //Solution

                     sOut = p.createStructure();
                     pdbWriter.writePDB(sOut,  pdbsOutFolder+ File.separator + pdb + "-Orig.pdb", pdbReader.pdbCodes);

                     ene = energyFactory.getEnergy().calculateEnergies(sOut, p);
                     outData.eEnd.add(ene);
                     logger.info("totalEnergy: {}",ene[0]);
                     logger.info("totalSelfEnergy {}",ene[1]);
                     logger.info("totalVDWEnergy {}",ene[2]);
                     logger.info("totalDisulfidBondEnergy {}",ene[3]);

                    double self= p.getTotalSelfEnergy();
                    double pair = p.getTotalPairEnergy();


                if (Math.abs((pair+self) - ene[0]) > verEnerLimit)
                       logger.warn("Energies discrepancies in Solution! {} \t {}", ene[0] , p.getTotalEnergy());
                    outData.collisionsOut.add(StructureS.calculateTotalCollisions(sOut));
                    outData.calculateAccuraciesAndRSMDOut(sNat, sOut);

                    //Saving results
                    outData.calcForAA(sNat, sOut);
                    pdbWriter.writePDB(sOut, pdbsOutFolder + File.separator + pdb + ".pdb", pdbReader.pdbCodes);
                    outData.writeRotamers(p, pdbsOutFolder, pdb, pdbReader.pdbCodes);
                    logger.info("Time: {} sec" ,  df.format((System.nanoTime() - tProt) / 1000000000.0));
                }

                //Saving final results
                outData.saveResults(testFolder, testName, energyTermsNames);
                outData.saveResultsAA(testFolder, testName);
                logger.info("Total time: {}  sec" , df.format((System.nanoTime() - tini) / 1000000000.0));
            }
        }
    }
