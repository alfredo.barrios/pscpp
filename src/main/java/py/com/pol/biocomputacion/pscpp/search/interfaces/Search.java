package py.com.pol.biocomputacion.pscpp.search.interfaces;


import py.com.pol.biocomputacion.pscpp.protein.Protein;

public interface Search {

    int iter = 0;
    //Set the protein for the process of search
    void setProtein(Protein protein);
    //Call the implementation method, the result is stored in the proteinn
    void search();
    // gets the probable solution
    double [] getPosibleSolution();

    int getIterations();

    int getChanges();
}
