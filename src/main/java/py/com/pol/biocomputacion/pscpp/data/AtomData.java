package py.com.pol.biocomputacion.pscpp.data;

/**
 * This class manages the necessary data for amino acid construction (from rotamers).
 */
public class AtomData {
    public final int[] refAtomsPos; //Positions (in list of atoms) of the necessary atoms for atom generation
    public final double[] bondValues; //[0]->bond lenght; [1]->bond angle; [2]->torsion angle
    
    public AtomData(int[] refAtomsPos, double[] bondValues){
        this.refAtomsPos = refAtomsPos;
        this.bondValues = bondValues;
    }
}
