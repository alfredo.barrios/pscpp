package py.com.pol.biocomputacion.pscpp.desktoptests;

import java.io.File;


/**
 * Utility main program
 * */
public class CleanFilesFromFolder {
    public static void main(String[] args) {
        //the Fist argument is the folder Path
        for (File f : new File(args[0]).listFiles()) {
            //the second argument is the extension file
            if (f.getName().endsWith(args[1])) {
                System.out.println("Deleting :"+f.getAbsolutePath());
                if(!f.delete()) System.out.println();
            }
        }
    }
}
