package py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing;

import com.esotericsoftware.kryo.io.Output;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.DependentKey;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.DependentLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess.IndependentLibrary;
import py.com.pol.biocomputacion.pscpp.desktoptests.datarepresentation.RotamerLibrary;

import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * Class for PreProcess the Rotamer Library
 *
 * @author Alfredo Barrios
 * @version 1.0
 * @since 1.0
 */
public class PreprocessRotamerLibrary {

    private static final Logger logger = LoggerFactory.getLogger(PreprocessRotamerLibrary.class);

    /**
     * Generate the binary file given the raw file
     *
     * @param dependentLibrary Path
     */
    public static Map<String, RotamerLibrary>  generateBinaryFileForDependentLibrary(String dependentLibrary, String output, Integer interval) {
        try {
            ConcurrentMap<DependentKey, List<DependentLibrary>> dependentHash = ProcessRotamerLibrary.rotamerLibraryToObject(dependentLibrary);
            HashMap<String, RotamerLibrary> libraryList = new HashMap<>();
            if(dependentHash!=null) {
                for (List<DependentLibrary> l : dependentHash.values()) {
                    for (DependentLibrary library : l) {
                        String key = library.getKey().toString();
                        if (libraryList.get(key) == null) {
                            RotamerLibrary rotamerLibrary = new RotamerLibrary();
                            rotamerLibrary.setKey(key);
                            rotamerLibrary.setDependentLibrary(true);
                            rotamerLibrary.getRotamers().add(library.getRotamer());
                            libraryList.put(key, rotamerLibrary);
                        } else {
                            libraryList.get(key).getRotamers().add(library.getRotamer());
                        }
                    }
                }
                serializeRotamerLibrary(libraryList, output + ".bin", interval, true);
            }else {
                logger.error("EMPTY DEPENDENT LIBRARY!!! Generating empty Binary File.... ");
            }
            return libraryList;
        } catch (Exception e) {
            logger.error("Problem Generating the Rotamer Binary File: " , e);
            System.exit(0);
        }
        return null;
    }

    /**
     * Generate the binary file given the raw file
     *
     * @param independentFileLibrary Path
     */
    public static void generateBinaryFileForIndependentLibrary(String independentFileLibrary, String output) {
        try {
            ConcurrentMap<String, List<IndependentLibrary>> hash = ProcessRotamerLibrary.rotamerIndependentLibrarytoObject(independentFileLibrary);
            HashMap<String, RotamerLibrary> libraryList = new HashMap<>();
            //Process for IndependentLibrary
            if(hash!=null) {
                for (List<IndependentLibrary> libraries : hash.values()) {
                    for (IndependentLibrary independentLibrary : libraries) {
                        String key = independentLibrary.getAmino();
                        if (libraryList.get(key) == null) {
                            RotamerLibrary rotamerLibrary = new RotamerLibrary();
                            rotamerLibrary.setKey(key);
                            rotamerLibrary.getRotamers().add(independentLibrary.getRotamer());
                            libraryList.put(key, rotamerLibrary);
                        } else {
                            libraryList.get(key).getRotamers().add(independentLibrary.getRotamer());
                        }
                    }
                }
                serializeRotamerLibrary(libraryList, output + ".dat", null, false);
            }else{
                logger.error("Error Generating the Rotamer Independent Library: EMPTY LIBRARY");

            }
        } catch (Exception e) {
            logger.error("Error Generating the Rotamer Independent Library: " , e);
            System.exit(0);
        }
    }

    /**
     * @param filename    the name of the binary file to be created
     * @param libraryList the LibraryList to be serialized
     */
    public static void serializeRotamerLibrary(Map<String, RotamerLibrary> libraryList, String filename,
                                               Integer interval, boolean isDependentLibrary) {
        try {
            if(libraryList.isEmpty()){
                System.err.println("Rotamer Library Error: The given library is empty!!!");
                return;
            }
            Output out = new Output(new FileOutputStream(new RandomAccessFile(filename, "rw").getFD()));
            out.writeInt(libraryList.size(), true);
            out.writeBoolean(isDependentLibrary); //We assume a Homogeneous library
            if(interval!=null) out.writeInt(interval);
            for (RotamerLibrary rotamerLibrary : libraryList.values()) {
                    out.writeString(rotamerLibrary.getKey());
                    out.writeInt(rotamerLibrary.getRotamers().size());
                    for (Rotamer rotamer : rotamerLibrary.getRotamers()) {
                        out.writeDouble(rotamer.getProbability());
                        out.writeInt(rotamer.getChi().length,true);
                        for (double d : rotamer.getChi()) {
                            out.writeDouble(d);
                        }
                    }
            }
            out.close();
        } catch (Exception e) {
            logger.error("Error Serializating the Rotamer Library: " , e);
            System.exit(0);
        }
    }
}
