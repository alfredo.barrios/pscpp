package py.com.pol.biocomputacion.pscpp.desktoptests.datarepresentation;

//import org.dizitart.no2.objects.Id;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a General Rotamer Library
 * <p>
 * Obs: The Key perform the search in O(1), and the index in log2(N).
 * <p>
 * Add index only if necessary, because it's increase the INSERT-TIME and the required storage.
 *
 * @Indices({
 * @Index(value = "isDependentLibrary", type = IndexType.NonUnique)
 * })
 */
public class RotamerLibrary {
    //@Id
    String key;
    boolean isDependentLibrary;
    List<Rotamer> rotamers = new ArrayList<>();


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isDependentLibrary() {
        return isDependentLibrary;
    }

    public void setDependentLibrary(boolean dependentLibrary) {
        isDependentLibrary = dependentLibrary;
    }

    public List<Rotamer> getRotamers() {
        return rotamers;
    }

    public void setRotamers(List<Rotamer> rotamers) {
        this.rotamers = rotamers;
    }

    @Override
    public String toString() {
        return "RotamerLibrary{" +
                "key='" + key + '\'' +
                ", isDependentLibrary=" + isDependentLibrary +
                ", rotamers=" + rotamers +
                '}';
    }
}
