package py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process;

import java.util.Arrays;

/**
 * Represents the Rotamer Library
 * Obs: Only the search key change between Dependent and Independent Library
 * Example:
 *          Dependent Key: PRO;30.0;20.0
 *          Independent Key: PRO
 * */
public class RotamerLibrary {
    private String key;
    private Rotamer[] rotamers;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Rotamer[] getRotamers() {
        return rotamers;
    }

    public void setRotamers(Rotamer[] rotamers) {
        this.rotamers = rotamers;
    }

    @Override
    public String toString() {
        return "RotamerLibrary{" +
                "key='" + key + '\'' +
                ", rotamers=" + Arrays.toString(rotamers) +
                '}';
    }
}
