package py.com.pol.biocomputacion.pscpp.energy.implementations;

import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.energy.interfaces.Energy;
import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.AtomS;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * This class contains all the functions necessary to implement the scoring function of CISRR.
 */
public class EnergyCISRR implements Energy {
    private final int cysIndex, hisIndex, pheIndex, tyrIndex, trpIndex, glyIndex, alaIndex;
    private final int cysSGIndex, cysCAIndex, cysCBIndex;
    private final RotamerCollection rlc;
    private final double krot = 0.35D;
    private  final double krep = 5.882D;
    private  final double katt = 0.08D;
    private  final String[] energyTerms = new String[]{"Etotal", "Eself", "EvdW", "Edb"};


    public EnergyCISRR(RotamerCollection rlc) {
        cysIndex = ListUtil.aaNamePosition.get("CYS");
        hisIndex = ListUtil.aaNamePosition.get("HIS");
        pheIndex = ListUtil.aaNamePosition.get("PHE");
        tyrIndex = ListUtil.aaNamePosition.get("TYR");
        trpIndex = ListUtil.aaNamePosition.get("TRP");
        glyIndex = ListUtil.aaNamePosition.get("GLY");
        alaIndex = ListUtil.aaNamePosition.get("ALA");
        cysSGIndex = ListUtil.atomNamePosition.get(cysIndex).get("SG");
        cysCAIndex = ListUtil.atomNamePosition.get(cysIndex).get("CA");
        cysCBIndex = ListUtil.atomNamePosition.get(cysIndex).get("CB");
        this.rlc = rlc;
    }

    @Override
    public String[] getEnergyTerms() {
        return energyTerms;
    }

    /**
     * van der Waals radii for CISRR
     */
    private static final double[][] vdWRadiiCISRR =
            {
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765}, //ALA
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.7765, 1.5675D, 1.672, 1.5675D, 1.5675D}, //ARG
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.672, 1.33, 1.5675D}, //ASN
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.672, 1.33, 1.33}, //ASP
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7575}, //CYS
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.672, 1.33, 1.5675D}, //GLN
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.672, 1.33, 1.33}, //GLU
                    {1.5675D, 1.7765, 1.672, 1.33}, //GLY
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.672, 1.5675D, 1.672, 1.672, 1.5675D}, //HIS
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.7765, 1.7765}, //ILE
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.7765, 1.7765}, //LEU
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.7765, 1.7765, 1.425}, //LYS
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.7575, 1.7765}, //MET
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.672, 1.672, 1.672, 1.672, 1.672, 1.672}, //PHE
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.7765}, //PRO
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.33}, //SER
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.33, 1.7765}, //THR
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.672, 1.672, 1.672, 1.5675D, 1.672, 1.672, 1.672, 1.672, 1.672}, //TRP
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.672, 1.672, 1.672, 1.672, 1.672, 1.672, 1.33}, //TYR
                    {1.5675D, 1.7765, 1.672, 1.33, 1.7765, 1.7765, 1.7765} //VAL
            };

    /**
     * Method that returns the internal energy of a residue, considering its rotamer probability in the rotamer library
     *
     * @param aa
     * @param rotInd rotamer position in the list
     * @param rl     list of rotamers
     * @return
     */
    public double calculateSelfEnergy(AminoAcidS aa, int rotInd, RotamerLibrary rl) {
        double pRot = rl.getRotamers()[rotInd].getProbability();
        if (pRot < 0.000001D) pRot = 0.000001D; //for very low values
        double pMax = rl.getRotamers()[0].getProbability(); //it is assumed an ordered rotamer library (descending order of probability).
        return krot * (-1.0D) * getGamma(aa.aminoIndex) * Math.log(pRot / pMax);
    }

    /**
     * Private method returning the gamma factor according to amino acid type.
     * @param aminoIndex
     * @return
     */
    private int getGamma(int aminoIndex){
        if(aminoIndex==hisIndex || aminoIndex==pheIndex || aminoIndex==tyrIndex || aminoIndex==trpIndex) return 2;
        if(aminoIndex==glyIndex || aminoIndex==alaIndex) return 0;
        return 1;
    }

    /**
     * Calculates the energy from pair (residue) interactions
     *
     * @param aa1
     * @param rotInd1
     * @param rl1
     * @param aa2
     * @param rotInd2
     * @param rl2
     * @return
     */
    public double calculatePairEnergy(AminoAcidS aa1, int rotInd1, RotamerLibrary rl1, AminoAcidS aa2, int rotInd2, RotamerLibrary rl2) {
        return (calculateVdWEnergy(aa1, aa2) + calculateDisulfideBondEnergy(aa1, rotInd1, rl1, aa2, rotInd2, rl2));

    }

    /**
     * Calculates the energy between two residues, using the van der Waals energy function defined in Cao(2011) for CISRR
     * @param aa1
     * @param aa2
     * @return
     */
    private double calculateVdWEnergy(AminoAcidS aa1, AminoAcidS aa2){
        double energy = 0.0;
        int aminoIndex1 = aa1.aminoIndex;
        int aminoIndex2 = aa2.aminoIndex;
        AtomS[] atoms1 = aa1.atoms;
        AtomS[] atoms2 = aa2.atoms;
        AtomS a,b;
        double dist;
        double radii1,radii2,Rij,en,ratio;
        double[] vdWRadiis1 = vdWRadiiCISRR[aminoIndex1];
        double[] vdWRadiis2 = vdWRadiiCISRR[aminoIndex2];
        for(int i=0; i<atoms1.length; i++){
            a = atoms1[i];
            if(a==null) continue;
            radii1 = vdWRadiis1[i];
            for(int j=0; j<atoms2.length; j++){
                b = atoms2[j];
                if(b==null) continue;
                if(i>4 || j>4){ //The interactions betweens BB atoms are not considered
                    radii2 = vdWRadiis2[j];
                    dist = AtomS.getDistance(a, b);
                    Rij = radii1+radii2;
                    ratio = dist/Rij;
                    en = 0.0;
                    if(1.0>=ratio) en = krep*(1-ratio);
                    else if(2.0>ratio) en = katt*((ratio*ratio)-(3.0*ratio)+2.0);
                    energy += en;
                }
            }
        }
        return energy;
    }

    /**
     * Calculates the energy of disulfide bonds for cysteine pair, according to Cao(2011) for CISRR.
     *
     * @param aa1
     * @param rotInd1 rotamer index for aa1
     * @param rl1     rotamer library for aa1
     * @param aa2
     * @param rotInd2 rotamer index for aa2
     * @param rl2     rotamer library for aa2
     * @return
     */
    private double calculateDisulfideBondEnergy(AminoAcidS aa1, int rotInd1, RotamerLibrary rl1, AminoAcidS aa2, int rotInd2, RotamerLibrary rl2) {
        if (!(aa1.aminoIndex == cysIndex && aa2.aminoIndex == cysIndex)) return 0.0;

        //{"N", "CA", "C", "O", "CB", "SG"}, CYS
        AtomS sg1, sg2, cb1, cb2, ca1, ca2;
        sg1 = aa1.atoms[cysSGIndex]; //SG
        cb1 = aa1.atoms[cysCBIndex]; //CB
        ca1 = aa1.atoms[cysCAIndex]; //CA
        if (sg1 == null || cb1 == null || ca1 == null) return 0.0;
        sg2 = aa2.atoms[cysSGIndex]; //SG
        cb2 = aa2.atoms[cysCBIndex]; //CB
        ca2 = aa2.atoms[cysCAIndex]; //CA
        if (sg2 == null || cb2 == null || ca2 == null) return 0.0;
        double dist = AtomS.getDistance(sg1, sg2);
        double A1 = AtomS.getBondAngle(cb1, sg1, sg2);//CB1-SG1-SG2
        double A2 = AtomS.getBondAngle(sg1, sg2, cb2); //SG1-SG2-CB2
        double X2 = AtomS.getTorsionAngle(ca1, cb1, sg1, sg2); //CA1-CB1-SG1-SG2
        double X4 = AtomS.getTorsionAngle(sg1, sg2, cb2, ca2); //SG1-SG2-CB2-CA2
        double X3 = AtomS.getTorsionAngle(cb1, sg1, sg2, cb2); //CB1-SG1-SG2-CB2
        double t1 = Math.abs(dist - 2.0) / 0.05;
        double t2 = Math.abs(A1 - 104) / 5.0;
        double t3 = Math.abs(A2 - 104) / 5.0;
        double t4 = min(Math.abs(Math.abs(X2) - 80), Math.abs(Math.abs(X2) - 180)) / 10.0;
        double t5 = min(Math.abs(Math.abs(X4) - 80), Math.abs(Math.abs(X4) - 180)) / 10.0;
        double t6 = Math.abs(Math.abs(X3) - 90) / 20.0;
        double t7 = (this.calculateSelfEnergy(aa1, rotInd1, rl1) + this.calculateSelfEnergy(aa2, rotInd2, rl2)) / 2.0;
        double S = t1 + t2 + t3 + t4 + t5 + t6 + t7;
        double Ess = (0.1 * S - 4.0);
        if (Ess < 0.0) return Ess;
        else return 0.0;
    }

    private static double min(double n1, double n2) {
        if (n1 > n2) return n2;
        else return n1;
    }

    @Override
    public double[] calculateEnergies(StructureS s, Protein pRef) {
        double totalSelfEnergy=0D, totalVDWEnergy=0D, totalDisulfidBondEnergy=0D;
        Protein p = new Protein(s);
        p.rotLibs = pRef.rotLibs;
        p.assignRotamers("Best");
        p.obtainNeighbors();
        List<List<Integer>> neighbors = p.neighborList;
        List<Integer> rotamersToVerify;
        RotamerLibrary[] rotLibs = p.rotLibs;
        RotamerLibrary rl1,rl2;
        int[] rotIndices = p.rotamersIndex;
        AminoAcidS[] residues = p.origResidues;
        AminoAcidS aa1,aa2;
        for(int i=0; i<p.strucSize; i++){
            aa1 = residues[i];
            rl1 = rotLibs[i];

            if(rl1!=null) totalSelfEnergy += calculateSelfEnergy(aa1, rotIndices[i], rl1);

            rotamersToVerify = neighbors.get(i);
            for(Integer j: rotamersToVerify){
                if(j>i){
                    aa2 = residues[j];
                    rl2 = rotLibs[j];
                    totalVDWEnergy += calculateVdWEnergy(aa1, aa2);
                    totalDisulfidBondEnergy += calculateDisulfideBondEnergy(aa1, rotIndices[i], rl1, aa2, rotIndices[j], rl2);
                }
            }
        }

        double totalEnergy = totalSelfEnergy+totalVDWEnergy+totalDisulfidBondEnergy;
        double[] energies = new double[4];
        energies[0] = totalEnergy;
        energies[1] = totalSelfEnergy;
        energies[2] = totalVDWEnergy;
        energies[3] = totalDisulfidBondEnergy;
        return energies;
    }

}
