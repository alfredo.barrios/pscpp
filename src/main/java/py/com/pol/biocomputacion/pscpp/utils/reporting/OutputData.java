package py.com.pol.biocomputacion.pscpp.utils.reporting;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.ChainS;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;


/**
 * Class for calculating global results of a test and print them in Excel files.
 */
public class OutputData {

    private static final Logger logger = LoggerFactory.getLogger(OutputData.class);

    //Arrays with output data
    public final List<String> pdbNames;
    public final List<Integer> chainNums, resNums, collisionsNat, collisionsIn, collisionsOut, iterations, changes;
    public final List<Boolean> isComplete;
    public final List<double[]> eNat, eStart, eEnd, accuracyIn, accuracyInB, accuracyInE, accuracyOut, accuracyOutB, accuracyOutE;
    public final List<Long> timesI, timesS;
    public final List<Double> rmsdIn, rmsdOut;
    private final int numOfTorAng, numberOfAAs;
    public int[][][] accuracyAA, counterAA;
    public double[] perc_access; //SASA values for the residues of the protein

    private static final double LIMITDOWNACC, LIMITUPACC;
    static{
        LIMITDOWNACC = (double) ParameterFile.getParameters().get(ParameterFile.DOUBLE_LIMIT_SOL_ACC_DOWN);
        LIMITUPACC =(double) ParameterFile.getParameters().get(ParameterFile.DOUBLE_LIMIT_SOL_ACC_UP);
    }

    /**
     * Constructor
     * @param numMaxPDBs number of proteins in the list
     * @param numTorAng number of considered torsion angles
     */
    public OutputData(int numMaxPDBs, int numTorAng){
        pdbNames = new ArrayList<>(numMaxPDBs);
        chainNums = new ArrayList<>(numMaxPDBs);
        resNums = new ArrayList<>(numMaxPDBs);
        isComplete = new ArrayList<>(numMaxPDBs);
        eNat = new ArrayList<>(numMaxPDBs);
        collisionsNat = new ArrayList<>(numMaxPDBs);
        eStart = new ArrayList<>(numMaxPDBs);
        eEnd = new ArrayList<>(numMaxPDBs);
        iterations = new ArrayList<>(numMaxPDBs);
        changes = new ArrayList<>(numMaxPDBs);
        timesI = new ArrayList<>(numMaxPDBs);
        timesS = new ArrayList<>(numMaxPDBs);
        accuracyIn = new ArrayList<>(numMaxPDBs);
        accuracyInB = new ArrayList<>(numMaxPDBs);
        accuracyInE = new ArrayList<>(numMaxPDBs);
        accuracyOut = new ArrayList<>(numMaxPDBs);
        accuracyOutB = new ArrayList<>(numMaxPDBs);
        accuracyOutE = new ArrayList<>(numMaxPDBs);
        rmsdIn = new ArrayList<>(numMaxPDBs);
        rmsdOut = new ArrayList<>(numMaxPDBs);
        collisionsIn = new ArrayList<>(numMaxPDBs);
        collisionsOut = new ArrayList<>(numMaxPDBs);

        //Precision for AA
        this.numOfTorAng = numTorAng;
        numberOfAAs = ListUtil.resNum;
        accuracyAA = new int[3][numberOfAAs+1][numOfTorAng]; //[]T, B or E; []AA; []chi;
        counterAA = new int[3][numberOfAAs+1][numOfTorAng]; //[]T, B or E; []AA; []chi;
        for(int i=0; i<accuracyAA.length; i++) for(int j=0; j<accuracyAA[i].length; j++) for(int k=0; k<accuracyAA[i][j].length; k++){
            counterAA[i][j][k]=0;
            accuracyAA[i][j][k]=0;
        }
    }

    /**
     * Calculates and saves the accuracy results for a initial solution
     * @param sRef
     * @param sIn
     */
    public void calculateAccuraciesAndRSMDIn(StructureS sRef, StructureS sIn){
        double[] accuracy;
        accuracy = new double[numOfTorAng];
        for(int i=0; i<numOfTorAng; i++) accuracy[i]=StructureS.calculateAccuracy(sRef, sIn, (i+1));
        accuracyIn.add(accuracy);
        accuracy = new double[numOfTorAng];
        for(int i=0; i<numOfTorAng; i++) accuracy[i]=StructureS.calculateAccuracyBuried(sRef, sIn, (i+1), perc_access);
        accuracyInB.add(accuracy);
        accuracy = new double[numOfTorAng];
        for(int i=0; i<numOfTorAng; i++) accuracy[i]=StructureS.calculateAccuracyExposed(sRef, sIn, (i+1), perc_access);
        accuracyInE.add(accuracy);
        rmsdIn.add(StructureS.calculateSCRMSD(sRef, sIn));
    }

    /**
     * Calculates and saves the accuracy results for a final solution
     * @param sRef
     * @param sOut
     */
    public void calculateAccuraciesAndRSMDOut(StructureS sRef, StructureS sOut){
        double[] accuracy;
        accuracy = new double[numOfTorAng];
        for(int i=0; i<numOfTorAng; i++) accuracy[i]=StructureS.calculateAccuracy(sRef, sOut, (i+1));
        accuracyOut.add(accuracy);
        accuracy = new double[numOfTorAng];
        for(int i=0; i<numOfTorAng; i++) accuracy[i]=StructureS.calculateAccuracyBuried(sRef, sOut, (i+1), perc_access);
        accuracyOutB.add(accuracy);
        accuracy = new double[numOfTorAng];
        for(int i=0; i<numOfTorAng; i++) accuracy[i]=StructureS.calculateAccuracyExposed(sRef, sOut, (i+1), perc_access);
        accuracyOutE.add(accuracy);
        rmsdOut.add(StructureS.calculateSCRMSD(sRef, sOut));
    }

    /**
     * Calculations of accuracy for every type of residue
     * @param sNat
     * @param sCan
     */
    public void calcForAA(StructureS sNat, StructureS sCan){
        ChainS cNat,cCan;
        AminoAcidS aaNat,aaCan;
        int aminoInd;
        boolean[] corrAndEnab;
        int cont=0;
        for(int chain=0; chain<sNat.chains.length; chain++){
            cNat = sNat.chains[chain];
            cCan = sCan.chains[chain];
            for(int group=0; group<cNat.residues.length; group++){
                aaNat = cNat.residues[group];
                aaCan = cCan.residues[group];
                aminoInd = aaNat.aminoIndex;
                for(int i=0;i<numOfTorAng;i++){
                    corrAndEnab = AminoAcidS.isAccurate(aaNat, aaCan, (i+1));
                    if(corrAndEnab[0]) {accuracyAA[0][aminoInd][i]++; accuracyAA[0][numberOfAAs][i]++;}
                    if(corrAndEnab[1]) {counterAA[0][aminoInd][i]++; counterAA[0][numberOfAAs][i]++;}
                    if(perc_access[cont]<=LIMITDOWNACC){
                        if(corrAndEnab[0]) {accuracyAA[1][aminoInd][i]++; accuracyAA[1][numberOfAAs][i]++;}
                        if(corrAndEnab[1]) {counterAA[1][aminoInd][i]++; counterAA[1][numberOfAAs][i]++;}
                    }
                    if(perc_access[cont]>LIMITUPACC){
                        if(corrAndEnab[0]) {accuracyAA[2][aminoInd][i]++; accuracyAA[2][numberOfAAs][i]++;}
                        if(corrAndEnab[1]) {counterAA[2][aminoInd][i]++; counterAA[2][numberOfAAs][i]++;}
                    }
                }
                cont++;
            }
        }
    }

    /**
     * Saves data regarding the search method in an Excel file
     * @param pdbID
     * @param folder
     * @param values1
     * @param values2
     * @param numRes
     */
    public void createExcelSearch(String pdbID, String folder, double[] values1, double[] values2, int numRes) throws Exception{
        URL resource = OutputData.class.getResource(File.separator+"data"+File.separator+"TemplateSearch.xlsx");
        String FILE_PATH = folder+File.separator+pdbID+".xlsx";
        File destination = new File(FILE_PATH);
        try {
            Files.copy(Paths.get(resource.toURI()), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            logger.error("Problem copying the file {}", FILE_PATH);
        }

        FileInputStream fis;
        try {
            fis = new FileInputStream(FILE_PATH);
            Workbook workbook = new XSSFWorkbook(fis);
            Sheet sheet = workbook.getSheet("TOT");

            int rowC=1,colC=0;
            Font titleFont = workbook.createFont();
            titleFont.setBold(true);
            titleFont.setFontHeightInPoints((short) 20);
            titleFont.setColor(IndexedColors.BLACK.getIndex());
            CellStyle titleCellStyle = workbook.createCellStyle();
            titleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
            //increase row height to accomodate two lines of text



            titleCellStyle.setFont(titleFont);
            titleCellStyle.setBorderTop(BorderStyle.MEDIUM);
            titleCellStyle.setBorderRight(BorderStyle.MEDIUM);
            titleCellStyle.setBorderLeft(BorderStyle.MEDIUM);
            titleCellStyle.setBorderBottom(BorderStyle.MEDIUM);
            titleCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());



            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setWrapText(true); //Set wordwrap
            headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
            //increase row height to accomodate two lines of text



            headerCellStyle.setFont(headerFont);
            headerCellStyle.setBorderTop(BorderStyle.MEDIUM);
            headerCellStyle.setBorderRight(BorderStyle.MEDIUM);
            headerCellStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerCellStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            Row row = sheet.createRow(rowC++);
            Cell subtitle1 = row.createCell(colC++);
            subtitle1.setCellValue("Prot");
            subtitle1.setCellStyle(headerCellStyle);
            Cell subtitle2 = row.createCell(colC++);
            subtitle2.setCellValue("Res");
            subtitle2.setCellStyle(headerCellStyle);
            Cell subtitle3 = row.createCell(colC++);
            subtitle3.setCellValue("Iter");
            subtitle3.setCellStyle(headerCellStyle);
            Cell subtitle4 = row.createCell(colC++);
            subtitle4.setCellValue("Energy");
            subtitle4.setCellStyle(headerCellStyle);
            Cell subtitle5 = row.createCell(colC++);
            subtitle5.setCellValue("Energy2");
            subtitle5.setCellStyle(headerCellStyle);

            boolean printVal2 = (values2!=null);
            for(int i=0;i<values1.length;i++){
                row = sheet.createRow(rowC++);
                if(i==0){ row.createCell(0).setCellValue(pdbID); row.createCell(1).setCellValue(numRes);}
                colC=2;
                row.createCell(colC++).setCellValue(i);
                row.createCell(colC++).setCellValue(values1[i]);
                if(printVal2)row.createCell(colC++).setCellValue(values2[i]);
                else row.createCell(colC++).setCellValue(0.0);
            }

            //Modify ranges
            XSSFName[] ranges = new XSSFName[workbook.getNumberOfNames()];
            String formula,type,for1,for2;
            for (int i = 0; i < workbook.getNumberOfNames(); i++){
                ranges[i] = (XSSFName) workbook.getNameAt(i);
                type = ranges[i].getRefersToFormula().substring(7, 10);
                if(type.equals("TOT")){
                    formula = ranges[i].getRefersToFormula();
                    for1 = formula.substring(0,36);
                    for2 = formula.substring(37);
                    formula = for1+(values1.length+1)+for2;
                    ranges[i].setRefersToFormula(formula);
                }
            }
            int numberOfColum = sheet.getRow(1).getPhysicalNumberOfCells();
            for(int i=0; i<numberOfColum-1;i++)
                sheet.autoSizeColumn(i);
            //adjust column width to fit the content
            headerCellStyle.setWrapText(true);
            titleCellStyle.setWrapText(true);
            sheet.addMergedRegion(new CellRangeAddress(
                    0, //first row (0-based)
                    0, //last row  (0-based)
                    0, //first column (0-based)
                    numberOfColum-1  //last column  (0-based)
            ));

            FileOutputStream fos;
            fos = new FileOutputStream(FILE_PATH);
            workbook.write(fos);
            fis.close();
            fos.close();
        } catch (IOException ex) {
            logger.error("Problem creating the file {}", FILE_PATH);
        }
    }

    /**
     * Creates an Excel file with the results of all proteins in the list
     * @param folderOut
     * @param testName
     * @param eNames energy terms names
     */
    public void saveResults(String folderOut, String testName, String[] eNames){

        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Results");
        Font titleFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 20);
        titleFont.setColor(IndexedColors.BLACK.getIndex());
        CellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setWrapText(true); //Set wordwrap
        titleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
        //increase row height to accomodate two lines of text



        titleCellStyle.setFont(titleFont);
        titleCellStyle.setBorderTop(BorderStyle.MEDIUM);
        titleCellStyle.setBorderRight(BorderStyle.MEDIUM);
        titleCellStyle.setBorderLeft(BorderStyle.MEDIUM);
        titleCellStyle.setBorderBottom(BorderStyle.MEDIUM);
        titleCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());


        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setWrapText(true); //Set wordwrap
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        //increase row height to accomodate two lines of text



        headerCellStyle.setFont(headerFont);
        headerCellStyle.setBorderTop(BorderStyle.MEDIUM);
        headerCellStyle.setBorderRight(BorderStyle.MEDIUM);
        headerCellStyle.setBorderLeft(BorderStyle.MEDIUM);
        headerCellStyle.setBorderBottom(BorderStyle.MEDIUM);
        headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());


        int numOfProts=pdbNames.size();
        int fila=0;
        Row row = sheet.createRow(fila++);
        int col=0;
        Cell title0= row.createCell(col);
        title0.setCellValue("Results for a list of "+numOfProts+" proteins");
        title0.setCellStyle(titleCellStyle);
        row = sheet.createRow(fila++);

        col=4;

        Cell title1 = row.createCell(col);
        title1.setCellValue("Native Energies");col+=(eNames.length+1);
        title1.setCellStyle(headerCellStyle);

        Cell title2 = row.createCell(col);
        title2.setCellValue("Initial Energies");col+=eNames.length;
        title2.setCellStyle(headerCellStyle);

        Cell title3 = row.createCell(col);
        title3.setCellValue("Final Energies");col+=(eNames.length+4);
        title3.setCellStyle(headerCellStyle);

        Cell title4 = row.createCell(col);
        title4.setCellValue("Initial Accuracies");col+=(2+3*numOfTorAng);
        title4.setCellStyle(headerCellStyle);

        Cell title5 = row.createCell(col);
        title5.setCellValue("Final Accuracies");
        title5.setCellStyle(headerCellStyle);

        //Means calculations
        ArrayList<ArrayList<Double>> values = new ArrayList<>(numOfProts);
        for(int i=0;i<numOfProts;i++){
            values.add(new ArrayList<>(100));
            values.get(i).add(1.0*chainNums.get(i));
            values.get(i).add(1.0*resNums.get(i));
            for(int j=0;j<eNat.get(i).length;j++) values.get(i).add(eNat.get(i)[j]);
            values.get(i).add(1.0*collisionsNat.get(i));
            for(int j=0;j<eStart.get(i).length;j++) values.get(i).add(eStart.get(i)[j]);
            for(int j=0;j<eEnd.get(i).length;j++) values.get(i).add(eEnd.get(i)[j]);
            values.get(i).add(1.0*iterations.get(i));
            values.get(i).add(1.0*changes.get(i));
            values.get(i).add(1.0*timesI.get(i));
            values.get(i).add(1.0*timesS.get(i));
            for(int j=0;j<accuracyIn.get(i).length;j++) values.get(i).add(100*accuracyIn.get(i)[j]);
            for(int j=0;j<accuracyInB.get(i).length;j++) values.get(i).add(100*accuracyInB.get(i)[j]);
            for(int j=0;j<accuracyInE.get(i).length;j++) values.get(i).add(100*accuracyInE.get(i)[j]);
            values.get(i).add(1.0*collisionsIn.get(i));
            values.get(i).add(1.0*rmsdIn.get(i));
            for(int j=0;j<accuracyOut.get(i).length;j++) values.get(i).add(100*accuracyOut.get(i)[j]);
            for(int j=0;j<accuracyOutB.get(i).length;j++) values.get(i).add(100*accuracyOutB.get(i)[j]);
            for(int j=0;j<accuracyOutE.get(i).length;j++) values.get(i).add(100*accuracyOutE.get(i)[j]);
            values.get(i).add(1.0*collisionsOut.get(i));
            values.get(i).add(1.0*rmsdOut.get(i));
        }

        int cantCol=values.get(0).size();
        double[] promedios=new double[cantCol]; for(int i=0; i<promedios.length; i++) promedios[i]=0.0;
        for(int i=0;i<numOfProts;i++) for(int j=0;j<promedios.length;j++) promedios[j]+=values.get(i).get(j);
        for(int j=0;j<promedios.length;j++) promedios[j]/=numOfProts;

        //Output
        row = sheet.createRow(fila++);
        col=2;

        Cell subtitle1 = row.createCell(col++);
        subtitle1.setCellValue("NumOfChains");
        subtitle1.setCellStyle(headerCellStyle);

        Cell subtitle2 =row.createCell(col++);
        subtitle2.setCellValue("NumOfRes");
        subtitle2.setCellStyle(headerCellStyle);

        Cell subtitle3 = row.createCell(col++);
        subtitle3.setCellValue("ENat");
        subtitle3.setCellStyle(headerCellStyle);

        for(int i=1; i<eNames.length; i++) {
            int countCol = col++;
            Cell eNameSubtitle =row.createCell(countCol);
            eNameSubtitle.setCellValue(eNames[i]);
            row.getCell(countCol).setCellStyle(headerCellStyle);

        }

        Cell subtitle4 = row.createCell(col++);
        subtitle4.setCellValue("CollNat");
        subtitle4.setCellStyle(headerCellStyle);
        Cell subtitle5 = row.createCell(col++);
        subtitle5.setCellValue("EStart");
        subtitle5.setCellStyle(headerCellStyle);

        for(int i=1; i<eNames.length; i++) {
            Cell subtitleE2 = row.createCell(col++);
            subtitleE2.setCellValue(eNames[i]);
            subtitleE2.setCellStyle(headerCellStyle);

        }

        Cell subtitle6 = row.createCell(col++);
        subtitle6.setCellValue("EEnd");
        subtitle6.setCellStyle(headerCellStyle);

        for(int i=1; i<eNames.length; i++) {
            Cell subttleE1 =row.createCell(col++);
            subttleE1.setCellValue(eNames[i]);
            subttleE1.setCellStyle(headerCellStyle);
        }

        Cell subtitle7 = row.createCell(col++);
        subtitle7.setCellValue("Iter");
        subtitle7.setCellStyle(headerCellStyle);

        Cell subtitle8 = row.createCell(col++);
        subtitle8.setCellValue("RotChanges");
        subtitle8.setCellStyle(headerCellStyle);

        Cell subtitle9 = row.createCell(col++);
        subtitle9.setCellValue("TimeI");
        subtitle9.setCellStyle(headerCellStyle);

        Cell subtitle10 = row.createCell(col++);
        subtitle10.setCellValue("TimeS");
        subtitle10.setCellStyle(headerCellStyle);

        String[] suf = {"", "(B)", "(E)"};
        StringBuilder x;
        for(String s: suf){
            x = new StringBuilder("X");
            for(int i=0; i<numOfTorAng; i++){
                if(i==0) x.append("1");
                else x.append("+").append(i + 1);

                Cell subtilteX = row.createCell(col++);
                subtilteX.setCellValue(x+s);
                subtilteX.setCellStyle(headerCellStyle);
            }
        }
        Cell subtitle11 = row.createCell(col++);
        subtitle11.setCellValue("CollisionsIn");
        subtitle11.setCellStyle(headerCellStyle);

        Cell subtitle12 = row.createCell(col++);
        subtitle12.setCellValue("RMSDIn");
        subtitle12.setCellStyle(headerCellStyle);

        for(String s: suf){
            x = new StringBuilder("X");
            for(int i=0; i<numOfTorAng; i++){
                if(i==0) x.append("1");
                else x.append("+").append((i + 1));
                Cell subtitleX = row.createCell(col++);
                subtitleX.setCellValue(x+s);
                subtitleX.setCellStyle(headerCellStyle);
            }
        }
        Cell subtitle13 = row.createCell(col++);
        subtitle13.setCellValue("CollisionsOut");
        subtitle13.setCellStyle(headerCellStyle);

        Cell subtitle14 = row.createCell(col++);
        subtitle14.setCellValue("RMSD");
        subtitle14.setCellStyle(headerCellStyle);

        row = sheet.createRow(fila++);
        fila+=1;
        col=2;
        for(int j=0;j<promedios.length;j++) row.createCell(col++).setCellValue(round3(promedios[j]));

        row = sheet.createRow(fila++);
        col=0;
        Cell subtitle15 = row.createCell(col++);
        subtitle15.setCellValue("PDB ID");
        subtitle15.setCellStyle(headerCellStyle);

        Cell subtitle16 = row.createCell(col++);
        subtitle16.setCellValue("Complete?");
        subtitle16.setCellStyle(headerCellStyle);

        Cell subtitle17 = row.createCell(col++);
        subtitle17.setCellValue("NumOfChains");
        subtitle17.setCellStyle(headerCellStyle);

        Cell subtitle18= row.createCell(col++);
        subtitle18.setCellValue("NumOfRes");
        subtitle18.setCellStyle(headerCellStyle);

        Cell subtitle19 = row.createCell(col++);
        subtitle19.setCellValue("ENat");
        subtitle19.setCellStyle(headerCellStyle);

        for(int i=1; i<eNames.length; i++) {
            Cell eName = row.createCell(col++);
            eName.setCellValue(eNames[i]);
            eName.setCellStyle(headerCellStyle);
        }

        Cell subtitle20 = row.createCell(col++);
        subtitle20.setCellValue("CollNat");
        subtitle20.setCellStyle(headerCellStyle);

        Cell subtitle21 = row.createCell(col++);
        subtitle21.setCellValue("EStart");
        subtitle21.setCellStyle(headerCellStyle);

        for(int i=1; i<eNames.length; i++) {
            Cell eName =  row.createCell(col++);
            eName.setCellValue(eNames[i]);
            eName.setCellStyle(headerCellStyle);
        }

        Cell subtitle23 = row.createCell(col++);
        subtitle23.setCellValue("EEnd");
        subtitle23.setCellStyle(headerCellStyle);

        for(int i=1; i<eNames.length; i++) {
            Cell eName = row.createCell(col++);
            eName.setCellValue(eNames[i]);
            eName.setCellStyle(headerCellStyle);
        }

        Cell subtitle24 = row.createCell(col++);
        subtitle24.setCellValue("Iter");
        subtitle24.setCellStyle(headerCellStyle);

        Cell subtitle25 = row.createCell(col++);
        subtitle25.setCellValue("RotChanges");
        subtitle25.setCellStyle(headerCellStyle);

        Cell subtitle26 = row.createCell(col++);
        subtitle26.setCellValue("TimeI");
        subtitle26.setCellStyle(headerCellStyle);

        Cell subtitle27 = row.createCell(col++);
        subtitle27.setCellValue("TimeS");
        subtitle27.setCellStyle(headerCellStyle);

        for(String s: suf){
            x = new StringBuilder("X");
            for(int i=0; i<numOfTorAng; i++){
                if(i==0) x.append("1");
                else x.append("+").append((i + 1));
                Cell subtitleX = row.createCell(col++);
                subtitleX.setCellValue(x+s);
                subtitleX.setCellStyle(headerCellStyle);
            }
        }

        Cell subtitle28 = row.createCell(col++);
        subtitle28.setCellValue("CollisionsIn");
        subtitle28.setCellStyle(headerCellStyle);

        Cell subtitle29 = row.createCell(col++);
        subtitle29.setCellValue("RMSDIn");
        subtitle29.setCellStyle(headerCellStyle);

        for(String s: suf){
            x = new StringBuilder("X");
            for(int i=0; i<numOfTorAng; i++){
                if(i==0) x.append("1");
                else x.append("+").append((i + 1));
                Cell subtitleX = row.createCell(col++);
                subtitleX.setCellValue(x+s);
                subtitleX.setCellStyle(headerCellStyle);
            }
        }
        Cell subtitle30 = row.createCell(col++);
        subtitle30.setCellValue("CollisionsOut");
        subtitle30.setCellStyle(headerCellStyle);

        Cell subtitle31 = row.createCell(col++);
        subtitle31.setCellValue("RMSDOut");
        subtitle31.setCellStyle(headerCellStyle);

        for(int i=0;i<numOfProts;i++){
            row = sheet.createRow(fila++);
            col=0;
            row.createCell(col++).setCellValue(pdbNames.get(i));
            if(isComplete.get(i)) row.createCell(col++).setCellValue("YES");
            else row.createCell(col++).setCellValue("NO");
            row.createCell(col++).setCellValue(chainNums.get(i));
            row.createCell(col++).setCellValue(resNums.get(i));
            for(int j=0;j<eNat.get(i).length;j++) row.createCell(col++).setCellValue(round2(eNat.get(i)[j]));
            row.createCell(col++).setCellValue(collisionsNat.get(i));
            for(int j=0;j<eStart.get(i).length;j++) row.createCell(col++).setCellValue(round2(eStart.get(i)[j]));
            for(int j=0;j<eEnd.get(i).length;j++) row.createCell(col++).setCellValue(round2(eEnd.get(i)[j]));
            row.createCell(col++).setCellValue(iterations.get(i));
            row.createCell(col++).setCellValue(changes.get(i));
            row.createCell(col++).setCellValue(timesI.get(i));
            row.createCell(col++).setCellValue(timesS.get(i));
            for(int j=0; j<accuracyIn.get(i).length; j++) row.createCell(col++).setCellValue(round2(100*accuracyIn.get(i)[j]));
            for(int j=0; j<accuracyIn.get(i).length; j++) row.createCell(col++).setCellValue(round2(100*accuracyInB.get(i)[j]));
            for(int j=0; j<accuracyIn.get(i).length; j++) row.createCell(col++).setCellValue(round2(100*accuracyInE.get(i)[j]));
            row.createCell(col++).setCellValue(collisionsIn.get(i));
            row.createCell(col++).setCellValue(round3(rmsdIn.get(i)));
            for(int j=0; j<accuracyOut.get(i).length; j++) row.createCell(col++).setCellValue(round2(100*accuracyOut.get(i)[j]));
            for(int j=0; j<accuracyOut.get(i).length; j++) row.createCell(col++).setCellValue(round2(100*accuracyOutB.get(i)[j]));
            for(int j=0; j<accuracyOut.get(i).length; j++) row.createCell(col++).setCellValue(round2(100*accuracyOutE.get(i)[j]));
            row.createCell(col++).setCellValue(collisionsOut.get(i));
            row.createCell(col++).setCellValue(round3(rmsdOut.get(i)));

        }
        int numberOfColum = sheet.getRow(5).getPhysicalNumberOfCells();
        for(int i=0; i<numberOfColum-1;i++)
            sheet.autoSizeColumn(i);
        //adjust column width to fit the content
        headerCellStyle.setWrapText(true);
        titleCellStyle.setWrapText(true);
        sheet.addMergedRegion(new CellRangeAddress(
                0, //first row (0-based)
                0, //last row  (0-based)
                0, //first column (0-based)
                numberOfColum-1  //last column  (0-based)
        ));
        String FILE_PATH = folderOut+File.separator+"Results"+testName+".xlsx";
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(FILE_PATH);
            workbook.write(fos);
            fos.close();
        } catch (IOException ex) {
            logger.error("Problem creating excel file", ex);
        }
    }

    /**
     * Creates an excel file with results for each type of residue
     * @param folderOut
     * @param testName
     */
    public void saveResultsAA(String folderOut, String testName){
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Results_AA");

        Font titleFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 32);
        titleFont.setColor(IndexedColors.BLACK.getIndex());

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 16);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setWrapText(true); //Set wordwrap
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        //increase row height to accomodate two lines of text

        headerCellStyle.setFont(headerFont);
        headerCellStyle.setBorderTop(BorderStyle.MEDIUM);
        headerCellStyle.setBorderRight(BorderStyle.MEDIUM);
        headerCellStyle.setBorderLeft(BorderStyle.MEDIUM);
        headerCellStyle.setBorderBottom(BorderStyle.MEDIUM);
        headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());


        CellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setWrapText(true); //Set wordwrap
        titleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
        //increase row height to accomodate two lines of text

        titleCellStyle.setFont(titleFont);
        titleCellStyle.setBorderTop(BorderStyle.MEDIUM);
        titleCellStyle.setBorderRight(BorderStyle.MEDIUM);
        titleCellStyle.setBorderLeft(BorderStyle.MEDIUM);
        titleCellStyle.setBorderBottom(BorderStyle.MEDIUM);
        titleCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        int fila=0;
        Row row = sheet.createRow(fila++);
        int col=0;
        Cell title = row.createCell(col);
        title.setCellValue("Accuracy Results for a list of "+(pdbNames.size())+" proteins");
        title.setCellStyle(titleCellStyle);
        row = sheet.createRow(fila++);
        col=1;
        Cell subtitle1 = row.createCell(col);
        subtitle1.setCellValue("Total");
        subtitle1.setCellStyle(headerCellStyle);
        col+=numOfTorAng*3;
        Cell subtitle2 = row.createCell(col);
        subtitle2.setCellValue("Buried");
        subtitle2.setCellStyle(headerCellStyle);
        col+=numOfTorAng*3;
        Cell subtitle3 = row.createCell(col);
        subtitle3.setCellValue("Exposed");
        subtitle3.setCellStyle(headerCellStyle);
        row = sheet.createRow(fila++);
        col=0;
        Cell subtitle4 = row.createCell(col++);
        subtitle4.setCellValue("AA");
        subtitle4.setCellStyle(headerCellStyle);

        String[] suf = {"", "(A)", "(T)"};
        String s;
        for(int j=0; j<3; j++){
            s="X";
            for(int i=0; i<numOfTorAng; i++){
                if(i==0) s=s+(i+1);
                else s=s+"+"+(i+1);
                for(int k=0; k<3; k++) {
                    Cell cell = row.createCell(col++);
                    cell.setCellValue(s+suf[k]);
                    cell.setCellStyle(headerCellStyle);
                }
            }
        }

        //Total
        row = sheet.createRow(fila++);
        col=0;
        Cell subtitle5 = row.createCell(col++);
        subtitle5.setCellValue("Total");
        subtitle5.setCellStyle(headerCellStyle);
        for(int j=0; j<3; j++){
            for(int i=0; i<numOfTorAng; i++){
                if(counterAA[j][numberOfAAs][i]>0) row.createCell(col++).setCellValue(round2(100.0*accuracyAA[j][numberOfAAs][i]/counterAA[j][numberOfAAs][i]));
                else row.createCell(col++).setCellValue(-1);
                row.createCell(col++).setCellValue(accuracyAA[j][numberOfAAs][i]);
                row.createCell(col++).setCellValue(counterAA[j][numberOfAAs][i]);
            }
        }

        for(int k=0; k<numberOfAAs; k++){
            row = sheet.createRow(fila++);
            col=0;
            row.createCell(col++).setCellValue(ListUtil.aaThreeLetterCode[k]);
            for(int j=0; j<3; j++){
                for(int i=0; i<numOfTorAng; i++){
                    if(counterAA[j][k][i]>0) row.createCell(col++).setCellValue(round2(100.0*accuracyAA[j][k][i]/counterAA[j][k][i]));
                    else row.createCell(col++).setCellValue(-1);
                    row.createCell(col++).setCellValue(accuracyAA[j][k][i]);
                    row.createCell(col++).setCellValue(counterAA[j][k][i]);
                }
            }
        }

        int numberOfColum = sheet.getRow(2).getPhysicalNumberOfCells();
        for(int i=0; i<numberOfColum-1;i++)
            sheet.autoSizeColumn(i);
        //adjust column width to fit the content
        headerCellStyle.setWrapText(true);
        sheet.addMergedRegion(new CellRangeAddress(
                0, //first row (0-based)
                0, //last row  (0-based)
                0, //first column (0-based)
                numberOfColum-1  //last column  (0-based)
        ));

        String FILE_PATH = folderOut+File.separator+"Results_AA"+testName+".xlsx";
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(FILE_PATH);
            workbook.write(fos);
            fos.close();
        } catch (IOException ex) {
           logger.error("Problem creating excel file: {}", FILE_PATH);
        }
    }

    /**
     * Saves the information about rotamers
     * @param p
     * @param folderOut
     * @param pdb
     * @param resCodes
     */
    public void writeRotamers(Protein p, String folderOut, String pdb, List<String> resCodes){
        try {
            PrintWriter outFile;
            File fileOut = new File(folderOut+File.separator+pdb+"_Rots.txt");
            outFile = new PrintWriter(new BufferedWriter(new FileWriter(fileOut)));
            outFile.println("Rotamers for "+pdb);
            outFile.println("SeqN\tRes\tPhi\tPsi\tRot");
            int[] phi = p.phi;
            int[] psi = p.psi;
            int[] rotIndices = p.rotamersIndex;
            AminoAcidS[] residuesBB = p.residuesBB;
            for(int i=0; i<p.strucSize; i++){
                outFile.println(resCodes.get(i)+"\t"+ListUtil.aaThreeLetterCode[residuesBB[i].aminoIndex]+"\t"+phi[i]+"\t"+psi[i]+"\t"+rotIndices[i]);
            }
            outFile.close();
        } catch (IOException ex) {
           logger.error("Problem saving rotamers");
        }
    }

    private double round2(double x){
        return Math.round(100 * x) / 100.0;
    }
    private double round3(double x){
        return Math.round(1000 * x) / 1000.0;
    }
}

