package py.com.pol.biocomputacion.pscpp.protein;

import py.com.pol.biocomputacion.pscpp.data.AtomData;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;
import py.com.pol.biocomputacion.pscpp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringJoiner;

public class AminoAcidS implements Comparable<AminoAcidS> {
    public final AtomS[] atoms;
    public final int aminoIndex;
    public final int iD; //internal PDBCode
    private static final int CYSINDEX, PROINDEX, SGCYSINDEX, CDPROINDEX, CATOMINDEX, ARGINDEX, NH1ARGINDEX, NH2ARGINDEX;
    private static final boolean[][] IS_OXYGEN; //indicates if an atom is an oxygen.
    private static final double ALPHA, LIMITACC;
    private static final boolean ALLSCATOMS;
    private  Boolean start;
    private  Boolean end;

    static {
        ALLSCATOMS = (Boolean) ParameterFile.getParameters().get(ParameterFile.BOOL_ALL_SC_ATOMS);
        LIMITACC = (Double) ParameterFile.getParameters().get(ParameterFile.DOUBLE_LIMIT_ACCURACY);
        ALPHA = (Double) ParameterFile.getParameters().get(ParameterFile.DOUBLE_ALPHA_COLISION_FACTOR);
        ARGINDEX = ListUtil.aaNamePosition.get("ARG");
        NH1ARGINDEX = ListUtil.atomNamePosition.get(ARGINDEX).get("NH1");
        NH2ARGINDEX = ListUtil.atomNamePosition.get(ARGINDEX).get("NH2");
        CYSINDEX = ListUtil.aaNamePosition.get("CYS");
        SGCYSINDEX = ListUtil.atomNamePosition.get(CYSINDEX).get("SG");
        PROINDEX = ListUtil.aaNamePosition.get("PRO");
        CDPROINDEX = ListUtil.atomNamePosition.get(PROINDEX).get("CD");
        CATOMINDEX = ListUtil.atomNamePosition.get(PROINDEX).get("C");
        IS_OXYGEN = new boolean[ListUtil.resNum][];
        String atom;
        int numAtoms;
        for (int i = 0; i < ListUtil.resNum; i++) {
            numAtoms = ListUtil.atomGenOrder[i].length;
            IS_OXYGEN[i] = new boolean[numAtoms];
            for (int j = 0; j < numAtoms; j++) {
                atom = ListUtil.atomGenOrder[i][j];
                if (atom.substring(0, 1).equals("O")) IS_OXYGEN[i][j] = true;
                else IS_OXYGEN[i][j] = false;
            }
        }
    }

    /**
     * Constructor with the amino acid index (position of AA identifier in ListUtil.ATOM_LIST) and ID as input.
     *
     * @param aminoIndex
     * @param iD
     */
    public AminoAcidS(int aminoIndex, int iD) {
        this.aminoIndex = aminoIndex;
        atoms = new AtomS[ListUtil.atomGenOrder[aminoIndex].length];
        this.iD = iD;
    }

    /**
     * Method for cloning an AminoAcidS object
     *
     * @return
     */
    public AminoAcidS cloneAminoAcidS() {
        AminoAcidS aaOut = new AminoAcidS(aminoIndex, iD);
        for (int i = 0; i < this.atoms.length; i++) {
            if (this.atoms[i] != null) {
                aaOut.atoms[i] = this.atoms[i].cloneAtomS();
                aaOut.setIsStart(this.isStart());
                aaOut.setIsEnd(this.isEnd());
            }
            else aaOut.atoms[i] = null;
        }
        return aaOut;
    }

    /**
     * Method for cloning an AminoAcidS object, considering only the BB atoms (CB included).
     *
     * @return
     */
    public AminoAcidS cloneBBAtomsAminoAcidS() {
        AminoAcidS aaOut = new AminoAcidS(aminoIndex, iD);
        int numBBAtoms = 5;
        int numAtoms = atoms.length;
        if (numAtoms < numBBAtoms) numBBAtoms = numAtoms;
        for (int i = 0; i < numBBAtoms; i++) {
            if (this.atoms[i] != null) aaOut.atoms[i] = this.atoms[i].cloneAtomS();
            else aaOut.atoms[i] = null;
        }
        return aaOut;
    }

     /**
     * Indicates if a residue is 'accurate' with respect to a reference (torsion angle difference is less than or equal to a given value)
     * @param pdbAA reference residue
     * @param methodAA evaluated residue
     * @param chi number of torsion angles
     * @return
     */
    static public boolean[] isAccurate(AminoAcidS pdbAA, AminoAcidS methodAA, int chi){
        boolean[] correctAndEnabled = new boolean[2];
        correctAndEnabled[0] = true; //is correct?
        correctAndEnabled[1] = true; //adds to the total account?

        if(pdbAA.hasMissingSCAtoms() && ALLSCATOMS){
            correctAndEnabled[0] = false;
            correctAndEnabled[1] = false;
            return correctAndEnabled;
        }

        int aminoAcidIndex = pdbAA.aminoIndex;
        int torAngles = ListUtil.numSidechainTorsion[aminoAcidIndex];

        if(torAngles<chi){
            correctAndEnabled[0] = false;
            correctAndEnabled[1] = false;
            return correctAndEnabled;
        }

        double pdbAATorAngle, methodAATorAngle;
        AtomS a1,a2,a3,a4;
        AtomData[] atomsData = ListUtil.atomGenData[aminoAcidIndex];
        AtomData atData;
        int x1,x2,x3,x4;
        int[] refAtoms;

        //Verification of angles
        for(int i=0; i<chi; i++) {
            atData = atomsData[(i+5)];
            refAtoms = atData.refAtomsPos;
            x1=refAtoms[0];
            x2=refAtoms[1];
            x3=refAtoms[2];
            x4=(i+5);
            a1 = pdbAA.atoms[x1];
            a2 = pdbAA.atoms[x2];
            a3 = pdbAA.atoms[x3];
            a4 = pdbAA.atoms[x4];
            if(a1!=null && a2!=null && a3!=null && a4!=null) {
                pdbAATorAngle = AtomS.getTorsionAngle(a1,a2,a3,a4);
                a1 = methodAA.atoms[x1];
                a2 = methodAA.atoms[x2];
                a3 = methodAA.atoms[x3];
                a4 = methodAA.atoms[x4];
                if(a1!=null && a2!=null && a3!=null && a4!=null) {
                    methodAATorAngle = AtomS.getTorsionAngle(a1,a2,a3,a4);
                    if(Utils.angleDev(aminoAcidIndex,pdbAATorAngle,methodAATorAngle,(i+1))>LIMITACC) {
                        correctAndEnabled[0] = false;
                        break;
                    }
                }
                else{
                    correctAndEnabled[0] = false;
                    correctAndEnabled[1] = false;
                    return correctAndEnabled;
                }
            }
            else{
                correctAndEnabled[0] = false;
                correctAndEnabled[1] = false;
                return correctAndEnabled;
            }
        }
        return correctAndEnabled;
    }


    /**
     * Indicates if an amino acid has a missing side-chain main atom
     * @return
     */
    public boolean hasMissingSCAtoms(){
        for (AtomS atom : atoms) if (atom == null) return true;
        return false;
    }

    /**
     * Creates the side-chain atoms of a residue, according to BB atoms and a rotamer.
     * @param aaBB residue with BB atoms only.
     * @param r rotamer
     * @return
     */
    public static AminoAcidS createAminoacid(AminoAcidS aaBB, Rotamer r) {
        double chi, delta, k;
        AtomS a;
        int aminoAcidIndex = aaBB.aminoIndex;

        //BB atoms
        AminoAcidS aaOut = new AminoAcidS(aminoAcidIndex, aaBB.iD);
        int numAtoms = aaBB.atoms.length;
        int numBBAtoms = 5;
        if(numAtoms<numBBAtoms) numBBAtoms = numAtoms;
        for(int i=0; i<numBBAtoms; i++) aaOut.atoms[i] = aaBB.atoms[i];

        AtomData atData;
        AtomData[] atomsData = ListUtil.atomGenData[aminoAcidIndex];
        int[] refAtoms;
        double[] bondValues;
        double[] chiAngles = r.getChi();
        int numTorAng = chiAngles.length;
        //Side-chain main atoms
        for(int i=0; i<numTorAng; i++){
            atData = atomsData[(i+5)];
            refAtoms = atData.refAtomsPos;
            bondValues = atData.bondValues;
            delta = bondValues[1]; k = bondValues[0]; chi = chiAngles[i];
            a = AtomS.getAtom(aaOut.atoms[refAtoms[0]], aaOut.atoms[refAtoms[1]], aaOut.atoms[refAtoms[2]], chi, delta, k);
            aaOut.atoms[i+5] = a;
        }

        //Side-chain secondary atoms
        for(int i=(5+numTorAng); i<aaOut.atoms.length; i++){
            atData = atomsData[i];
            refAtoms = atData.refAtomsPos;
            bondValues = atData.bondValues;
            delta = bondValues[1]; k = bondValues[0];
            if(refAtoms.length==4) chi = AtomS.getTorsionAngle(aaOut.atoms[refAtoms[0]], aaOut.atoms[refAtoms[1]], aaOut.atoms[refAtoms[2]], aaOut.atoms[refAtoms[3]]);
            else chi = 0.0;
            a = AtomS.getAtom(aaOut.atoms[refAtoms[0]], aaOut.atoms[refAtoms[1]], aaOut.atoms[refAtoms[2]], (chi+bondValues[2]), delta, k);
            aaOut.atoms[i]= a;
        }
        aaOut.setIsEnd(aaBB.isEnd());
        aaOut.setIsStart(aaBB.isStart());
        return aaOut;
    }

    /**
     * Returns the number of collisions between two residues
     * @param aa1
     * @param aa2
     * @return
     */
    public static int calcCollisions(AminoAcidS aa1, AminoAcidS aa2){
        int col=0;
        int aminoAcidIndex1 = aa1.aminoIndex;
        int aminoAcidIndex2 = aa2.aminoIndex;
        AtomS at1,at2;
        double distance, vdWradius1, vdWradius2, MIN_DISTANCE;
        double[] vdWRadiis1 = ListUtil.vdWRadii[aminoAcidIndex1];
        double[] vdWRadiis2 = ListUtil.vdWRadii[aminoAcidIndex2];
        boolean specialCase;
        boolean[] isOx1 = IS_OXYGEN[aminoAcidIndex1];
        boolean[] isOx2 = IS_OXYGEN[aminoAcidIndex2];

        for(int i=0; i<aa1.atoms.length; i++){
            at1 = aa1.atoms[i];
            if(at1==null) continue;
            vdWradius1 = vdWRadiis1[i];
            for(int j=0; j<aa2.atoms.length; j++){
                at2 = aa2.atoms[j];
                if(at2==null) continue;
                if(i>4 || j>4){
                    distance = AtomS.getDistance(at1, at2);
                    vdWradius2 = vdWRadiis2[j];
                    MIN_DISTANCE = vdWradius1 + vdWradius2;
                    if (distance <= (MIN_DISTANCE*ALPHA)) {
                        specialCase = false;
                        // Special cases: possible disulfide bridge
                        if(aminoAcidIndex1==CYSINDEX && i==SGCYSINDEX && aminoAcidIndex2==CYSINDEX && j==SGCYSINDEX) specialCase=true;
                        // CD of PRO and C of previous residue
                        if(aminoAcidIndex1==PROINDEX || aminoAcidIndex2==PROINDEX){
                            if(aminoAcidIndex1==PROINDEX && aa2.iD==(aa1.iD-1)){
                                if(i==CDPROINDEX && j==CATOMINDEX) specialCase=true;
                            }
                            if(aminoAcidIndex2==PROINDEX && aa1.iD==(aa2.iD-1)){
                                if(j==CDPROINDEX && i==CATOMINDEX) specialCase=true;
                            }
                        }
                        //Possible hydrogen bonding
                        if(isOx1[i] && isOx2[j]) specialCase=true;
                        if(!specialCase) col++;
                    }
                }
            }
        }
        return col;
    }

    /**
     * Method that returns the 'direct' RMSD between side-chain atoms of two residues
     * @param aa1
     * @param aa2
     * @return
     */
    public static double calculateSCRMSDAAsOr(AminoAcidS aa1, AminoAcidS aa2){
        int maxNumAtoms = aa1.atoms.length;
        ArrayList<AtomS> alAtom1 = new ArrayList(maxNumAtoms);
        ArrayList<AtomS> alAtom2 = new ArrayList(maxNumAtoms);
        AtomS at1,at2;
        for(int j=5;j<maxNumAtoms;j++){ //CB is in the BB
            at1=aa1.atoms[j]; at2=aa2.atoms[j];
            if (at1!=null && at2!=null) {
                alAtom1.add(at1);
                alAtom2.add(at2);
            }
        }
        double rmsd;
        if(alAtom1.isEmpty()) rmsd=(999999.0);
        else rmsd = AtomS.getRMSD(alAtom1, alAtom2);
        return rmsd;
    }

    /**
     * Method that returns the RMSD between side-chain atoms of two residues, considering the observations from Eyal(2004)
     * @param aa1 Original amino acid
     * @param aa2 Created amino acid (it is assumed that is always complete)
     * @return
     */
    static public double calculateSCRMSDAAs(AminoAcidS aa1, AminoAcidS aa2) {
        int aminoAcidIndex = aa1.aminoIndex;
        double[] chi = new double[ListUtil.numSidechainTorsion[aminoAcidIndex]];
        AtomData atData;
        AtomData[] atomsData = ListUtil.atomGenData[aminoAcidIndex];
        int[] refAtoms;
        for(int j=0;j<chi.length;j++){
            atData = atomsData[(j+5)];
            refAtoms = atData.refAtomsPos;
            chi[j] = AtomS.getTorsionAngle(aa2.atoms[refAtoms[0]], aa2.atoms[refAtoms[1]], aa2.atoms[refAtoms[2]], aa2.atoms[j+5]);
        }
        //Special cases
        String aaName = ListUtil.aaThreeLetterCode[aminoAcidIndex];
        if(aaName.equalsIgnoreCase("ASP") || aaName.equalsIgnoreCase("PHE") || aaName.equalsIgnoreCase("TYR") || aaName.equalsIgnoreCase("ASN") || aaName.equalsIgnoreCase("HIS")){
            chi[1] = chi[1]+180.0;
            if(chi[1]>180.0) chi[1] = chi[1]-360.0;
        }
        if(aaName.equalsIgnoreCase("GLN") || aaName.equalsIgnoreCase("GLU")){
            chi[2] = chi[2]+180.0;
            if(chi[2]>180.0) chi[2] = chi[2]-360.0;
        }
        Rotamer r = new Rotamer();
        r.setChi(chi);
        r.setProbability(0.0);
        AminoAcidS aA2BB = aa2.cloneBBAtomsAminoAcidS();
        AminoAcidS aa2Mod = AminoAcidS.createAminoacid(aA2BB, r);
        AtomS aux;
        if(aminoAcidIndex==ARGINDEX){
            aux = aa2Mod.atoms[NH1ARGINDEX];
            aa2Mod.atoms[NH1ARGINDEX] = aa2Mod.atoms[NH2ARGINDEX];
            aa2Mod.atoms[NH2ARGINDEX] = aux;
        }
        double rmsd = calculateSCRMSDAAsOr(aa1,aa2);
        double rmsdMod = calculateSCRMSDAAsOr(aa1,aa2Mod);
        if(rmsdMod<rmsd) return rmsdMod;
        else return rmsd;
    }


    public  void setIsStart(Boolean start) {
        this.start = start;
    }

    public  Boolean isEnd() {
        return end;
    }

    public  void setIsEnd(Boolean end) {
        this.end = end;
    }
    public Boolean isStart(){
        return this.start;
    }

    @Override
    public String toString() {
        return new StringJoiner(", \n", AminoAcidS.class.getSimpleName() + "[", "]")
                .add("atoms=" + Arrays.toString(atoms))
                .add("aminoIndex=" + aminoIndex)
                .add("iD=" + iD)
                .add("start=" + start)
                .add("end=" + end)
                .toString();
    }

    @Override
    public int compareTo(AminoAcidS o) {

        return Integer.compare(this.iD, o.iD);
    }
}
