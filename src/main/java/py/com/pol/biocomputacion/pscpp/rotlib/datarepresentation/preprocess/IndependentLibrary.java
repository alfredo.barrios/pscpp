package py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess;

import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;

/**
 * Represents an Independent Library with the amino as the key and a single rotamer.
 */
public class IndependentLibrary {
    private String amino;
    private Rotamer rotamer;

    public IndependentLibrary() {
    }

    public String getAmino() {
        return amino;
    }

    public void setAmino(String amino) {
        this.amino = amino;
    }

    public Rotamer getRotamer() {
        return rotamer;
    }

    public void setRotamer(Rotamer rotamer) {
        this.rotamer = rotamer;
    }

    @Override
    public String toString() {
        return "IndependentLibrary{" +
                "amino='" + amino + '\'' +
                ", rotamer=" + rotamer +
                '}';
    }
}
