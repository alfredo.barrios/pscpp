package py.com.pol.biocomputacion.pscpp.desktoptests;

import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.pdb.PDBReader;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;
import py.com.pol.biocomputacion.pscpp.rotlib.dataprocessing.ProcessRotamerLibrary;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

public class CalculatePhiPsiAngles {
    static int [] oldPhi;
    static int [] oldPsi;
    /**
     *
     * Simple Main Test, for human expert compares
     *
     * Compare if two files of Phi, Psi angles are identical
     * */
    public static void main(String[] args) {
        try {
            ParameterFile.loadAll();
            ListUtil.initializeLists();
            PDBReader reader = new PDBReader();
            // Full path of the Pdb Folder
            // example of args[0] =  /home/alfredo/Documents/2018/biocomputacion/NewDatasets/PDB65
            // Name of the protein
            // example of args [1] = "1a7s"
            StructureS structureS = reader.readStructure(args[0], args[1]);
            System.out.println(structureS);
            Protein protein = new Protein(structureS);
            //PreprocessRotamerLibrary.generateBinaryFileForDependentLibrary("/home/alfredo/Documents/2018/biocomputacion/BibliotecasDeRotameros/bbdep02.May.sortlib","/home/alfredo/Documents/2018/biocomputacion/BibliotecasDeRotameros/Library",10);
            long t1=System.currentTimeMillis();
            //The path of full rotamerLibrary in binary format
            // example of args[2] = /home/alfredo/Documents/2018/biocomputacion/BibliotecasDeRotameros/Library.bin"
            RotamerCollection collection=ProcessRotamerLibrary.readBinaryFile(args[2]);
            long t2=System.currentTimeMillis();
            System.out.println("Time To read Binary File: "+(t2-t1)+"ms");
            protein.calculatePhiAndPsiAngles(collection.getInterval());
            // The correct set of angles for comparison purposes
            //example of args [3] = /home/alfredo/Documents/2018/biocomputacion/BioCode/pruebas/CalculatePhiPsi1A7S.txt
            readAngles(args[3]);
            System.out.println("The PHI angles are equals? :"+ Arrays.equals(oldPhi,protein.phi));
            System.out.println("The PSI angles equals? :"+Arrays.equals(oldPsi,protein.psi));

        } catch (Exception e) {
            System.out.println("Exeption in Calculate Phi");
        }
    }
    private static void readAngles(String filename){
        try {
            BufferedReader bf = new BufferedReader(new FileReader(filename));
            String phi=bf.readLine();
            String[] phis=phi.split(",");
            oldPhi=new int[phis.length];
            for(int i=0; i<phis.length;i++){
                oldPhi[i] = Integer.valueOf(phis[i].trim());
            }
            bf.readLine(); //space
            String psi=bf.readLine();
            String[] psis=psi.split(",");
            oldPsi=new int[psis.length];
            for(int i=0; i<psis.length;i++){
                oldPsi[i] = Integer.valueOf(psis[i].trim());
            }
        }catch (Exception e){
            System.out.println("Error reading the phi, psi angles: "+e);
        }
    }
}
