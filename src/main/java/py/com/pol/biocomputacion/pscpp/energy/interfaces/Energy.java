package py.com.pol.biocomputacion.pscpp.energy.interfaces;

import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;

public interface Energy {

    /**
     * Method that returns the energy terms values for a Structure
     *
     * @param s
     * @param pRef Reference protein (the one with the native structure)
     * @return
     */
    double[] calculateEnergies(StructureS s, Protein pRef);

    /**
     * Calculates the energy from pair (residue) interactions
     * @param aa1
     * @param rotInd1
     * @param rl1
     * @param aa2
     * @param rotInd2
     * @param rl2
     * @return
     */
    double calculatePairEnergy(AminoAcidS aa1, int rotInd1, RotamerLibrary rl1, AminoAcidS aa2, int rotInd2, RotamerLibrary rl2);

    /**
     * Method that returns the internal energy of a residue, considering its rotamer probability in the rotamer library
     * @param aa
     * @param rotInd rotamer position in the list
     * @param rl list of rotamers
     * @return
     */
    double calculateSelfEnergy(AminoAcidS aa, int rotInd, RotamerLibrary rl);

    String [] getEnergyTerms();
}
