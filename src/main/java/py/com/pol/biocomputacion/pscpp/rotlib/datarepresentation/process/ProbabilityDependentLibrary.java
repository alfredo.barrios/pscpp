package py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process;

import java.util.Comparator;
import java.util.Objects;

public class ProbabilityDependentLibrary implements Comparable<ProbabilityDependentLibrary>{
    private double probability;
    private double phi;
    private double psi;
    private String amino;

    public double getProbability() {
        return probability;
    }

    public double getPhi() {
        return phi;
    }

    public double getPsi() {
        return psi;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public void setPhi(double phi) {
        this.phi = phi;
    }

    public void setPsi(double psi) {
        this.psi = psi;
    }

    public String getAmino() {
        return amino;
    }

    public void setAmino(String amino) {
        this.amino = amino;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProbabilityDependentLibrary)) return false;
        ProbabilityDependentLibrary that = (ProbabilityDependentLibrary) o;
        return Double.compare(that.getProbability(), getProbability()) == 0 &&
                Double.compare(that.getPhi(), getPhi()) == 0 &&
                Double.compare(that.getPsi(), getPsi()) == 0 &&
                Objects.equals(getAmino(), that.getAmino());
    }

    @Override
    public int compareTo(ProbabilityDependentLibrary o){
        if(o ==null || o.getAmino()==null)
            return -1;

        return Comparator.comparing(ProbabilityDependentLibrary::getAmino)
                .thenComparingDouble(ProbabilityDependentLibrary::getProbability)
                .thenComparingDouble(ProbabilityDependentLibrary::getPhi)
                .thenComparingDouble(ProbabilityDependentLibrary::getPsi)
                .compare(this, o);
    }
    @Override
    public int hashCode() {

        return Objects.hash(getProbability(), getPhi(), getPsi());
    }
}
