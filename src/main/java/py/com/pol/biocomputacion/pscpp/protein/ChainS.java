package py.com.pol.biocomputacion.pscpp.protein;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * This class deals with the data and operations for a chain of a protein.
 *
 * @author jcolbes
 */
public class ChainS {
    public final AminoAcidS[] residues;
    public String chainID;

    public ChainS(int size) {
        residues = new AminoAcidS[size];
    }

    /**
     * Method for cloning a chain
     *
     * @return
     */
    public ChainS cloneChainS() {
        ChainS chainOut = new ChainS(this.residues.length);
        for (int i = 0; i < this.residues.length; i++) chainOut.residues[i] = this.residues[i].cloneAminoAcidS();
        chainOut.chainID = this.chainID;
        return chainOut;
    }

    public void print() {
        System.out.println("Chain ID: " + chainID);
        System.out.println("Number of residues: " + residues.length);
    }

    @Override
    public String toString() {
        return new StringJoiner(", \n", ChainS.class.getSimpleName() + "[", "]")
                .add("residues=" + Arrays.toString(residues))
                .add("chainID='" + chainID + "'")
                .toString();
    }
}
