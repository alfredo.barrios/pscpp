package py.com.pol.biocomputacion.pscpp.energy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.energy.interfaces.Energy;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerCollection;

public class EnergyFactory {

    private static Energy energy;
    private static final String PACKAGE_BASE = "py.com.pol.biocomputacion.pscpp.energy.implementations.";
    private static final Logger logger = LoggerFactory.getLogger(EnergyFactory.class);

    /**
     * When the Factory is created, we choose the EnergyFunction.
     * The complete path of the Implementation Class is a Simple String provided.
     */
    public EnergyFactory(RotamerCollection rlc) {
        try {
            Class energyFunction = Class.forName(PACKAGE_BASE + (ParameterFile.getParameters().get(ParameterFile.STR_ENERGY_FUNCTION_IMPLEMENTATION))
                    , true, this.getClass().getClassLoader());
            if (Energy.class.isAssignableFrom(energyFunction)) {
                logger.info("Energy Function Class: {}",energyFunction.getName());
                energy = (Energy) energyFunction.getConstructor(RotamerCollection.class).newInstance(rlc);
            } else {
                logger.error("The Given Class: {} do not implement the Energy Interface ",energyFunction.getName() );
                System.exit(1);
            }
        } catch (Exception e) {
            logger.error("Error generating the Energy Factory : " , e);
            System.exit(1);
        }
    }
    public Energy getEnergy(){
        return energy;
    }
}
