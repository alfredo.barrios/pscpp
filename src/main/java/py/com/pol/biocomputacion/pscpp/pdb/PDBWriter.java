package py.com.pol.biocomputacion.pscpp.pdb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.AtomS;
import py.com.pol.biocomputacion.pscpp.protein.ChainS;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PDBWriter {
    private static final Logger logger = LoggerFactory.getLogger(PDBWriter.class);
    /**
     * Saves a structure in a file with a short format (for faster reading).
     *
     * @param s
     * @param fileName
     * @param pdbCodes
     */
    public static void writeShortPDB(StructureS s, String fileName, ArrayList<String> pdbCodes) {
        File fileOut;
        PrintWriter outFile;
        DecimalFormat coords = new DecimalFormat("#0.000");
        try {
            fileOut = new File(fileName);
            outFile = new PrintWriter(new BufferedWriter(new FileWriter(fileOut)));
            int aminoAcidIndex, position;
            ChainS c;
            AminoAcidS aa;
            String[][] atomOrder = ListUtil.atomList;
            String atomName;
            Map<String, Integer> positions;
            int countRes = 0, numAtoms;

            outFile.println(s.chains.length + " " + pdbCodes.size());
            for (int iChain = 0; iChain < s.chains.length; iChain++) {
                c = s.chains[iChain];
                outFile.println(c.chainID + " " + c.residues.length);
                for (int iGroup = 0; iGroup < c.residues.length; iGroup++) {
                    aa = c.residues[iGroup];
                    aminoAcidIndex = aa.aminoIndex;
                    numAtoms = 0;
                    for (AtomS at : aa.atoms) if (at != null) numAtoms++;
                    outFile.println(ListUtil.aaThreeLetterCode[aminoAcidIndex] + " " + pdbCodes.get(countRes++) + " " + numAtoms);
                    positions = ListUtil.atomNamePosition.get(aminoAcidIndex);
                    for (int iAtom = 0; iAtom < atomOrder[aminoAcidIndex].length; iAtom++) {
                        atomName = atomOrder[aminoAcidIndex][iAtom];
                        position = positions.get(atomName);
                        if (aa.atoms[position] != null) {
                            outFile.println(atomName + " " + coords.format(aa.atoms[position].x) + " " + coords.format(aa.atoms[position].y) + " " + coords.format(aa.atoms[position].z));
                        }
                    }
                }
            }
            outFile.close();
        } catch (Exception ex) {
            logger.error("Problem saving the structure in a file: {}" , ex.getMessage());
        }
    }

    /**
     * Saves a structure in a file with PDB format.
     * @param s
     * @param fileName
     * @param pdbCodes
     */
    public  void writePDB(StructureS s, String fileName, List<String> pdbCodes) {
        File fileOut;
        PrintWriter outFile;
        try {
            fileOut = new File(fileName);
            outFile = new PrintWriter(new BufferedWriter(new FileWriter(fileOut)));
            int countRes=0,countAtom=1, aminoAcidIndex, position;
            ChainS c;
            AminoAcidS aa;
            String[][] atomOrder = ListUtil.atomList;
            String atomName, chainID, resName;
            Map<String,Integer> positions;

            for (int iChain = 0; iChain < s.chains.length; iChain++) {
                c = s.chains[iChain];
                chainID = c.chainID;
                for (int iGroup = 0; iGroup < c.residues.length; iGroup++) {
                    aa = c.residues[iGroup];
                    aminoAcidIndex = aa.aminoIndex;
                    resName = ListUtil.aaThreeLetterCode[aminoAcidIndex];
                    positions = ListUtil.atomNamePosition.get(aminoAcidIndex);
                    for (int iAtom=0; iAtom<atomOrder[aminoAcidIndex].length; iAtom++) {
                        atomName = atomOrder[aminoAcidIndex][iAtom];
                        position = positions.get(atomName);
                        if(aa.atoms[position]!=null){
                            outFile.println(getATOMRecord(aa.atoms[position], atomName, countAtom++, resName, pdbCodes.get(countRes), chainID));
                        }
                    }
                    countRes++;
                }
            }
            outFile.close();
        }
        catch (IOException ex) {
            logger.error("Problem saving the structure in a file.");
        }
    }

    /**
     * This method returns a string corresponding to an atom data in PDB format
     * @param a
     * @return
     */
    private String getATOMRecord(AtomS a, String atomName, int atomNum, String resName, String resNum, String chainID) {
        StringBuilder sb = new StringBuilder();
        DecimalFormat coords = new DecimalFormat("#0.000");
        DecimalFormat df2 = new DecimalFormat("#0.00");

        sb.append("ATOM  "); //record name

        //atom serial number
        String temp = Integer.toString(atomNum);
        while (temp.length() < 5) {
            temp = " " + temp;
        }
        sb.append(temp);
        sb.append("  ");

        //atom name
        sb.append(atomName);
        while (sb.length() < 16) sb.append(" ");


        //res name
        sb.append(" ");
        sb.append(resName);
        sb.append(" ");

        sb.append(chainID); //chain identifier

        //residue seq number
        temp = resNum;
        while (temp == null || temp.length() < 4) {
            temp = " " + temp;
        }
        if ('0' <= temp.charAt(temp.length() - 1) && temp.charAt(temp.length() - 1) <= '9') {
            temp = temp + " ";
        } else  {
            temp = " " + temp;
        }
        sb.append(temp);
        sb.append("   ");

        //X
        temp = coords.format(a.x);
        while (temp.length() < 8)
            temp = " " + temp;
        sb.append(temp);

        //Y
        temp = coords.format(a.y);
        while (temp.length() < 8)
            temp = " " + temp;
        sb.append(temp);

        //Z
        temp = coords.format(a.z);
        while (temp.length() < 8)
            temp = " " + temp;
        sb.append(temp);

        //occupancy
        temp = df2.format(0.0);
        while (temp.length() < 6)
            temp = " " + temp;
        sb.append(temp);

        //temperature factor
        temp = df2.format(0.0);
        while (temp.length() < 6)
            temp = " " + temp;
        sb.append(temp);
        sb.append("          ");

        //element symbol
        temp = "" + atomName.charAt(0);
        while (temp.length() < 2)
            temp = " " + temp;
        sb.append(temp);

        sb.append("  ");    //charge

        return sb.toString();
    }
}
