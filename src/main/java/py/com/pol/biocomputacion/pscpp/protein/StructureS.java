package py.com.pol.biocomputacion.pscpp.protein;

import py.com.pol.biocomputacion.pscpp.data.AtomData;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.Rotamer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StructureS {
    public final ChainS[] chains;
    public String pdbID;
    public boolean isComplete; //indicates if a structure is complete (i.e., no missing atoms)

    private static final double LIMITDOWNACC, LIMITUPACC;
    private static final int ASPINDEX, PHEINDEX, TYRINDEX, ASNINDEX, HISINDEX, GLNINDEX, GLUINDEX, ARGINDEX, NH1ARGINDEX, NH2ARGINDEX;

    static {
        LIMITDOWNACC = (Double) ParameterFile.getParameters().get(ParameterFile.DOUBLE_LIMIT_SOL_ACC_DOWN);
        LIMITUPACC = (Double) ParameterFile.getParameters().get(ParameterFile.DOUBLE_LIMIT_SOL_ACC_UP);
        ASPINDEX = ListUtil.aaNamePosition.get("ASP");
        PHEINDEX = ListUtil.aaNamePosition.get("PHE");
        TYRINDEX = ListUtil.aaNamePosition.get("TYR");
        ASNINDEX = ListUtil.aaNamePosition.get("ASN");
        HISINDEX = ListUtil.aaNamePosition.get("HIS");
        GLNINDEX = ListUtil.aaNamePosition.get("GLN");
        GLUINDEX = ListUtil.aaNamePosition.get("GLU");
        ARGINDEX = ListUtil.aaNamePosition.get("ARG");
        NH1ARGINDEX = ListUtil.atomNamePosition.get(ARGINDEX).get("NH1");
        NH2ARGINDEX = ListUtil.atomNamePosition.get(ARGINDEX).get("NH2");
    }

    public StructureS(int size) {
        chains = new ChainS[size];
    }

    /**
     * Method for cloning a structure
     *
     * @return
     */
    public StructureS cloneStructure() {
        StructureS sOut = new StructureS(this.chains.length);
        for (int i = 0; i < this.chains.length; i++) sOut.chains[i] = this.chains[i].cloneChainS();
        sOut.isComplete = this.isComplete;
        sOut.pdbID = this.pdbID;
        return sOut;
    }
    /**
     * Returns the number of atoms in the structure
     * @return
     */
    public int getNumberofAtoms(){
        int count=0;
        for(ChainS c: this.chains){
            for(AminoAcidS aa: c.residues){
                for(AtomS atom: aa.atoms) if(atom!=null) count++;
            }
        }
        return count;
    }
    /**
     * Returns the number of residues in the structure
     *
     * @return
     */
    public int getNumberofResidues() {
        int count = 0;
        for (ChainS c : this.chains) count += c.residues.length;
        return count;
    }

    /**
     * Number of residues with side-chain torsion angles
     *
     * @return
     */
    public int getNumberofResiduesWithTorsionAngle() {
        int count = 0;
        for (ChainS c : chains) {
            for (AminoAcidS aa : c.residues) {
                if (ListUtil.numSidechainTorsion[aa.aminoIndex] > 0) count++;
            }
        }
        return count;
    }

    /**
     * Indicates if a structure has a missing side-chain main atom
     * @return
     */
    public boolean hasMissingSCAtoms(){
        for(ChainS c: chains) for(AminoAcidS aa: c.residues) if(aa.hasMissingSCAtoms()) return true;
        return false;
    }


    /**
     * Method for calculating the accuracy of a structure, given another as reference
     * @param pdbStructure reference structure
     * @param methodStructure evaluated structure
     * @param chi number of torsion angles
     * @return
     */
    static public double calculateAccuracy(StructureS pdbStructure, StructureS methodStructure, int chi) {
        AminoAcidS pdbAA,methodAA;
        ChainS pdbChain,methodChain;
        int count_correct=0, count_enabled=0;
        boolean[] corrAndEnab;
        for (int chain=0; chain<pdbStructure.chains.length; chain++) { //it is assumed that two structures of the same protein are compared
            pdbChain = pdbStructure.chains[chain];
            methodChain = methodStructure.chains[chain];
            for (int group=0; group<pdbChain.residues.length; group++) {
                pdbAA = pdbChain.residues[group];
                methodAA = methodChain.residues[group];
                corrAndEnab = AminoAcidS.isAccurate(pdbAA, methodAA, chi);
                if(corrAndEnab[0]) count_correct++;
                if(corrAndEnab[1]) count_enabled++;
            }
        }
        if(count_enabled==0) return (-1.0);
        return (1.0*count_correct/count_enabled);
    }

    /**
     * Method for calculating the accuracy of buried atoms of a structure, given another as reference
     * @param pdbStructure reference structure
     * @param methodStructure evaluated structure
     * @param chi number of torsion angles
     * @param porcAccess SASA values for each residue
     * @return
     */
    static public double calculateAccuracyBuried(StructureS pdbStructure, StructureS methodStructure, int chi, double[] porcAccess) {
        AminoAcidS pdbAA,methodAA;
        ChainS pdbChain,methodChain;
        int count_correct=0, count_enabled=0;
        boolean[] corrAndEnab;
        int count=0;
        for (int chain=0; chain<pdbStructure.chains.length; chain++) { //it is assumed that two structures of the same protein are compared
            pdbChain = pdbStructure.chains[chain];
            methodChain = methodStructure.chains[chain];
            for (int group=0; group<pdbChain.residues.length; group++) {
                if(porcAccess[count]<=LIMITDOWNACC){
                    pdbAA = pdbChain.residues[group];
                    methodAA = methodChain.residues[group];
                    corrAndEnab = AminoAcidS.isAccurate(pdbAA, methodAA, chi);
                    if(corrAndEnab[0]) count_correct++;
                    if(corrAndEnab[1]) count_enabled++;

                }
                count++;
            }
        }
        if(count_enabled==0) return (-1.0);
        return (1.0*count_correct/count_enabled);
    }

    /**
     * Method for calculating the accuracy of exposed atoms of a structure, given another as reference
     * @param pdbStructure reference structure
     * @param methodStructure evaluated structure
     * @param chi number of torsion angles
     * @param porcAccess SASA values for each residue
     * @return
     */
    static public double calculateAccuracyExposed(StructureS pdbStructure, StructureS methodStructure, int chi, double[] porcAccess) {
        AminoAcidS pdbAA,methodAA;
        ChainS pdbChain,methodChain;
        int count_correct=0, count_enabled=0;
        boolean[] corrAndEnab;
        int count=0;
        for (int chain=0; chain<pdbStructure.chains.length; chain++) { //it is assumed that two structures of the same protein are compared
            pdbChain = pdbStructure.chains[chain];
            methodChain = methodStructure.chains[chain];
            for (int group=0; group<pdbChain.residues.length; group++) {
                if(porcAccess[count]>LIMITUPACC){
                    pdbAA = pdbChain.residues[group];
                    methodAA = methodChain.residues[group];
                    corrAndEnab = AminoAcidS.isAccurate(pdbAA, methodAA, chi);
                    if(corrAndEnab[0]) count_correct++;
                    if(corrAndEnab[1]) count_enabled++;

                }
                count++;
            }
        }
        if(count_enabled==0) return (-1.0);
        return (1.0*count_correct/count_enabled);
    }

    /**
     * Method for calculating the RMSD between two structures
     * @param s1 Reference structure
     * @param s2 Evaluated structure
     * @return
     */
    public static double calculateSCRMSD(StructureS s1, StructureS s2){
        ChainS c1,c2;
        double rmsdAA, rmsdAAMod;
        int aminoAcidIndex;
        List<AtomS> alAtom1 = new ArrayList();
        List<AtomS> alAtom2 = new ArrayList();
        AminoAcidS aa1,aa2,aa2BB,aa2Mod,aa2Out;
        double[] chi;
        int numTorAng;
        AtomData[] atomsData;
        int[] refAtoms;
        AtomData atData;
        int x1,x2,x3,x4;
        AtomS a1,a2,a3,a4;
        AtomS at1,at2,aux;
        //Generation of list of atoms
        for(int chain=0; chain<s1.chains.length; chain++){
            c1 = s1.chains[chain];
            c2 = s2.chains[chain];
            for(int i=0;i<c1.residues.length;i++){
                aa1 = c1.residues[i];
                aa2 = c2.residues[i];
                aminoAcidIndex = aa1.aminoIndex;
                numTorAng = ListUtil.numSidechainTorsion[aminoAcidIndex];
                if(numTorAng<1) continue;
                //Torsion angles are calculated
                chi = new double[numTorAng];
                atomsData = ListUtil.atomGenData[aminoAcidIndex];
                for(int j=0;j<chi.length;j++){
                    atData = atomsData[(j+5)];
                    refAtoms = atData.refAtomsPos;
                    x1=refAtoms[0];
                    x2=refAtoms[1];
                    x3=refAtoms[2];
                    x4=(j+5);
                    a1 = aa2.atoms[x1];
                    a2 = aa2.atoms[x2];
                    a3 = aa2.atoms[x3];
                    a4 = aa2.atoms[x4];
                    if(a1!=null && a2!=null && a3!=null && a4!=null){
                        chi[j] = AtomS.getTorsionAngle(a1, a2, a3, a4);
                    }
                    else chi[j]=0.0;
                }


                //Special cases
                if(aminoAcidIndex==ASPINDEX || aminoAcidIndex==PHEINDEX || aminoAcidIndex==TYRINDEX || aminoAcidIndex==ASNINDEX || aminoAcidIndex==HISINDEX){
                    chi[1] = chi[1]+180.0;
                    if(chi[1]>180.0) chi[1] = chi[1]-360.0;
                }
                if(aminoAcidIndex==GLNINDEX || aminoAcidIndex==GLUINDEX){
                    chi[2] = chi[2]+180.0;
                    if(chi[2]>180.0) chi[2] = chi[2]-360.0;
                }

                Rotamer r = new Rotamer();
                r.setChi(chi); r.setProbability(0.0D);
                aa2BB = aa2.cloneBBAtomsAminoAcidS();
                aa2Mod = AminoAcidS.createAminoacid(aa2BB, r);
                if(aminoAcidIndex==ARGINDEX){
                    aux = aa2Mod.atoms[NH1ARGINDEX];
                    aa2Mod.atoms[NH1ARGINDEX] = aa2Mod.atoms[NH2ARGINDEX];
                    aa2Mod.atoms[NH2ARGINDEX] = aux;
                }
                rmsdAA = AminoAcidS.calculateSCRMSDAAsOr(aa1,aa2);
                rmsdAAMod = AminoAcidS.calculateSCRMSDAAsOr(aa1,aa2Mod);

                if(rmsdAA>rmsdAAMod) aa2Out=aa2Mod;
                else aa2Out=aa2;

                //Passing side-chain atoms
                for(int j=5;j<aa1.atoms.length;j++){ //CB is a BB atom
                    at1=aa1.atoms[j];
                    at2=aa2Out.atoms[j];
                    if (at1!=null && at2!=null) {
                        alAtom1.add(at1);
                        alAtom2.add(at2);
                    }
                }
            }
        }
        double rmsd;
        if(alAtom1.isEmpty()) rmsd=(999999.0);
        else rmsd = AtomS.getRMSD(alAtom1, alAtom2);
        return rmsd;
    }

    /**
     * Method for calculating the number of collisions in a structure
     * @param s input structure
     * @return
     */
    public static int calculateTotalCollisions(StructureS s){
        //Passing to a list of residues
        List<AminoAcidS> resList = new ArrayList<>(s.getNumberofResidues());
        AminoAcidS aa;
        ChainS c;
        for(int i=0; i<s.chains.length; i++){
            c = s.chains[i];
            for(int j=0; j<c.residues.length; j++){
                aa = c.residues[j];
                resList.add(aa);
            }
        }

        int col = 0;
        for(int i=0; i<resList.size(); i++){
            for(int j=(i+1); j<resList.size(); j++){
                col += AminoAcidS.calcCollisions(resList.get(i), resList.get(j));
            }
        }
        return col;
    }

    @Override
    public String toString() {
        return "StructureS{" +
                "chains=" + Arrays.toString(chains) +
                ", pdbID='" + pdbID + '\'' +
                ", isComplete=" + isComplete +
                '}';
    }
}
