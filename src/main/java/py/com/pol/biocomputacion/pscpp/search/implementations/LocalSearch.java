package py.com.pol.biocomputacion.pscpp.search.implementations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;
import py.com.pol.biocomputacion.pscpp.search.interfaces.Search;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class LocalSearch implements Search {

    private static final Logger logger = LoggerFactory.getLogger(LocalSearch.class);

    private int iter,changes;
    private double totEnergyEnd,totEnergyStart;
    private Protein p;
    private int[] rotIndices;
    private int[] numRots;
    private boolean[] hasRotamers;
    private DecimalFormat df;
    public double[] actualSolutionEnergy;

    @Override
    public void setProtein(Protein protein){
        p = protein;
        df = new DecimalFormat("#0.000");
        this.p = p;
        numRots = new int[p.strucSize];
        hasRotamers = new boolean[p.strucSize];
        RotamerLibrary rl;
        RotamerLibrary[] rotLibs = p.rotLibs;
        for(int i=0; i<p.strucSize; i++){
            rl = rotLibs[i];
            if(rl==null){ numRots[i]=0; hasRotamers[i]=false; }
            else{ numRots[i]=rl.getRotamers().length; hasRotamers[i]=true; }
        }
        rotIndices = p.rotamersIndex; //Solution

    }
    @Override
    public void search() {
        iter=0; changes=0;
        totEnergyStart = p.getTotalEnergy();
        logger.info("Energy[0]:\t {}", df.format(p.getTotalEnergy()));
        Double delta;
        boolean change = true;
        int aux,pos;
        List<Double> actEnergy = new ArrayList<>();
        actEnergy.add(totEnergyStart);
        double actEn;
        while(change){
            change = false;
            for(int i=0; i<p.strucSize; i++){
                if(hasRotamers[i]){ //ALA and GLY
                    aux = rotIndices[i]; //original position
                    pos = aux;
                    for(int j=0; j<numRots[i]; j++){
                        if(j!=aux){
                            delta = calculateSingleEnergy(i,j) - calculateSingleEnergy(i,pos);
                            if(delta<0.0){
                                change = true;
                                pos=j;
                            }
                        }
                    }
                    if(pos!=aux){
                        changes++;
                        rotIndices[i] = pos; //Final change
                    }
                }
            }
            iter++;
            actEn = p.getTotalEnergy();
            logger.info("Energy[\"{}\"]:\t {}",iter,df.format(actEn));
            actEnergy.add(actEn);
        }
        actualSolutionEnergy = new double[actEnergy.size()];
        for(int i=0; i<actEnergy.size(); i++) actualSolutionEnergy[i] = actEnergy.get(i);

        totEnergyEnd = p.getTotalEnergy();

    }
    /**
     * Calculates the contribution of a rotamer in the selected residue position
     * @param res
     * @param rot
     * @return
     */
    private double calculateSingleEnergy(int res, int rot){
        double e = p.enerSelfInteractions[res][rot];
        int rotJ;
        for(int j: p.neighborList.get(res)){
            rotJ = rotIndices[j];
            if(rotJ==(-1)) rotJ = 0;
            if(j>res) e += p.enerPairInteractions[res][rot][j][rotJ];
            else e += p.enerPairInteractions[j][rotJ][res][rot];
        }
        return e;
    }


    @Override
    public double[] getPosibleSolution() {
        return actualSolutionEnergy;
    }

    @Override
    public int getChanges() {
        return changes;
    }

    @Override
    public int getIterations(){
        return iter;
    }
}
