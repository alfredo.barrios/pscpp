package py.com.pol.biocomputacion.pscpp.utils;

import java.util.HashMap;

public class ListUtils {
    //Set an order for Aminoacids;
    public static final HashMap<String,Integer> aminoacidsOrder = new HashMap<String, Integer>(){{
        put("ALA",0);
        put("ARG",1);
        put("ASN",2);
        put("ASP",3);
        put("CYS",4);
        put("GLN",5);
        put("GLU",6);
        put("GLY",7);
        put("HIS",8);
        put("ILE",9);
        put("LEU",10);
        put("LYS",11);
        put("MET",12);
        put("PHE",13);
        put("PRO",14);
        put("SER",15);
        put("THR",16);
        put("TRP",17);
        put("TYR",18);
        put("VAL",19);

    }};

}
