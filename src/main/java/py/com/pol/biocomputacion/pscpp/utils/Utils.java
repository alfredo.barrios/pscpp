package py.com.pol.biocomputacion.pscpp.utils;

import org.biojava.nbio.structure.*;
import org.biojava.nbio.structure.io.PDBParseException;
import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.AtomS;
import py.com.pol.biocomputacion.pscpp.protein.ChainS;
import py.com.pol.biocomputacion.pscpp.protein.StructureS;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Class with utility methods.
 * @author jcolbes
 */
public class Utils {
    
    private static final int ASPINDEX, PHEINDEX, TYRINDEX, ASNINDEX, HISINDEX, GLNINDEX, GLUINDEX, CAINDEX;


    static{
        ASPINDEX = ListUtil.aaNamePosition.get("ASP");
        PHEINDEX = ListUtil.aaNamePosition.get("PHE");
        TYRINDEX = ListUtil.aaNamePosition.get("TYR");
        ASNINDEX = ListUtil.aaNamePosition.get("ASN");
        HISINDEX = ListUtil.aaNamePosition.get("HIS");
        GLNINDEX = ListUtil.aaNamePosition.get("GLN");
        GLUINDEX = ListUtil.aaNamePosition.get("GLU");
        CAINDEX = ListUtil.atomNamePosition.get(ASPINDEX).get("CA");
    }
    
    public static double round(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }
    
    /**
     * Difference between torsion angles of two conformations. It had considered the observations from Eyal(2004) about symmetry and pseudo-symmetry.
     * @param angle1 first torsion angle
     * @param angle2 second torsion angle
     * @param aminoIndex index of three-letter code of the residue
     * @param chi number of torsion angle (X1=1, X2=2, ...)
     * @return 
     */
    public static double angleDev(int aminoIndex, double angle1, double angle2, int chi) {   
        if(chi==2){ // X2 torsion angle
            if(aminoIndex==ASPINDEX || aminoIndex==PHEINDEX || aminoIndex==TYRINDEX || aminoIndex==ASNINDEX || aminoIndex==HISINDEX){
                double dif1 = Math.abs(angle1 - angle2);
                double ang2 = angle2;
                if(ang2>0.0) ang2 -= 180.0;
                else ang2 += 180.0;
                double dif2 = Math.abs(angle1 - ang2);
                
                dif1 = Math.min(dif1, (360.0 - dif1));
                dif2 = Math.min(dif2, (360.0 - dif2));
                double dif = Math.min(dif1, dif2);
                return dif;
            }
        }
        if(chi==3){ // X3 torsion angle
            if(aminoIndex==GLNINDEX || aminoIndex==GLUINDEX){
                double dif1 = Math.abs(angle1 - angle2);
                double ang2 = angle2;
                if(ang2>0.0) ang2 -= 180.0;
                else ang2 += 180.0;
                double dif2 = Math.abs(angle1 - ang2);
                
                dif1 = Math.min(dif1, (360.0 - dif1));
                dif2 = Math.min(dif2, (360.0 - dif2));
                double dif = Math.min(dif1, dif2);
                return dif;
            }
        }
        
        double dif = Math.abs(angle1 - angle2);
        dif = Math.min(dif, (360 - dif));
        return dif;
    }

    
    /**
     * Method for getting all the CA atoms of a structure
     * @param s
     * @return
     * @throws StructureException 
     */
    private static Atom[] getCAAtoms(Structure s) throws StructureException{
        List<Atom> cA_Atoms = new ArrayList<>();
        AminoAcid aa;
        for(Chain c: s.getChains()){
            for(Group g : c.getAtomGroups()){
                aa = (AminoAcid) g;
                cA_Atoms.add(aa.getAtom(CAINDEX));
            }
        }
        return cA_Atoms.toArray(new Atom[cA_Atoms.size()]);
    }
    
    /**
     * Converts an object of StructureS into an object of BioJava Structure
     * @param s
     * @return
     * @throws PDBParseException 
     */
    public static Structure toBioJavaStructure(StructureS s) throws PDBParseException {
        Structure struc = new StructureImpl();
        struc.setPDBCode(s.pdbID);
        Chain chain;
        AminoAcid aa;
        Atom at;
        int count=1, count2, aminoIndex;
        String[] atomNames;
        for(ChainS c: s.chains){
            chain = new ChainImpl();
            chain.setChainID(c.chainID);
            for(AminoAcidS aaS: c.residues){
                aa = new AminoAcidImpl();
                aminoIndex = aaS.aminoIndex;
                aa.setPDBName(ListUtil.aaThreeLetterCode[aminoIndex]);
                aa.setResidueNumber(new ResidueNumber(c.chainID, count++, 'A'));
                count2=0;
                atomNames = ListUtil.atomGenOrder[aminoIndex];
                for(AtomS atS: aaS.atoms){
                    if(atS==null) continue;
                    at = new AtomImpl();
                    at.setName(atomNames[count2++]);
                    at.setX(atS.x);
                    at.setY(atS.y);
                    at.setZ(atS.z);
                    aa.addAtom(at);
                }
                chain.addGroup(aa);
            }
            struc.addChain(chain);
        }
        return struc;
    }
}
