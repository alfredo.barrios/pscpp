package py.com.pol.biocomputacion.pscpp.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.search.interfaces.Search;

public class SearchFactory {
    private static final Logger logger = LoggerFactory.getLogger(SearchFactory.class);
    private static Search search;
    private static final String PACKAGE_BASE = "py.com.pol.biocomputacion.pscpp.search.implementations.";

    /**
     * When the Factory is created, we choose the SearchMethod.
     * The complete path of the Implementation Class is a Simple String provided.
     */
    public SearchFactory() {
        try {
            Class searchMethod = Class.forName(PACKAGE_BASE + (ParameterFile.getParameters().get(ParameterFile.STR_SEARCH_METHOD_IMPLEMENTATION))
                    , true, this.getClass().getClassLoader());
            if (Search.class.isAssignableFrom(searchMethod)) {
                logger.info("Search Class: {}",searchMethod.getName());
                search = (Search) searchMethod.newInstance();
            } else {
                logger.error("The Given Class do not implement the Energy Interface");
                System.exit(1);
            }
        } catch (Exception e) {
            logger.error("Error generating the Search Factory : " , e);
            System.exit(1);
        }
    }
    public Search getSearch(){
        return search;
    }
}
