package py.com.pol.biocomputacion.pscpp.desktoptests;

import py.com.pol.biocomputacion.pscpp.data.ListUtil;
import py.com.pol.biocomputacion.pscpp.main.AnalysisPSCPP;
import py.com.pol.biocomputacion.pscpp.main.ParameterFile;

public class ResultAnalysisTest {

    /**
     * Run the complete analysis of the PSCPP problem, with the correspondent data
     * */
    public static void main(String[] args) {
        try {

            long t1 = System.currentTimeMillis();
            ParameterFile.loadAll();
            ListUtil.initializeLists();
            // FileName,FolderIn, FolderOut, InitialStruct, AddRotamers?
            AnalysisPSCPP.analysisPSCPP(args[0],args[1],args[2],args[3], false);
            long t2 = System.currentTimeMillis();
            System.out.println("Total: "+ (t2-t1));
        }catch (Exception e ){
            e.printStackTrace();
        }
    }
}
