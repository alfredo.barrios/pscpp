package py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.preprocess;

import java.util.Objects;

/**
 * The Class Represents the key for a dependent library.
 */
public class DependentKey {

    private String amino;
    private double phi;
    private double psi;

    public DependentKey() {
    }

    public double getPhi() {
        return phi;
    }

    public void setPhi(double phi) {
        this.phi = phi;
    }

    public double getPsi() {
        return psi;
    }

    public void setPsi(double psi) {
        this.psi = psi;
    }

    public DependentKey(String aminoR, double phi, double psi) {
        this.phi = phi;
        this.psi = psi;
        this.amino= aminoR;
    }

    public String getAmino() {
        return amino;
    }

    public void setAmino(String amino) {
        this.amino = amino;
    }

    @Override
    public String toString() {
        return amino + ";" + phi +
                ";" + psi;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass() || amino == null) return false;
        DependentKey key = (DependentKey) o;
        return (amino.equalsIgnoreCase(key.getAmino()) && Double.compare(phi, key.phi)==0 && Double.compare(psi,key.phi)==0);
    }

    @Override
    public int hashCode() {

        return Objects.hash(amino, phi, psi);
    }
}