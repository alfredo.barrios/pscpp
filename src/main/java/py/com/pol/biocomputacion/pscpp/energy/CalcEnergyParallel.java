package py.com.pol.biocomputacion.pscpp.energy;

import py.com.pol.biocomputacion.pscpp.energy.interfaces.Energy;
import py.com.pol.biocomputacion.pscpp.protein.AminoAcidS;
import py.com.pol.biocomputacion.pscpp.protein.Protein;
import py.com.pol.biocomputacion.pscpp.rotlib.datarepresentation.process.RotamerLibrary;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

    /**
     * This class contains the method compute(), necessary for parallelization of the interaction calculation with CISRR scoring function.
     */
    public class CalcEnergyParallel  extends RecursiveAction  {

        private final Protein p;
        private final int structSize;
        private final int i;
        public double[][][] enerPairInteractions;
        public double[] enerSelfInteractions;
        private final Energy ener;



    public CalcEnergyParallel(Protein p, int i, Energy ener){
        this.p = p;
        this.ener = ener;
        this.i = i;
        this.structSize = p.strucSize;
    }

    /**
     * Calculates the possible interactions of one residue (self and pair interactions)
     */
        @Override
        protected void compute() {
            if(i==(-1)){
                ArrayList<CalcEnergyParallel> tasks = new ArrayList<>();
                for(int i=0; i<structSize; i++){
                    tasks.add(new CalcEnergyParallel(p, i, ener));
                }
                invokeAll(tasks);
            }
            else{
                AminoAcidS[][] possibleResidues = p.possibleResidues;
                RotamerLibrary rl1 = p.rotLibs[i], rl2;
                AminoAcidS[] posResI = possibleResidues[i], posResJ;
                List<Integer> pDBsToVerify = p.neighborList.get(i);
                AminoAcidS aa1, aa2;
                int tam1=posResI.length,tam2;
                RotamerLibrary[] rotLibs = p.rotLibs;
                enerSelfInteractions = new double[tam1];
                enerPairInteractions = new double[tam1][][];
                boolean[] isChainEnd = p.chainEnd;
                for(int rotI=0; rotI<tam1; rotI++){
                    aa1 = posResI[rotI];
                    aa1.setIsEnd(isChainEnd[i]);
                    if(rl1==null) enerSelfInteractions[rotI]=0.0;
                    else enerSelfInteractions[rotI] = ener.calculateSelfEnergy(aa1, rotI, rl1);
                    enerPairInteractions[rotI] = new double[structSize][];
                    for(int j: pDBsToVerify){
                        if(i<j){
                            posResJ = possibleResidues[j];
                            tam2 = posResJ.length;
                            rl2 = rotLibs[j];
                            enerPairInteractions[rotI][j] = new double[tam2];
                            for(int rotJ=0; rotJ<tam2; rotJ++){
                                aa2 = posResJ[rotJ];
                                aa2.setIsEnd(isChainEnd[j]);
                                enerPairInteractions[rotI][j][rotJ] = ener.calculatePairEnergy(aa1, rotI, rl1, aa2, rotJ, rl2);
                            }
                        }
                    }
                }
                p.enerPairInteractions[i] = enerPairInteractions;
                p.enerSelfInteractions[i] = enerSelfInteractions;
            }
        }

}
