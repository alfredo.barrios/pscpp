package py.com.pol.biocomputacion.pscpp.desktoptests;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
/**
 * Compare If Two Folders Text files are Identical
* */
public class CompareTwoFiles {

    public static void main(String[] args) {
        try {
            //The fist argument is the path of the first file.
            List<String> list1=Files.readAllLines(Paths.get(args[0]));
            //The second argument is the path of the second file.
            List<String> list2=Files.readAllLines(Paths.get(args[1]));
            boolean equal = list1.containsAll(list2) && list1.size() == list2.size();
            System.out.println("Equal Files: "+ equal);
            if(!equal) {
                Collection<String> similar = new HashSet<>(list1);
                Collection<String> different = new HashSet<>();
                different.addAll(list1);
                different.addAll(list2);
                similar.retainAll(list2);
                different.removeAll(similar);
                System.out.println("Los diferentes son");
                different.forEach(System.out::println);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static boolean compare(String path1, String path2){
        try {
            //The fist argument is the path of the first file.
            List<String> list1=Files.readAllLines(Paths.get(path1));
            //The second argument is the path of the second file.
            List<String> list2=Files.readAllLines(Paths.get(path2));
            boolean equal = list1.containsAll(list2) && list1.size() == list2.size();
            System.out.println("Equal Files: "+ equal);
            return equal;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
